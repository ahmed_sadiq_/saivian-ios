//
//  AppDelegate.m
//  Saivian
//
//  Created by Apple on 27/04/2016.
//  Copyright © 2016 Osama. All rights reserved.
//

#import "AppDelegate.h"
#import "NavigationHandler.h"
#import "StoreModel.h"
#import "RecieptModel.h"
#import "CashBackLocalModel.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize viewController,navigationController,userModel,isLoading;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
    BOOL isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLoggedIn"];
    if(isLoggedIn) {
        [self loadUserData];
    }
    
    NavigationHandler *navHandler = [[NavigationHandler alloc] initWithMainWindow:self.window];
    [navHandler loadSplashAnimaion];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Load User Data and Delegates

- (void) loadUserData {
    
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    NSString *urlString = [NSString stringWithFormat:@"http://www.saivian-squad.com/merchant/fetchCashBackLocal?customer_id=%@",strUserId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    request = [ASIFormDataRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
    //[req setPostValue:strUserId forKey:@"customer_id"];
    
    [request setRequestMethod:@"GET"];
    [request setTimeOutSeconds:300];
    [request setDelegate:self];
    [request startAsynchronous];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
    int success = [[result objectForKey:@"status"] intValue];
    
    if(success == 1) {
        
        int monthCashBack = [[result objectForKey:@"month_cashback"] intValue];
        int yearCashBack = [[result objectForKey:@"year_cashback"] intValue];
        
        NSString *strMonthCashBack = [NSString stringWithFormat:@"%d",monthCashBack];
        NSString *strYearCashBack = [NSString stringWithFormat:@"%d",yearCashBack];
        
        [[NSUserDefaults standardUserDefaults] setObject:strMonthCashBack forKey:@"monthCashBack"];
        [[NSUserDefaults standardUserDefaults] setObject:strYearCashBack forKey:@"yearCashBack"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSDictionary *user = [result objectForKey:@"customer_data"];
        
        self.userModel = [[UserModel alloc] init];
        
        self.userModel.userID = [user objectForKey:@"id"];
        self.userModel.referral_id = [user objectForKey:@"referral_id"];
        self.userModel.first_name = [user objectForKey:@"first_name"];
        self.userModel.last_name = [user objectForKey:@"last_name"];
        self.userModel.email = [user objectForKey:@"email"];
        self.userModel.password = [user objectForKey:@"password"];
        self.userModel.profile_image = [user objectForKey:@"profile_image"];
        self.userModel.gender = [user objectForKey:@"gender"];
        self.userModel.dob = [user objectForKey:@"dob"];
        self.userModel.city = [user objectForKey:@"city"];
        self.userModel.state = [user objectForKey:@"state"];
        self.userModel.zip = [user objectForKey:@"zip"];
        self.userModel.country = [user objectForKey:@"country"];
        self.userModel.phone_no = [user objectForKey:@"phone_no"];
        self.userModel.is_verified = [user objectForKey:@"is_verified"];
        self.userModel.is_active = [user objectForKey:@"is_active"];
        self.userModel.created_date = [user objectForKey:@"created_date"];
        self.userModel.saivian_id = [user objectForKey:@"saivian_id"];
        
        self.userModel.cashBackLocalArray = [[NSMutableArray alloc] init];
        
        NSArray *cashBackData = [result objectForKey:@"cash_back_data"];
        
        for(int i=0; i<cashBackData.count; i++) {
            CashBackLocalModel *cblModel = [[CashBackLocalModel alloc] init];
            NSDictionary *tempDict = [cashBackData objectAtIndex:i];
            
            cblModel.cashBackId = [tempDict objectForKey:@"id"];
            cblModel.categoryName = [tempDict objectForKey:@"category_name"];
            cblModel.cashBackDescription = [tempDict objectForKey:@"description"];
            cblModel.image = [tempDict objectForKey:@"image"];
            cblModel.isActive = [[tempDict objectForKey:@"is_active"] boolValue];
            cblModel.storesList = [[NSMutableArray alloc] init];
            
            NSArray *storesData = [tempDict objectForKey:@"stores_list"];
            
            for(int j=0; j<storesData.count; j++) {
                NSDictionary *storeDict = [storesData objectAtIndex:j];
                
                StoreModel *sModel = [[StoreModel alloc] init];
                
                sModel.storeId = [storeDict objectForKey:@"store_id"];
                sModel.storeName = [storeDict objectForKey:@"store_name"];
                sModel.firstName = [storeDict objectForKey:@"first_name"];
                sModel.lastName = [storeDict objectForKey:@"last_name"];
                sModel.storeDescription = [storeDict objectForKey:@"description"];
                sModel.email = [storeDict objectForKey:@"email"];
                sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
                sModel.webUrl = [storeDict objectForKey:@"web_url"];
                sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
                sModel.profileImage = [storeDict objectForKey:@"profile_image"];
                sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
                
                NSDictionary *locationDict = [storeDict objectForKey:@"location"];
                
                sModel.address = [locationDict objectForKey:@"address"];
                sModel.city = [locationDict objectForKey:@"city"];
                sModel.state = [locationDict objectForKey:@"state"];
                sModel.zip = [locationDict objectForKey:@"zip"];
                sModel.country = [locationDict objectForKey:@"country"];
                sModel.latitude = [locationDict objectForKey:@"latitude"];
                sModel.longitude = [locationDict objectForKey:@"longitude"];
                
                
                sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
                
                
                [cblModel.storesList addObject:sModel];
            }
            [self.userModel.cashBackLocalArray addObject:cblModel];
        }
        
        
        self.userModel.topTenStores = [[NSMutableArray alloc] init];
        
        NSArray *topTenStoresData = [result objectForKey:@"top_ten_stores"];
        
        for(int i=0; i<topTenStoresData.count; i++) {
            NSDictionary *storeDict = [topTenStoresData objectAtIndex:i];
            
            StoreModel *sModel = [[StoreModel alloc] init];
            
            sModel.storeId = [storeDict objectForKey:@"store_id"];
            sModel.storeName = [storeDict objectForKey:@"store_name"];
            sModel.firstName = [storeDict objectForKey:@"first_name"];
            sModel.lastName = [storeDict objectForKey:@"last_name"];
            sModel.storeDescription = [storeDict objectForKey:@"description"];
            sModel.email = [storeDict objectForKey:@"email"];
            sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
            sModel.webUrl = [storeDict objectForKey:@"web_url"];
            sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
            sModel.profileImage = [storeDict objectForKey:@"profile_image"];
            sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
            
            NSDictionary *locationDict = [storeDict objectForKey:@"location"];
            
            sModel.address = [locationDict objectForKey:@"address"];
            sModel.city = [locationDict objectForKey:@"city"];
            sModel.state = [locationDict objectForKey:@"state"];
            sModel.zip = [locationDict objectForKey:@"zip"];
            sModel.country = [locationDict objectForKey:@"country"];
            sModel.latitude = [locationDict objectForKey:@"latitude"];
            sModel.longitude = [locationDict objectForKey:@"longitude"];
            
            
            sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
            
            [self.userModel.topTenStores addObject:sModel];
        }
        
        self.userModel.favouriteStores = [[NSMutableArray alloc] init];
        
        NSArray *favouriteStoresData = [result objectForKey:@"favourite_stores"];
        
        for(int i=0; i<favouriteStoresData.count; i++) {
            NSDictionary *storeDict = [favouriteStoresData objectAtIndex:i];
            
            StoreModel *sModel = [[StoreModel alloc] init];
            
            sModel.storeId = [storeDict objectForKey:@"store_id"];
            sModel.storeName = [storeDict objectForKey:@"store_name"];
            sModel.firstName = [storeDict objectForKey:@"first_name"];
            sModel.lastName = [storeDict objectForKey:@"last_name"];
            sModel.storeDescription = [storeDict objectForKey:@"description"];
            sModel.email = [storeDict objectForKey:@"email"];
            sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
            sModel.webUrl = [storeDict objectForKey:@"web_url"];
            sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
            sModel.profileImage = [storeDict objectForKey:@"profile_image"];
            sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
            
            NSDictionary *locationDict = [storeDict objectForKey:@"location"];
            
            sModel.address = [locationDict objectForKey:@"address"];
            sModel.city = [locationDict objectForKey:@"city"];
            sModel.state = [locationDict objectForKey:@"state"];
            sModel.zip = [locationDict objectForKey:@"zip"];
            sModel.country = [locationDict objectForKey:@"country"];
            sModel.latitude = [locationDict objectForKey:@"latitude"];
            sModel.longitude = [locationDict objectForKey:@"longitude"];
            
            
            sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
            
            [self.userModel.favouriteStores addObject:sModel];
        }
        
        self.userModel.allStores = [[NSMutableArray alloc] init];
        
        NSArray *allStoresData = [result objectForKey:@"all_stores_list"];
        
        for(int i=0; i<allStoresData.count; i++) {
            NSDictionary *storeDict = [allStoresData objectAtIndex:i];
            
            StoreModel *sModel = [[StoreModel alloc] init];
            
            sModel.storeId = [storeDict objectForKey:@"store_id"];
            sModel.storeName = [storeDict objectForKey:@"store_name"];
            sModel.firstName = [storeDict objectForKey:@"first_name"];
            sModel.lastName = [storeDict objectForKey:@"last_name"];
            sModel.storeDescription = [storeDict objectForKey:@"description"];
            sModel.email = [storeDict objectForKey:@"email"];
            sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
            sModel.webUrl = [storeDict objectForKey:@"web_url"];
            sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
            sModel.profileImage = [storeDict objectForKey:@"profile_image"];
            sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
            
            NSDictionary *locationDict = [storeDict objectForKey:@"location"];
            
            sModel.address = [locationDict objectForKey:@"address"];
            sModel.city = [locationDict objectForKey:@"city"];
            sModel.state = [locationDict objectForKey:@"state"];
            sModel.zip = [locationDict objectForKey:@"zip"];
            sModel.country = [locationDict objectForKey:@"country"];
            sModel.latitude = [locationDict objectForKey:@"latitude"];
            sModel.longitude = [locationDict objectForKey:@"longitude"];
            
            
            sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
            
            [self.userModel.allStores addObject:sModel];
        }
        
        self.userModel.recieptArray = [[NSMutableArray alloc] init];
        
        NSArray *recieptsData = [result objectForKey:@"receipts_list"];
        
        for(int i=0; i<recieptsData.count; i++) {
            NSDictionary *recieptDict = [recieptsData objectAtIndex:i];
            
            RecieptModel *rModel = [[RecieptModel alloc] init];
            
            rModel.recieptId = [recieptDict objectForKey:@"receipt_id"];
            rModel.storeName = [recieptDict objectForKey:@"store_name"];
            rModel.recieptType = [recieptDict objectForKey:@"receipt_type"];
            rModel.purchasedDate = [recieptDict objectForKey:@"purchased_date"];
            
            int subTotal = [[recieptDict objectForKey:@"subtotal_amount"] intValue];
            
            rModel.subtotalAmount = [NSString stringWithFormat:@"%d",subTotal];
            rModel.uploadedDate = [recieptDict objectForKey:@"uploaded_date"];
            
            [self.userModel.recieptArray addObject:rModel];
        }
        
        NSDictionary *storeDict = (NSDictionary*)[result objectForKey:@"payment_info"];
        self.userModel.paymentInfoModel = [[PaymentMethod alloc] init];
        
        self.userModel.paymentInfoModel.cardHolderName = [storeDict objectForKey:@"card_holder_name"];
        self.userModel.paymentInfoModel.cardType = [storeDict objectForKey:@"card_type"];
        self.userModel.paymentInfoModel.cardNumber = [storeDict objectForKey:@"card_number"];
        self.userModel.paymentInfoModel.cvc = [storeDict objectForKey:@"cvc"];
        self.userModel.paymentInfoModel.expiryMonth = [storeDict objectForKey:@"expiry_month"];
        self.userModel.paymentInfoModel.expiryYear = [storeDict objectForKey:@"expiry_year"];
        
        
        
        [[NSUserDefaults standardUserDefaults] setObject:self.userModel.userID forKey:@"userID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    self.isLoading = false;
}

- (void)requestFailed:(ASIHTTPRequest *)theRequest {
    self.isLoading = false;
    NSString *response = [[NSString alloc] initWithData:[theRequest responseData] encoding:NSUTF8StringEncoding];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Something went wrong!!!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
    
}

@end
