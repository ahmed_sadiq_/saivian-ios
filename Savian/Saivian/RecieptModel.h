//
//  RecieptModel.h
//  Saivian
//
//  Created by Ahmed Sadiq on 30/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecieptModel : NSObject
@property (nonatomic, retain) NSString *recieptId;
@property (nonatomic, retain) NSString *storeName;
@property (nonatomic, retain) NSString *recieptType;
@property (nonatomic, retain) NSString *purchasedDate;
@property (nonatomic, retain) NSString *subtotalAmount;
@property (nonatomic, retain) NSString *uploadedDate;

@end
