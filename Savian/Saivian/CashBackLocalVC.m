//
//  CashBackLocalVC.m
//  Saivian
//
//  Created by Ahmed Sadiq on 11/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CashBackLocalVC.h"
#import "DrawerVC.h"
#import "DealCell.h"
#import "DealsModel.h"
#import "DealCellExpanded.h"
#import "DealCellLocationExpanded.h"
#import "DealCellHOOExpanded.h"
#import "CashBackLocalModel.h"
#import "InstantSavingCell.h"
#import "NavigationHandler.h"
#import "Constants.h"
#import "CustomLoading.h"
#import "InstantSavingModel.h"
#import "InstantSavingCategory.h"
#import "InstantSavingOffer.h"
#import <QuartzCore/QuartzCore.h>

@interface CashBackLocalVC ()

@end

@implementation CashBackLocalVC
@synthesize isInstantSaving,locationManager;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    // Do any additional setup after loading the view from its nib.
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    _lowerScroller.contentSize = CGSizeMake(screenWidth*appDel.userModel.cashBackLocalArray.count, 432);
    _upperScroller.contentSize = CGSizeMake(155*appDel.userModel.cashBackLocalArray.count, 0);
    populator = 0;
    
    if(isInstantSaving){
        
        _lowerScroller.contentSize = CGSizeMake(screenWidth*appDel.instantSavingModel.categoriesList.count, 432);
        _upperScroller.contentSize = CGSizeMake(155*appDel.instantSavingModel.categoriesList.count, 0);
        
        cbImg.image = [UIImage imageNamed:@"cashbacklocal_icon.png"];
        isImg.image = [UIImage imageNamed:@"instantsaving_icon_active.png"];
        
        [_cashbackBtn setBackgroundImage:[UIImage imageNamed:@"top_nav_bar.png"] forState:UIControlStateNormal];
        [_instantBtn setBackgroundImage:[UIImage imageNamed:@"top_menu_bar.png"] forState:UIControlStateNormal];
        
        if(!appDel.instantSavingModel) {
            [self sendInstantSavingCall];
        }
        else{
            [self prepareLowerScrollerForInstantSaving];
            [self populateLowerViewForInstantView];
            [self populateTableViewsForInstantSaving];
        }
    }
    
    [self CurrentLocationIdentifier];
}

- (void)viewDidLayoutSubviews {
    // VC just laid off its views
    
    tableViewWidth = _lowerScroller.frame.size.width;
    tableViewHeight = _lowerScroller.frame.size.height;
    
    tableViewX = 0;
    tableViewY = 0;
    
    if(isInstantSaving) {
        _dataDictionary = [[NSMutableDictionary alloc] init];
        
        for(int i=0; i<appDel.instantSavingModel.categoriesList.count; i++) {
            InstantSavingCategory *cdlModel = [appDel.instantSavingModel.categoriesList objectAtIndex:i];
            
            for(int j=0; j<cdlModel.offersList.count; j++) {
                InstantSavingOffer *sModel = [cdlModel.offersList objectAtIndex:j];
                sModel.belongTo = i;
                sModel.touchState = 0;
            }
            
            NSString *keyStr = [NSString stringWithFormat:@"array%d",i];
            [_dataDictionary setObject:cdlModel.offersList forKey:keyStr];
        }
        
        [self prepareLowerScrollerForInstantSaving];
        [self populateLowerViewForInstantView];
        [self populateTableViewsForInstantSaving];
    }
    else {
        _dataDictionary = [[NSMutableDictionary alloc] init];
        
        for(int i=0; i<appDel.userModel.cashBackLocalArray.count; i++) {
            CashBackLocalModel *cdlModel = [appDel.userModel.cashBackLocalArray objectAtIndex:i];
            
            for(int j=0; j<cdlModel.storesList.count; j++) {
                StoreModel *sModel = [cdlModel.storesList objectAtIndex:j];
                sModel.belongTo = i;
                sModel.touchState = 0;
            }
            
            NSString *keyStr = [NSString stringWithFormat:@"array%d",i];
            [_dataDictionary setObject:cdlModel.storesList forKey:keyStr];
        }
        
        [self populateTableViews];
        [self populateLowerView];
    }
    
    if(_tabControl == nil) {
        _tabControl = [[SavianTabController alloc] showTabController:self.lowerBar andFrame:_lowerBar.frame];
        _tabControl.direction = @"down";
        _tabControl.delegate = self;
    }
    
    
}

- (void) populateLowerViewForInstantView {
    
    NSArray *viewsToRemove = [self.upperScroller subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
        
    }
    
    int x=10;
    int y=20;
    
    _buttonsList = [[NSMutableArray alloc] init];
    
    for(int i=0; i< appDel.instantSavingModel.categoriesList.count; i++) {
        InstantSavingCategory *isCategory = [appDel.instantSavingModel.categoriesList objectAtIndex:i];
        UIButton *tabButton = [[UIButton alloc] initWithFrame:CGRectMake(x, y, 135, 30)];
        tabButton.tag = i+200;
        if(i==0) {
            [tabButton setTitleColor:[UIColor colorWithRed:33/255.0 green:86/255.0 blue:130/255.0 alpha:1.0] forState:UIControlStateNormal];
        }
        else {
            [tabButton setTitleColor:[UIColor colorWithRed:47/255.0 green:143/255.0 blue:207/255.0 alpha:1.0] forState:UIControlStateNormal];
        }
        
        
        [tabButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14.0]];
        
        [tabButton addTarget:self
                      action:@selector(tabBarPressed:)
            forControlEvents:UIControlEventTouchUpInside];
        [tabButton setTitle:isCategory.category_name forState:UIControlStateNormal];
        [_buttonsList addObject:tabButton];
        [_upperScroller addSubview:tabButton];
        
        x = x+150;
        
    }
}

- (void) populateLowerView {
    
    NSArray *viewsToRemove = [self.upperScroller subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
        
    }
    
    int x=0;
    int y=20;
    
    _buttonsList = [[NSMutableArray alloc] init];
    
    for(int i=0; i< appDel.userModel.cashBackLocalArray.count; i++) {
        CashBackLocalModel *cblModel = [appDel.userModel.cashBackLocalArray objectAtIndex:i];
        UIButton *tabButton = [[UIButton alloc] initWithFrame:CGRectMake(x, y, 136, 30)];
        tabButton.tag = i+200;
        if(i==0) {
            [tabButton setTitleColor:[UIColor colorWithRed:33/255.0 green:86/255.0 blue:130/255.0 alpha:1.0] forState:UIControlStateNormal];
        }
        else {
            [tabButton setTitleColor:[UIColor colorWithRed:47/255.0 green:143/255.0 blue:207/255.0 alpha:1.0] forState:UIControlStateNormal];
        }
        
        
        [tabButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
        
        [tabButton addTarget:self
                      action:@selector(tabBarPressed:)
            forControlEvents:UIControlEventTouchUpInside];
        [tabButton setTitle:cblModel.categoryName forState:UIControlStateNormal];
        [_buttonsList addObject:tabButton];
        [_upperScroller addSubview:tabButton];
        
        x = x+150;
        
    }
}

- (void) populateTableViews {
    for (; populator<appDel.userModel.cashBackLocalArray.count; populator++){
        
        UITableView *tblView = [[UITableView alloc] initWithFrame:CGRectMake(tableViewX, tableViewY, tableViewWidth, tableViewHeight) style:UITableViewStylePlain];
        
        
        
        tblView.tag = populator+1000;
        tblView.delegate = self;
        tblView.dataSource = self;
        tblView.backgroundColor = [UIColor clearColor];
        tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *ithView = (UIView*)[_lowerScroller viewWithTag:populator+100];
        [ithView addSubview:tblView];
        
        
    }
}

- (void) populateTableViewsForInstantSaving {
    for (; populator<appDel.instantSavingModel.categoriesList.count; populator++){
        
        UITableView *tblView = [[UITableView alloc] initWithFrame:CGRectMake(tableViewX, tableViewY, tableViewWidth, tableViewHeight) style:UITableViewStylePlain];
        
        tblView.tag = populator+1000;
        tblView.delegate = self;
        tblView.dataSource = self;
        tblView.backgroundColor = [UIColor clearColor];
        tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *ithView = (UIView*)[_lowerScroller viewWithTag:populator+100];
        [ithView addSubview:tblView];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)sliderPressed:(id)sender {
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (IBAction)tabBarPressed:(id)sender {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    UIButton *btnSender = (UIButton*)sender;
    
    [btnSender setTitleColor:[UIColor colorWithRed:33/255.0 green:86/255.0 blue:130/255.0 alpha:1.0] forState:UIControlStateNormal];
    
    if(isInstantSaving){
        for(int i=0; i< appDel.instantSavingModel.categoriesList.count; i++) {
            UIButton *btnTemp = (UIButton*)[_buttonsList objectAtIndex:i];
            if(i != (btnSender.tag-200)) {
                
                [btnTemp setTitleColor:[UIColor colorWithRed:47/255.0 green:143/255.0 blue:207/255.0 alpha:1.0] forState:UIControlStateNormal];
            }
        }
    }
    else {
        for(int i=0; i< appDel.userModel.cashBackLocalArray.count; i++) {
            UIButton *btnTemp = (UIButton*)[_buttonsList objectAtIndex:i];
            if(i != (btnSender.tag-200)) {
                
                [btnTemp setTitleColor:[UIColor colorWithRed:47/255.0 green:143/255.0 blue:207/255.0 alpha:1.0] forState:UIControlStateNormal];
            }
        }
    }
    _lowerScroller.contentOffset = CGPointMake(((btnSender.tag)-200)*screenWidth, 0);
}

- (IBAction)cbPressed:(id)sender {
    
    cbImg.image = [UIImage imageNamed:@"cashbacklocal_icon_active.png"];
    isImg.image = [UIImage imageNamed:@"instantsaving_icon.png"];
    
    [_cashbackBtn setBackgroundImage:[UIImage imageNamed:@"top_menu_bar.png"] forState:UIControlStateNormal];
    [_instantBtn setBackgroundImage:[UIImage imageNamed:@"top_nav_bar.png"] forState:UIControlStateNormal];
    
    isInstantSaving = false;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    _lowerScroller.contentSize = CGSizeMake(screenWidth*appDel.userModel.cashBackLocalArray.count, 432);
    _upperScroller.contentSize = CGSizeMake(155*appDel.userModel.cashBackLocalArray.count, 0);
    
    _dataDictionary = [[NSMutableDictionary alloc] init];
    
    for(int i=0; i<appDel.userModel.cashBackLocalArray.count; i++) {
        CashBackLocalModel *cdlModel = [appDel.userModel.cashBackLocalArray objectAtIndex:i];
        
        for(int j=0; j<cdlModel.storesList.count; j++) {
            StoreModel *sModel = [cdlModel.storesList objectAtIndex:j];
            sModel.belongTo = i;
            sModel.touchState = 0;
        }
        
        NSString *keyStr = [NSString stringWithFormat:@"array%d",i];
        [_dataDictionary setObject:cdlModel.storesList forKey:keyStr];
    }
    
    [self populateLowerView];
    [self prepareLowerScroller];
}

- (IBAction)isPressed:(id)sender {
    cbImg.image = [UIImage imageNamed:@"cashbacklocal_icon.png"];
    isImg.image = [UIImage imageNamed:@"instantsaving_icon_active.png"];
    
    [_cashbackBtn setBackgroundImage:[UIImage imageNamed:@"top_nav_bar.png"] forState:UIControlStateNormal];
    [_instantBtn setBackgroundImage:[UIImage imageNamed:@"top_menu_bar.png"] forState:UIControlStateNormal];
    
    isInstantSaving = true;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    _lowerScroller.contentSize = CGSizeMake(screenWidth*appDel.instantSavingModel.categoriesList.count, 432);
    _upperScroller.contentSize = CGSizeMake(155*appDel.instantSavingModel.categoriesList.count, 0);
    
    _dataDictionary = [[NSMutableDictionary alloc] init];
    
    for(int i=0; i<appDel.instantSavingModel.categoriesList.count; i++) {
        InstantSavingCategory *cdlModel = [appDel.instantSavingModel.categoriesList objectAtIndex:i];
        
        for(int j=0; j<cdlModel.offersList.count; j++) {
            InstantSavingOffer *sModel = [cdlModel.offersList objectAtIndex:j];
            sModel.belongTo = i;
            sModel.touchState = 0;
        }
        
        NSString *keyStr = [NSString stringWithFormat:@"array%d",i];
        [_dataDictionary setObject:cdlModel.offersList forKey:keyStr];
    }
    
    if(!appDel.instantSavingModel) {
        [self sendInstantSavingCall];
        [self prepareLowerScrollerForInstantSaving];
    }
    else{
        [self prepareLowerScrollerForInstantSaving];
        [self populateLowerViewForInstantView];
        [self populateTableViewsForInstantSaving];
    }
    
    
    
}
- (void) prepareLowerScrollerForInstantSaving {
    NSArray *viewsToRemove = [self.lowerScroller subviews];
    for (UIView *v in viewsToRemove) {
        if(v.tag >= 100 && v.tag<=105)
        {
            NSArray *viewsToRemove = [v subviews];
            for (UIView *vv in viewsToRemove) {
                [vv removeFromSuperview];
            }
        }
    }
    populator = 0;
    tableViewX = 0;
    tableViewY = 0;
    
    [self populateTableViewsForInstantSaving];
}

- (void) prepareLowerScroller {
    NSArray *viewsToRemove = [self.lowerScroller subviews];
    for (UIView *v in viewsToRemove) {
        if(v.tag >= 100 && v.tag<=105)
        {
            NSArray *viewsToRemove = [v subviews];
            for (UIView *vv in viewsToRemove) {
                [vv removeFromSuperview];
            }
        }
        
    }
    populator = 0;
    tableViewX = 0;
    tableViewY = 0;
    
    [self populateTableViews];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    CGPoint point = scrollView.contentOffset;
    if(scrollView.tag == 12) {
        
        int pageNum = point.x/screenWidth;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        
        if(isInstantSaving){
            for(int i=0; i< appDel.instantSavingModel.categoriesList.count; i++) {
                UIButton *btnTemp = (UIButton*)[_buttonsList objectAtIndex:i];
                if(i != pageNum) {
                    
                    [btnTemp setTitleColor:[UIColor colorWithRed:47/255.0 green:143/255.0 blue:207/255.0 alpha:1.0] forState:UIControlStateNormal];
                }
                else {
                    
                    [btnTemp setTitleColor:[UIColor colorWithRed:33/255.0 green:86/255.0 blue:130/255.0 alpha:1.0] forState:UIControlStateNormal];
                }
            }
        }
        else{
            for(int i=0; i< appDel.userModel.cashBackLocalArray.count; i++) {
                UIButton *btnTemp = (UIButton*)[_buttonsList objectAtIndex:i];
                if(i != pageNum) {
                    
                    [btnTemp setTitleColor:[UIColor colorWithRed:47/255.0 green:143/255.0 blue:207/255.0 alpha:1.0] forState:UIControlStateNormal];
                }
                else {
                    
                    [btnTemp setTitleColor:[UIColor colorWithRed:33/255.0 green:86/255.0 blue:130/255.0 alpha:1.0] forState:UIControlStateNormal];
                }
            }
        }
        
        
        
        _upperScroller.contentOffset = CGPointMake(140*pageNum, 0);
        [UIView commitAnimations];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    int ithIndex = (int)tableView.tag-1000;
    
    if(!isInstantSaving) {
        CashBackLocalModel *cblModel = [appDel.userModel.cashBackLocalArray objectAtIndex:ithIndex];
        return cblModel.storesList.count;
    }
    else {
        InstantSavingCategory *isCategory = [appDel.instantSavingModel.categoriesList objectAtIndex:ithIndex];
        return isCategory.offersList.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!isInstantSaving) {
        
        NSString *keyStr = [NSString stringWithFormat:@"array%d",(int)tableView.tag-1000];
        
        NSMutableArray *tempArray = (NSMutableArray*)[_dataDictionary objectForKey:keyStr];
        
        StoreModel *dmModel = [tempArray objectAtIndex:indexPath.row];
        
        if(dmModel.touchState == 0) {
            DealCell *cell = (DealCell *)[tableView dequeueReusableCellWithIdentifier:nil];
            if (cell == nil)
            {
                NSArray *nib;
                if(IS_IPAD){
                    nib = [[NSBundle mainBundle] loadNibNamed:@"DealCell_iPad" owner:self options:nil];
                }
                else{
                    nib = [[NSBundle mainBundle] loadNibNamed:@"DealCell" owner:self options:nil];
                }
                
                cell = [nib objectAtIndex:0];
            }
            
            cell.imgView.imageURL = [NSURL URLWithString:dmModel.profileImage];
            NSURL *url = [NSURL URLWithString:dmModel.profileImage];
            [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            [tableView setContentSize:CGSizeMake(tableView.contentSize.width, tableView.contentSize.height+20)];
            
            return cell;
        }
        else if(dmModel.touchState == 1) {
            DealCellExpanded *cell = (DealCellExpanded *)[tableView dequeueReusableCellWithIdentifier:nil];
            if (cell == nil)
            {
                NSArray *nib ;
                if(IS_IPAD){
                   nib = [[NSBundle mainBundle] loadNibNamed:@"DealCellExpanded_iPad" owner:self options:nil];
                }else{
                    nib = [[NSBundle mainBundle] loadNibNamed:@"DealCellExpanded" owner:self options:nil];
                }
                
                cell = [nib objectAtIndex:0];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if(dmModel.isFavourite) {
                [cell.heartBtn setImage:[UIImage imageNamed:@"fave_icon_active.png"] forState:UIControlStateNormal];
                
            }
            else{
                [cell.heartBtn setImage:[UIImage imageNamed:@"fave_icon.png"] forState:UIControlStateNormal];
            }
            
            cell.imgView.imageURL = [NSURL URLWithString:dmModel.profileImage];
            NSURL *url = [NSURL URLWithString:dmModel.profileImage];
            [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            [cell.locationBtn addTarget:self
                                 action:@selector(locationBtnPressed:)
                       forControlEvents:UIControlEventTouchUpInside];
            cell.locationBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.locationBtn.tag = indexPath.row;
            
            [cell.callBtn addTarget:self
                             action:@selector(callBtnPressed:)
                   forControlEvents:UIControlEventTouchUpInside];
            cell.callBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.callBtn.tag = indexPath.row;
            
            [cell.webBtn addTarget:self
                             action:@selector(webBtnPressed:)
                   forControlEvents:UIControlEventTouchUpInside];
            cell.webBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.webBtn.tag = indexPath.row;
            
            [cell.hoursBtn addTarget:self
                              action:@selector(hoursOfOperationBtnPressed:)
                    forControlEvents:UIControlEventTouchUpInside];
            cell.hoursBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.hoursBtn.tag = indexPath.row;
            
            [cell.heartBtn addTarget:self
                              action:@selector(heartPressed:)
                    forControlEvents:UIControlEventTouchUpInside];
            cell.heartBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.heartBtn.tag = indexPath.row;
            
            
            cell.titleLbl.text = dmModel.storeName;
            cell.descLbl.text = dmModel.storeDescription;
            cell.addressLbl.text = [NSString stringWithFormat:@"Address: %@",dmModel.address];
            cell.addressLbl2.text = [NSString stringWithFormat:@"%@, %@",dmModel.city, dmModel.country];
            cell.phoneLbl.text = dmModel.phoneNo;
            cell.stateLbl.text = dmModel.webUrl;
            
            [tableView setContentSize:CGSizeMake(tableView.contentSize.width, tableView.contentSize.height+20)];
            
            return cell;
        }
        else if(dmModel.touchState == 2) {
            DealCellLocationExpanded *cell = (DealCellLocationExpanded *)[tableView dequeueReusableCellWithIdentifier:nil];
            if (cell == nil)
            {
                NSArray *nib;
                if(IS_IPAD){
                    nib = [[NSBundle mainBundle] loadNibNamed:@"DealCellLocationExpanded_iPad" owner:self options:nil];
                }
                else{
                    nib = [[NSBundle mainBundle] loadNibNamed:@"DealCellLocationExpanded" owner:self options:nil];
                }
                
                cell = [nib objectAtIndex:0];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if(dmModel.isFavourite) {
                [cell.heartBtn setImage:[UIImage imageNamed:@"fave_icon_active.png"] forState:UIControlStateNormal];
                
            }
            else{
                [cell.heartBtn setImage:[UIImage imageNamed:@"fave_icon.png"] forState:UIControlStateNormal];
            }
            
            [cell.heartBtn addTarget:self
                              action:@selector(heartPressed:)
                    forControlEvents:UIControlEventTouchUpInside];
            cell.heartBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.heartBtn.tag = indexPath.row;
            
            [cell.hoursBtn addTarget:self
                              action:@selector(hoursOfOperationBtnPressed:)
                    forControlEvents:UIControlEventTouchUpInside];
            cell.hoursBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.hoursBtn.tag = indexPath.row;
            
            [cell.webBtn addTarget:self
                            action:@selector(webBtnPressed:)
                  forControlEvents:UIControlEventTouchUpInside];
            cell.webBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.webBtn.tag = indexPath.row;
            
            [cell.callBtn addTarget:self
                             action:@selector(callBtnPressed:)
                   forControlEvents:UIControlEventTouchUpInside];
            cell.callBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.callBtn.tag = indexPath.row;
            
            cell.titleLbl.text = dmModel.storeName;
            cell.descLbl.text = dmModel.storeDescription;
            cell.addressLbl.text = [NSString stringWithFormat:@"Address: %@",dmModel.address];
            cell.addressLbl2.text = [NSString stringWithFormat:@"%@, %@",dmModel.city, dmModel.country];
            cell.phoneLbl.text = dmModel.phoneNo;
            cell.stateLbl.text = dmModel.webUrl;
            
            CLLocationCoordinate2D yourPos = CLLocationCoordinate2DMake([dmModel.latitude doubleValue], [dmModel.longitude doubleValue]);
            
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(yourPos, 800, 800);
            [cell.map setRegion:[cell.map regionThatFits:region] animated:YES];
            
            [tableView setContentSize:CGSizeMake(tableView.contentSize.width, tableView.contentSize.height+20)];
            
            return cell;
        }
        else{
            //DealCellHOOExpanded
            
            DealCellHOOExpanded *cell = (DealCellHOOExpanded *)[tableView dequeueReusableCellWithIdentifier:nil];
            if (cell == nil)
            {
                NSArray *nib;
                if(IS_IPAD){
                    nib = [[NSBundle mainBundle] loadNibNamed:@"DealCellHOOExpanded_iPad" owner:self options:nil];
                }
                else{
                    nib = [[NSBundle mainBundle] loadNibNamed:@"DealCellHOOExpanded" owner:self options:nil];
                }
                
                cell = [nib objectAtIndex:0];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if(dmModel.isFavourite) {
                [cell.heartBtn setImage:[UIImage imageNamed:@"fave_icon_active.png"] forState:UIControlStateNormal];
                
            }
            else{
                [cell.heartBtn setImage:[UIImage imageNamed:@"fave_icon.png"] forState:UIControlStateNormal];
            }
            
            [cell.locationBtn addTarget:self
                                 action:@selector(locationBtnPressed:)
                       forControlEvents:UIControlEventTouchUpInside];
            cell.locationBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.locationBtn.tag = indexPath.row;
            
            [cell.heartBtn addTarget:self
                              action:@selector(heartPressed:)
                    forControlEvents:UIControlEventTouchUpInside];
            cell.heartBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.heartBtn.tag = indexPath.row;
            
            [cell.webBtn addTarget:self
                            action:@selector(webBtnPressed:)
                  forControlEvents:UIControlEventTouchUpInside];
            cell.webBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.webBtn.tag = indexPath.row;
            
            [cell.callBtn addTarget:self
                             action:@selector(callBtnPressed:)
                   forControlEvents:UIControlEventTouchUpInside];
            cell.callBtn.accessibilityHint = [NSString stringWithFormat:@"%d",(int)tableView.tag];
            cell.callBtn.tag = indexPath.row;
            
            cell.titleLbl.text = dmModel.storeName;
            cell.descLbl.text = dmModel.storeDescription;
            cell.addressLbl.text = [NSString stringWithFormat:@"Address: %@",dmModel.address];
            cell.addressLbl2.text = [NSString stringWithFormat:@"%@, %@",dmModel.city, dmModel.country];
            cell.phoneLbl.text = dmModel.phoneNo;
            cell.stateLbl.text = dmModel.webUrl;
            
            NSDictionary *dictTemp = dmModel.openingHoursDict;
            cell.sundayTime.text = [dictTemp objectForKey:@"sunday"];
            cell.mondayTime.text = [dictTemp objectForKey:@"monday"];
            cell.tuesdayTime.text = [dictTemp objectForKey:@"tuesday"];
            cell.wednesdayTime.text = [dictTemp objectForKey:@"wednesday"];
            cell.thursdayTime.text = [dictTemp objectForKey:@"thursday"];
            cell.firdayTime.text = [dictTemp objectForKey:@"friday"];
            cell.saturdayTime.text = [dictTemp objectForKey:@"saturday"];
            
            [tableView setContentSize:CGSizeMake(tableView.contentSize.width, tableView.contentSize.height+20)];
            
            return cell;
        }
    }
    else {
        
        NSString *keyStr = [NSString stringWithFormat:@"array%d",(int)tableView.tag-1000];
        NSMutableArray *tempArray = (NSMutableArray*)[_dataDictionary objectForKey:keyStr];
        
        InstantSavingOffer *dmModel = [tempArray objectAtIndex:indexPath.row];
        
        InstantSavingCell *cell = (InstantSavingCell *)[tableView dequeueReusableCellWithIdentifier:nil];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"InstantSavingCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.titleLbl.text = dmModel.title;
        
        cell.bgView.layer.borderColor = [UIColor grayColor].CGColor;
        cell.bgView.layer.borderWidth = 1.0f;
        
        cell.offerImg.imageURL = [NSURL URLWithString:dmModel.logo_url];
        NSURL *url = [NSURL URLWithString:dmModel.logo_url];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!isInstantSaving) {
        NSString *keyStr = [NSString stringWithFormat:@"array%d",(int)tableView.tag-1000];
        NSMutableArray *tempArray = (NSMutableArray*)[_dataDictionary objectForKey:keyStr];
        
        StoreModel *dmModel = [tempArray objectAtIndex:indexPath.row];
        
        if(dmModel.touchState == 0) {
            if(IS_IPAD){
                return 500;
            }
            return 220;
        }
        else {
            if(IS_IPAD){
                return 800;
            }
            return 450;
        }
    }
    else{
        return 120;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(!isInstantSaving) {
        NSString *keyStr = [NSString stringWithFormat:@"array%d",(int)tableView.tag-1000];
        NSMutableArray *tempArray = (NSMutableArray*)[_dataDictionary objectForKey:keyStr];
        
        StoreModel *dmModel = [tempArray objectAtIndex:indexPath.row];
        
        if(dmModel.touchState == 0) {
            dmModel.touchState = 1;
            NSArray *indexPaths = @[indexPath];
            [tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
            
        }
        else if (dmModel.touchState >= 1) {
            dmModel.touchState = 0;
            NSArray *indexPaths = @[indexPath];
            [tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}


#pragma mark - Expanded Cell Methods

- (void) locationBtnPressed : (id) sender {
    UIButton *btnSender = (UIButton*)sender;
    
    UITableViewCell *buttonCell = (UITableViewCell*)[[btnSender superview] superview];
    UITableView* table = [(UITableView *)[buttonCell superview] superview];
    NSIndexPath* pathOfTheCell = [table indexPathForCell:buttonCell];
    
    
    NSString *keyStr = [NSString stringWithFormat:@"array%d",(int)table.tag-1000];
    NSMutableArray *tempArray = (NSMutableArray*)[_dataDictionary objectForKey:keyStr];
    
    StoreModel *dmModel = [tempArray objectAtIndex:pathOfTheCell.row];
    dmModel.touchState = 2;
    
    NSArray *indexPaths = @[pathOfTheCell];
    [table reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    
    [UIView transitionWithView:self.containerView
                      duration:1.0
                       options:(true ? UIViewAnimationOptionTransitionFlipFromRight :
                                UIViewAnimationOptionTransitionFlipFromLeft)
                    animations: ^{
                        
                    }
     
                    completion:^(BOOL finished) {
                    }];
    
}

- (void) heartPressed : (id) sender {
    UIButton *btnSender = (UIButton*)sender;
    UITableViewCell *buttonCell = (UITableViewCell*)[[btnSender superview] superview];
    UITableView* table = [(UITableView *)[buttonCell superview] superview];
    NSIndexPath* pathOfTheCell = [table indexPathForCell:buttonCell];
    
    
    NSString *keyStr = [NSString stringWithFormat:@"array%d",(int)table.tag-1000];
    NSMutableArray *tempArray = (NSMutableArray*)[_dataDictionary objectForKey:keyStr];
    
    StoreModel *dmModel = [tempArray objectAtIndex:pathOfTheCell.row];
    if(dmModel.isFavourite) {
        dmModel.isFavourite = false;
    }
    else {
        dmModel.isFavourite = true;
    }
    
    sModelSelected = dmModel;
    
    NSArray *indexPaths = @[pathOfTheCell];
    [table reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    
    [self sendFaouriteCall:dmModel.storeId];
    
}

- (void) hoursOfOperationBtnPressed : (id) sender {
    UIButton *btnSender = (UIButton*)sender;
    
    UITableViewCell *buttonCell = (UITableViewCell*)[[btnSender superview] superview];
    UITableView* table = [(UITableView *)[buttonCell superview] superview];
    NSIndexPath* pathOfTheCell = [table indexPathForCell:buttonCell];
    
    
    NSString *keyStr = [NSString stringWithFormat:@"array%d",(int)table.tag-1000];
    NSMutableArray *tempArray = (NSMutableArray*)[_dataDictionary objectForKey:keyStr];
    
    StoreModel *dmModel = [tempArray objectAtIndex:pathOfTheCell.row];
    dmModel.touchState = 3;
    
    NSArray *indexPaths = @[pathOfTheCell];
    [table reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    
    [UIView transitionWithView:self.containerView
                      duration:1.0
                       options:(true ? UIViewAnimationOptionTransitionFlipFromRight :
                                UIViewAnimationOptionTransitionFlipFromLeft)
                    animations: ^{
                        
                    }
     
                    completion:^(BOOL finished) {
                    }];
    
}

- (IBAction)callBtnPressed:(id)sender {
    UIButton *btnSender = (UIButton*)sender;
    
    UITableViewCell *buttonCell = (UITableViewCell*)[[btnSender superview] superview];
    UITableView* table = [(UITableView *)[buttonCell superview] superview];
    NSIndexPath* pathOfTheCell = [table indexPathForCell:buttonCell];
    
    
    NSString *keyStr = [NSString stringWithFormat:@"array%d",(int)table.tag-1000];
    NSMutableArray *tempArray = (NSMutableArray*)[_dataDictionary objectForKey:keyStr];
    
    StoreModel *dmModel = [tempArray objectAtIndex:pathOfTheCell.row];
    
    NSString *newString = [[dmModel.phoneNo componentsSeparatedByCharactersInSet:
                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                           componentsJoinedByString:@""];
    
    NSString *phoneNumber = [@"tel://" stringByAppendingString:newString];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

- (IBAction)webBtnPressed:(id)sender {
    UIButton *btnSender = (UIButton*)sender;
    
    UITableViewCell *buttonCell = (UITableViewCell*)[[btnSender superview] superview];
    UITableView* table = [(UITableView *)[buttonCell superview] superview];
    NSIndexPath* pathOfTheCell = [table indexPathForCell:buttonCell];
    
    
    NSString *keyStr = [NSString stringWithFormat:@"array%d",(int)table.tag-1000];
    NSMutableArray *tempArray = (NSMutableArray*)[_dataDictionary objectForKey:keyStr];
    
    StoreModel *dmModel = [tempArray objectAtIndex:pathOfTheCell.row];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:dmModel.webUrl]];
}

#pragma mark - Mark Favourite Server Methods

- (void) sendFaouriteCall : (NSString *) storeID {
    
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://www.saivian-squad.com/stores/addFavouriteStore"];
    request = [ASIFormDataRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
    [request setPostValue:strUserId forKey:@"customer_id"];
    [request setPostValue:storeID forKey:@"store_id"];
    //request.tag = 10;
    
    [request setRequestMethod:@"POST"];
    [request setTimeOutSeconds:300];
    [request setDelegate:self];
    [request startAsynchronous];
}

#pragma mark Instant Saving Helpher Methods

- (BOOL) isCateoryAddedinInstantSavingModel :(NSString *)catName {
    
    for(int i=0; i<appDel.instantSavingModel.categoriesList.count; i++) {
        InstantSavingCategory *isCategory = [appDel.instantSavingModel.categoriesList objectAtIndex:i];
        
        if([isCategory.category_name isEqualToString:catName]) {
            return true;
        }
        
    }
    return false;
}

- (InstantSavingCategory*) returnInstantSavingCategory : (NSString *) catName {
    for(int i=0; i<(int)appDel.instantSavingModel.categoriesList; i++) {
        InstantSavingCategory *isCategory = [appDel.instantSavingModel.categoriesList objectAtIndex:i];
        
        if([isCategory.category_name isEqualToString:catName]) {
            return isCategory;
        }
        
    }
    return nil;
}

#pragma mark Server Request Delegate Methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [CustomLoading DismissAlertMessage];
    if(request.tag == 10){
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
        NSArray *offersArray = [result objectForKey:@"offers"];
        appDel.instantSavingModel = [[InstantSavingModel alloc] init];
        appDel.instantSavingModel.categoriesList = [[NSMutableArray alloc] init];
        
        for(int i=0; i<offersArray.count; i++) {
            NSDictionary *offerDict = (NSDictionary*)[offersArray objectAtIndex:i];
            
            NSArray *catArray = [offerDict objectForKey:@"categories"];
            NSDictionary *dictCategory = [catArray objectAtIndex:0];
            
            NSString *categoryName = [dictCategory objectForKey:@"category_name"];
            
            if(![self isCateoryAddedinInstantSavingModel:categoryName]) {
                InstantSavingCategory *isCategory = [[InstantSavingCategory alloc] init];
                isCategory.category_name = [dictCategory objectForKey:@"category_name"];
                isCategory.category_key = [dictCategory objectForKey:@"category_key"];
                isCategory.category_parent_key = [dictCategory objectForKey:@"category_parent_key"];
                isCategory.category_parent_name = [dictCategory objectForKey:@"category_parent_name"];
                isCategory.category_type = [dictCategory objectForKey:@"category_type"];
                isCategory.offersList = [[NSMutableArray alloc] init];
                
                [appDel.instantSavingModel.categoriesList addObject:isCategory];
                
                
            }
            
            InstantSavingCategory *isCategory = [self returnInstantSavingCategory:categoryName];
            
            InstantSavingOffer *isOffer = [[InstantSavingOffer alloc] init];
            
            isOffer.search_distance = [offerDict objectForKey:@"search_distance"];
            isOffer.offer_key = [offerDict objectForKey:@"offer_key"];
            isOffer.offer_group_key = [offerDict objectForKey:@"offer_group_key"];
            isOffer.title = [offerDict objectForKey:@"title"];
            isOffer.started_on = [offerDict objectForKey:@"started_on"];
            isOffer.expires_on = [offerDict objectForKey:@"expires_on"];
            isOffer.terms_of_use = [offerDict objectForKey:@"terms_of_use"];
            isOffer.logo_url = [offerDict objectForKey:@"logo_url"];
            isOffer.offer_photo_url = [offerDict objectForKey:@"offer_photo_url"];
            isOffer.national_offer = [offerDict objectForKey:@"national_offer"];
            isOffer.uses_per_period = [offerDict objectForKey:@"uses_per_period"];
            isOffer.offer_set = [offerDict objectForKey:@"offer_set"];
            isOffer.savings_amount = [offerDict objectForKey:@"savings_amount"];
            isOffer.minimum_purchase = [offerDict objectForKey:@"minimum_purchase"];
            
            isOffer.maximum_award = [offerDict objectForKey:@"maximum_award"];
            isOffer.offer_value = [offerDict objectForKey:@"offer_value"];
            isOffer.discount_type = [offerDict objectForKey:@"discount_type"];
            isOffer.discount_value = [offerDict objectForKey:@"discount_value"];
            isOffer.promotion_code = [offerDict objectForKey:@"promotion_code"];
            
            
            [isCategory.offersList addObject:isOffer];
        }
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        
        _dataDictionary = [[NSMutableDictionary alloc] init];
        
        for(int i=0; i<appDel.instantSavingModel.categoriesList.count; i++) {
            InstantSavingCategory *cdlModel = [appDel.instantSavingModel.categoriesList objectAtIndex:i];
            
            for(int j=0; j<cdlModel.offersList.count; j++) {
                InstantSavingOffer *sModel = [cdlModel.offersList objectAtIndex:j];
                sModel.belongTo = i;
                sModel.touchState = 0;
            }
            
            NSString *keyStr = [NSString stringWithFormat:@"array%d",i];
            [_dataDictionary setObject:cdlModel.offersList forKey:keyStr];
        }
        
        _lowerScroller.contentSize = CGSizeMake(screenWidth*appDel.instantSavingModel.categoriesList.count, 432);
        _upperScroller.contentSize = CGSizeMake(155*appDel.instantSavingModel.categoriesList.count, 0);
        [self populateLowerViewForInstantView];
        [self populateTableViewsForInstantSaving];
    }
    
    else{
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
        int success = [[result objectForKey:@"status"] intValue];
        
        if(success == 1) {
            
            NSString *message = [result objectForKey:@"message"];
            StoreModel *sModel = sModelSelected;
            
            if([message isEqualToString:@"Store added successfully."]){
                [appDel.userModel.favouriteStores addObject:sModel];
            }
            else{
                for(int i=0; i<appDel.userModel.favouriteStores.count; i++){
                    StoreModel *tempModel = [appDel.userModel.favouriteStores objectAtIndex:i];
                    
                    if ([tempModel.storeId isEqualToString:sModel.storeId]) {
                        [appDel.userModel.favouriteStores removeObject:tempModel];
                        
                        break;
                    }
                }
            }
        }
    }
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)theRequest {
    NSString *response = [[NSString alloc] initWithData:[theRequest responseData] encoding:NSUTF8StringEncoding];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Please try later!!!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
    
}

- (void) SavianTabControllerPressed: (SavianTabController *) sender {
    if([_tabControl.direction isEqualToString:@"down"]) {
        
        _tabControl.direction = @"up";
        
        CGRect lowerBarFrame = _tabControl.frame;
        
        
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            if(IS_IPAD) {
                self.tabControl.frame  = CGRectMake(0, lowerBarFrame.origin.y-100, lowerBarFrame.size.width,lowerBarFrame.size.height);
            }
            else {
                self.tabControl.frame  = CGRectMake(0, lowerBarFrame.origin.y-60, lowerBarFrame.size.width,lowerBarFrame.size.height);
            }
            
            
        } completion:^(BOOL finished) {
            
        }];
    }
    else {
        [[NavigationHandler getInstance] MoveToCameraController];
    }
}

- (void) SavianTabControllerRecieptPressed: (SavianTabController *) sender {
    [[NavigationHandler getInstance] NavigateToRecieptScreen];
}
- (void) SavianTabControllerStorePressed:(SavianTabController *)sender {
    [[NavigationHandler getInstance] NavigateToStoreScreen];
}

#pragma mark Instant Saving Methods

-(void) sendInstantSavingCall {
    [CustomLoading showAlertMessage];
    
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    float latitude = currentLocation.coordinate.latitude;
    float longitude = currentLocation.coordinate.longitude;
    
    NSString *urlString = [NSString stringWithFormat:@"https://offer-demo.adcrws.com/v1/offers?lat=%f&lon=%f&access_token=1842f8c32dbc7530cd6faca1b0025c8ee8ffa5338b4b5ce5b4855790dbed2e6b&member_key=%@",latitude,longitude,strUserId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    request = [ASIFormDataRequest requestWithURL:url];
    request.tag = 10;
    
    [request setRequestMethod:@"GET"];
    [request setTimeOutSeconds:300];
    [request setDelegate:self];
    [request startAsynchronous];
}


#pragma mark - Location Methods

-(void)CurrentLocationIdentifier
{
    //---- For getting current gps location
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    //------
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [self.locationManager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!(error))
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"\nCurrent Location Detected\n");
             NSLog(@"placemark %@",placemark);
             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             NSString *Address = [[NSString alloc]initWithString:locatedAt];
             NSString *Area = [[NSString alloc]initWithString:placemark.locality];
             NSString *Country = [[NSString alloc]initWithString:placemark.country];
             NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
             NSLog(@"%@",CountryArea);
         }
         else
         {
             NSLog(@"Geocode failed with error %@", error);
             NSLog(@"\nCurrent Location Not Detected\n");
             //return;
         }
     }];
}

    
    
    
    
    @end
