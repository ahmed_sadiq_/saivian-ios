//
//  RecieptVC.m
//  Saivian
//
//  Created by Ahmed Sadiq on 23/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "RecieptVC.h"
#import "RecieptCell.h"
#import "RecieptModel.h"
#import "Constants.h"
#import "DrawerVC.h"

@interface RecieptVC ()

@end

@implementation RecieptVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(isSearch) {
        return _searchArray.count;
    }
    else {
        return appDel.userModel.recieptArray.count;
    }
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"RecieptCell";
    
    RecieptCell *cell = (RecieptCell *)[tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil)
    {
        NSArray *nib;
        if(IS_IPAD){
            nib= [[NSBundle mainBundle] loadNibNamed:@"RecieptCell_iPad" owner:self options:nil];
        }
        else{
            nib= [[NSBundle mainBundle] loadNibNamed:@"RecieptCell" owner:self options:nil];
        }
        
        cell = [nib objectAtIndex:0];
    }
    RecieptModel *rModel;
    if(isSearch) {
        rModel = (RecieptModel*)[_searchArray objectAtIndex:indexPath.row];
    }
    else {
        rModel = (RecieptModel*)[appDel.userModel.recieptArray objectAtIndex:indexPath.row];
    }
    
    
    cell.lbl.text = rModel.storeName;
    cell.priceLbl.text = [NSString stringWithFormat:@"$%@",rModel.subtotalAmount];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD){
        return 80;
    }
    else{
        return 44;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
        
        if(textField.text.length > 0) {
            isSearch = true;
            [self startSearch:textField.text];
        }
        else if(isMonthSelected) {
            isSearch = true;
            [self getRecieptsForMonth];
        }
        else {
            isSearch = false;
            [_mainTblView reloadData];;
        }
    }
    
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    // add your method here
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [self.view addGestureRecognizer:tapRecognizer];
    self.view.tag=2;
    
}

- (IBAction)monthPressed:(id)sender {
    
    arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Oct 2015",@"Nov 2015",@"Dec 2015",@"Jan 2016",@"Feb 2016",@"March 2016",@"April 2016",@"May 2016",@"June 2016",@"July 2016",@"Aug 2016",@"Sept 2016",@"Oct 2016",@"Nov 2016",@"Dec 2016",@"Jan 2017",@"Feb 2017",@"March 2017",@"April 2017",nil];
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 400;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)sliderPressed:(id)sender {
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    isMonthSelected = true;
    selectedIndex = sender.selectedIndex;
    [self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
    [self getRecieptsForMonth];
}


- (void) startSearch : (NSString *)stringToSearch {
    _searchArray = [[NSMutableArray alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.storeName contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
    
    NSArray *results = [appDel.userModel.recieptArray filteredArrayUsingPredicate:predicate];
    if(results && results.count > 0) {
        if(isMonthSelected) {
            for(int i=0; i<results.count; i++) {
                
                RecieptModel *rModel = (RecieptModel*)[results objectAtIndex:i];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *date = [dateFormatter dateFromString:rModel.purchasedDate];
                
                NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                [dateFormatter1 setDateFormat:@"MMMM yyyy"];
                NSString *stringDate = [dateFormatter1 stringFromDate:date];
                
                NSString *btnTitle = [arr objectAtIndex:selectedIndex];
                
                if([btnTitle isEqualToString:stringDate]) {
                    [_searchArray addObject:rModel];
                }
            }
        }
        else {
            _searchArray = [[NSMutableArray alloc] initWithArray:results];
        }
    }
    [_mainTblView reloadData];
}

-(void) getRecieptsForMonth {
    isSearch = true;
    _searchArray = [[NSMutableArray alloc] init];
    if(isMonthSelected) {
        for(int i=0; i<appDel.userModel.recieptArray.count; i++) {
            
            RecieptModel *rModel = (RecieptModel*)[appDel.userModel.recieptArray objectAtIndex:i];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [dateFormatter dateFromString:rModel.purchasedDate];
            
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"MMMM yyyy"];
            NSString *stringDate = [dateFormatter1 stringFromDate:date];
            NSString *btnTitle = [arr objectAtIndex:selectedIndex];
            if([btnTitle isEqualToString:stringDate]) {
                [_searchArray addObject:rModel];
            }
        }
    }
    [_mainTblView reloadData];
    NSArray *items = [[arr objectAtIndex:selectedIndex] componentsSeparatedByString:@" "];
    NSString *month = [items objectAtIndex:0];
    NSString *year = [items objectAtIndex:1];
    
    monthsPan.text = [NSString stringWithFormat:@"%@ 1 - %@ %d, %@",month, month, [self getDaysOfMonth:month], year];
}

- (int) getDaysOfMonth : (NSString *) month {
    if([month isEqualToString:@"January"] || [month isEqualToString:@"March"] || [month isEqualToString:@"May"] || [month isEqualToString:@"July"] || [month isEqualToString:@"August"] || [month isEqualToString:@"October"] || [month isEqualToString:@"December"] ) {
        return 31;
    }
    else if ([month isEqualToString:@"January"]) {
        return 28;
    }
    
    return 30;
}
-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender
{
    if(sender.view.tag==2) {
        [self.view endEditing:YES];
    }
    [self.view removeGestureRecognizer:sender];
}
@end
