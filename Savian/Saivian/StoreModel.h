//
//  StoreModel.h
//  Saivian
//
//  Created by Ahmed Sadiq on 26/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreModel : NSObject {
    
}
@property int touchState;
@property int belongTo;
@property BOOL isSelected;
@property (nonatomic, retain) NSString *storeId;
@property (nonatomic, retain) NSString *storeName;
@property (nonatomic, retain) NSString *storeDescription;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *webUrl;
@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSString *phoneNo;
@property (nonatomic, retain) NSString *profileImage;
@property (nonatomic, retain) NSString *featuredImage;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *country;
@property (nonatomic, retain) NSString *state;
@property (nonatomic, retain) NSString *zip;
@property (nonatomic, retain) NSString *latitude;
@property (nonatomic, retain) NSString *longitude;
@property BOOL isFavourite;
@property (strong, retain) NSDictionary *openingHoursDict; // 7 objects of OpeningHours.h


@end
