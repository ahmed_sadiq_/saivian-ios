//
//  ForgotPasswordVC.m
//  Saivian
//
//  Created by Ahmed Sadiq on 06/06/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "CustomLoading.h"

@interface ForgotPasswordVC ()

@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [self.view addGestureRecognizer:tapRecognizer];
    self.view.tag=2;
}

-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender
{
    if(sender.view.tag==2) {
        [self.view endEditing:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)forgotPswdPressed:(id)sender {
    if(_emailAddressTxt.text < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Email or username is not in valid format" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
    else {
        [self sendForgotPasswordCall];
    }
}

- (IBAction)backPressed:(id)sender {
    
    
//    [UIView animateWithDuration:0.5
//                     animations:^{
//                         
//                         [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
//                     }
//                     completion:^(BOOL finished){
//                         [self.navigationController popViewControllerAnimated:false];
//                     }];\

    
    [UIView animateWithDuration:0.75
                     animations:^{
                         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                         [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
                     }];
    [self.navigationController popViewControllerAnimated:NO];
    
}

#pragma mark Forgot Password Server Call

- (void) sendForgotPasswordCall {
    [CustomLoading showAlertMessage];
    
    
    NSURL *url = [NSURL URLWithString:@"http://www.saivian-squad.com/customer/forgotPassword"];
    request = [ASIFormDataRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
    [request setPostValue:[_emailAddressTxt.text lowercaseString] forKey:@"email"];
    
    [request setRequestMethod:@"POST"];
    [request setTimeOutSeconds:300];
    [request setDelegate:self];
    [request startAsynchronous];
}
- (void)requestFinished:(ASIHTTPRequest *)request
{
    [CustomLoading DismissAlertMessage];
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
    int success = [[result objectForKey:@"status"] intValue];
    
    if(success == 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Sent" message:@"Your password sent on your email" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Verification Error" message:@"We are not available at the moment" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}
- (void)requestFailed:(ASIHTTPRequest *)theRequest {
    [CustomLoading DismissAlertMessage];
    NSString *response = [[NSString alloc] initWithData:[theRequest responseData] encoding:NSUTF8StringEncoding];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Something went wrong!!!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
}


#pragma mark Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    // add your method here
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}

-(BOOL)validateEmail:(NSString *)candidate {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}
@end
