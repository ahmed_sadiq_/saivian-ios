//
//  ForgotPasswordVC.h
//  Saivian
//
//  Created by Ahmed Sadiq on 06/06/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"

@interface ForgotPasswordVC : UIViewController <ASIHTTPRequestDelegate> {
    ASIFormDataRequest *request;
}
@property (strong, nonatomic) IBOutlet UITextField *emailAddressTxt;

- (IBAction)forgotPswdPressed:(id)sender;
- (IBAction)backPressed:(id)sender;
@end
