//
//  AddStoreCell.h
//  Saivian
//
//  Created by Ahmed Sadiq on 23/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTAutocompleteTextField.h"
@interface AddStoreCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *zipBtn;
@property (weak, nonatomic) IBOutlet UITextField *stateTxt;
@property (weak, nonatomic) IBOutlet UITextField *phoneNum;
@property (weak, nonatomic) IBOutlet UITextField *storeAddress;
@property (weak, nonatomic) IBOutlet UITextField *storeName;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UITextField *cityTxt;
@property (strong, nonatomic) IBOutlet HTAutocompleteTextField *stName;
@end
