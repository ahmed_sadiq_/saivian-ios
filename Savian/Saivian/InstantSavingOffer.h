//
//  InstantSavingOffer.h
//  Saivian
//
//  Created by Ahmed Sadiq on 19/06/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstantSavingOffer : NSObject
@property (nonatomic, retain) NSString *search_distance;
@property (nonatomic, retain) NSString *offer_key;
@property (nonatomic, retain) NSString *offer_group_key;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *started_on;
@property (nonatomic, retain) NSString *expires_on;
@property (nonatomic, retain) NSString *terms_of_use;
@property (nonatomic, retain) NSString *logo_url;
@property (nonatomic, retain) NSString *offer_photo_url;
@property (nonatomic, retain) NSString *national_offer;
@property (nonatomic, retain) NSString *uses_per_period;
@property (nonatomic, retain) NSString *offer_set;
@property (nonatomic, retain) NSString *savings_amount;
@property (nonatomic, retain) NSString *minimum_purchase;
@property (nonatomic, retain) NSString *maximum_award;
@property (nonatomic, retain) NSString *offer_value;
@property (nonatomic, retain) NSString *discount_type;
@property (nonatomic, retain) NSString *discount_value;
@property (nonatomic, retain) NSString *promotion_code;
@property int touchState;
@property int belongTo;



@end
