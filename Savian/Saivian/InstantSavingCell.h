//
//  InstantSavingCell.h
//  Saivian
//
//  Created by Ahmed Sadiq on 06/06/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface InstantSavingCell : UITableViewCell

@property (strong, nonatomic) IBOutlet AsyncImageView *offerImg;
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@end
