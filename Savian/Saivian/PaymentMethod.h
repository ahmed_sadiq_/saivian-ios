//
//  PaymentMethod.h
//  Saivian
//
//  Created by Ahmed Sadiq on 26/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentMethod : NSObject
@property (nonatomic, retain) NSString *cardHolderName;
@property (nonatomic, retain) NSString *cardType;
@property (nonatomic, retain) NSString *cardNumber;
@property (nonatomic, retain) NSString *cvc;
@property (nonatomic, retain) NSString *expiryMonth;
@property (nonatomic, retain) NSString *expiryYear;

@end
