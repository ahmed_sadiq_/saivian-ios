//
//  InstantSavingCategory.h
//  Saivian
//
//  Created by Ahmed Sadiq on 19/06/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstantSavingCategory : NSObject
@property (nonatomic, retain) NSString *category_key;
@property (nonatomic, retain) NSString *category_name;
@property (nonatomic, retain) NSString *category_parent_key;
@property (nonatomic, retain) NSString *category_parent_name;
@property (nonatomic, retain) NSString *category_type;
@property (strong, retain) NSMutableArray *offersList;
@end
