//
//  CameraControllerVC.m
//  Saivian
//
//  Created by Ahmed Sadiq on 10/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CameraControllerVC.h"
#import "DrawerVC.h"
#import "CustomLoading.h"
#import "RecieptModel.h"
#import "HTAutocompleteManager.h"

#import "MLPAutoCompleteTextField.h"
#import "DEMOCustomAutoCompleteCell.h"
#import "DEMOCustomAutoCompleteObject.h"
#import "DEMODataSource.h"
#import <QuartzCore/QuartzCore.h>


//transform values for full screen support
#define CAMERA_TRANSFORM_X 1
#define CAMERA_TRANSFORM_Y 1.12412


@interface CameraControllerVC ()

@end

@implementation CameraControllerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [_dbPicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_utDobPicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    _dbPicker.maximumDate = [NSDate date];
    _utDobPicker.maximumDate = [NSDate date];
    
    recieptType = @"";
    
    [self.storeName setAutoCompleteTableAppearsAsKeyboardAccessory:NO];
    if ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] != NSOrderedAscending) {
        [self.storeName registerAutoCompleteCellClass:[DEMOCustomAutoCompleteCell class]
                                           forCellReuseIdentifier:@"CustomCellId"];
    }
    else{
        //Turn off bold effects on iOS 5.0 as they are not supported and will result in an exception
        self.storeName.applyBoldEffectToAutoCompleteSuggestions = NO;
    }

}
- (void)viewDidLayoutSubviews{
    // Do any additional setup after loading the view from its nib.
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    AVCaptureDevice *videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (videoDevice)
    {
        NSError *error;
        AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
        if (!error)
        {
            if ([session canAddInput:videoInput])
            {
                [session addInput:videoInput];
                AVCaptureVideoPreviewLayer *previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
                previewLayer.frame = self.view.bounds;
                [self.cameraView.layer addSublayer:previewLayer];
                
                _stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
                [session addOutput:_stillImageOutput];
                
                [session startRunning];
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)datePressed:(id)sender {
    
    _storeName.hidden = true;
    
    _datePickerView.hidden = false;
    
    [self.view endEditing:YES];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [self.view addGestureRecognizer:tapRecognizer];
    self.view.tag=2;
    
    _datePickerView.layer.borderColor = [UIColor grayColor].CGColor;
    _datePickerView.layer.borderWidth = 1.0f;
    
}

- (IBAction)storePressed:(id)sender {
    NSArray *arr = [[NSArray alloc] init];
    
    NSMutableArray *storeNames = [[NSMutableArray alloc] init];
    
    for(int i=0; i< appDel.userModel.allStores.count; i++) {
        StoreModel*sModel = [appDel.userModel.allStores objectAtIndex:i];
        [storeNames addObject:sModel.storeName];
    }
    
    arr = storeNames;
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 400;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    selectedIndex = sender.selectedIndex;
    
    selectedStoreModel = [appDel.userModel.allStores objectAtIndex:selectedIndex];
    
    if(![recieptType isEqualToString:@"TRAVEL"]) {
        _storeName.text = selectedStoreModel.storeName;
    }
    else {
        _airlineTxt.text = selectedStoreModel.storeName;
    }
    
    
    [self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

- (IBAction)sliderBtnPressed:(id)sender {
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (IBAction)cameraPressed:(id)sender {
    
    UIButton *btnSender = (UIButton*)sender;
    
    if(btnSender.tag == 0) {
        if(isTypeSelected) {
                        
            AVCaptureConnection *videoConnection = nil;
            for (AVCaptureConnection *connection in _stillImageOutput.connections){
                for (AVCaptureInputPort *port in [connection inputPorts]){
                    if ([[port mediaType] isEqual:AVMediaTypeVideo]){
                        videoConnection = connection;
                        break;
                    }
                }
                if (videoConnection) {
                    break;
                }
            }
            
            [_stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                if(imageDataSampleBuffer) {
                    NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                    
                    UIImage *image = [[UIImage alloc]initWithData:imageData];
                    _capturedImg.image = image;
                    _capturedImg.hidden = false;
                    _confirmBar.hidden = false;
                    btnSender.tag = 1;
                    
                    [_cameraBtn setBackgroundImage:[UIImage imageNamed:@"retake_icon_active@3x.png"] forState:UIControlStateNormal];
                }
            }];
        }
        else {
            _selectTypeImg.hidden = false;
        }
    }
    else {
        btnSender.tag = 0;
        _capturedImg.hidden = true;
        _confirmBar.hidden = true;
        [_cameraBtn setBackgroundImage:[UIImage imageNamed:@"camera_icon.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)shoppingPressed:(id)sender {
    isTypeSelected = true;
    _selectTypeImg.hidden = true;
    UIButton *btnSender = (UIButton*)sender;
    if(btnSender.tag == 0) {
        btnSender.tag = 1;
        _travelBtn.tag = 0;
        [btnSender setImage:[UIImage imageNamed:@"shopping_icon_active.png"] forState:UIControlStateNormal];
        [_travelBtn setImage:[UIImage imageNamed:@"travel_icon.png"] forState:UIControlStateNormal];
        
        recieptType = @"SHOPPING";
    }
    
}

- (IBAction)travelPressed:(id)sender {
    isTypeSelected = true;
    _selectTypeImg.hidden = true;
    UIButton *btnSender = (UIButton*)sender;
    if(btnSender.tag == 0) {
        btnSender.tag = 1;
        [_shoppingBtn setImage:[UIImage imageNamed:@"shopping_icon.png"] forState:UIControlStateNormal];
        _shoppingBtn.tag = 0;
        [btnSender setImage:[UIImage imageNamed:@"travel_icon_active.png"] forState:UIControlStateNormal];
        
        recieptType = @"TRAVEL";
    }
}

- (IBAction)retakeImgPressed:(id)sender {
    _cameraBtn.tag = 0;
    _capturedImg.hidden = true;
    _confirmBar.hidden = true;
    
    _uploadView.hidden = true;
    _uploadTravelView.hidden = true;
    
    [_cameraBtn setBackgroundImage:[UIImage imageNamed:@"camera_icon.png"] forState:UIControlStateNormal];
}

- (IBAction)uploadPressed:(id)sender {
    
    if(![recieptType isEqualToString:@"TRAVEL"]){
        _uploadViewPicture.image = _capturedImg.image;
        _uploadView.hidden = false;
    }
    else {
        _utCardUploadImg.image = _capturedImg.image;
        _uploadTravelView.hidden = false;
    }
}

- (IBAction)cancelUploadPressed:(id)sender {
    _uploadView.hidden = true;
}

- (IBAction)uploadNowPressed:(id)sender {
    if(_purchaseDate.text.length > 0 && _subtotalAmount.text.length > 0 && _storeName.text.length>0 && [self isStoreValid:_storeName.text]){
        [self submitReciept];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Please fill all fields with valid values" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField setTextColor:[UIColor blackColor]];
    if(textField.tag != 11) {
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
        [self.view addGestureRecognizer:tapRecognizer];
        self.view.tag=2;
    }
    
    [self animateTextField: textField up: YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    // add your method here
    if(textField.tag == 11) {
        if([self isStoreValid:textField.text]) {
            [textField setTextColor:[UIColor blackColor]];
            isvalidStore = true;
        }
        else {
            [textField setTextColor:[UIColor redColor]];
            isvalidStore = false;
        }
    }
    else if (textField.tag == 20 || textField.tag == 30) {
        textField.text = [NSString stringWithFormat:@"$%@",textField.text];
    }
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self animateTextField: textField up: NO];
}
- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    if(!up && self.view.frame.origin.y == 0) {
        movement= 0;
    }
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:datePicker.date]];
    //assign text to label
    
    if([recieptType isEqualToString:@"TRAVEL"]) {
        _checkInTxt.text=str;
    }
    else {
         _purchaseDate.text=str;
    }
}


#pragma mark - Reciept Methods

- (void) submitReciept {
    
    selectedStoreModel = [self returnStore:_storeName.text];
    
    [self.view endEditing:true];
    [CustomLoading showAlertMessage];
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    //create request
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    
    //Set Params
    [urlRequest setHTTPShouldHandleCookies:NO];
    [urlRequest setTimeoutInterval:60];
    [urlRequest setHTTPMethod:@"POST"];
    
    //Create boundary, it can be anything
    NSString *boundary = @"---011000010111000001101001";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [urlRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    //Populate a dictionary with all the regular values you would like to send.
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:recieptType forKey:@"receipt_type"];
    
    [parameters setValue:selectedStoreModel.storeId forKey:@"store_id"];
    
    NSString *str = [_subtotalAmount.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    [parameters setValue:str forKey:@"subtotal_amount"];
    
    [parameters setValue:_purchaseDate.text forKey:@"purchased_date"];
    
    [parameters setValue:strUserId forKey:@"customer_id"];
    
    if([recieptType isEqualToString:@"TRAVEL"]){
        [parameters setValue:@"" forKey:@"third_party"];
    }
    
    // add params (all params are strings)
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSString *FileParamConstant = @"receipt_image";
    
    NSData *imageData = UIImageJPEGRepresentation(_uploadViewPicture.image, 1);
    
    //Assuming data is not nil we add this to the multipart form
    if (imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [urlRequest setHTTPBody:body];
    
    NSString *urlString = [NSString stringWithFormat:@"http://www.saivian-squad.com/receipts/upload"];
    NSString *escapedPath = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // set URL
    [urlRequest setURL:[NSURL URLWithString:escapedPath]];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                               
                               [CustomLoading DismissAlertMessage];
                               NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                    options:kNilOptions error:&error];
                               
                               
                               if ([httpResponse statusCode] == 200) {
                                   
                                   int status  =  [[json objectForKey:@"status"] intValue];
                                   
                                   if(status == 1) {
                                       
                                       NSArray * array = [json objectForKey:@"receipt"];
                                       NSDictionary *dict = [array objectAtIndex:0];
                                       
                                       RecieptModel *rModel = [[RecieptModel alloc] init];
                                       rModel.recieptId = [dict objectForKey:@"receipt_id"];
                                       rModel.recieptId = [dict objectForKey:@"receipt_id"];
                                       rModel.purchasedDate = [dict objectForKey:@"purchased_date"];
                                       rModel.recieptType = [dict objectForKey:@"receipt_type"];
                                       rModel.storeName = [dict objectForKey:@"store_name"];
                                       rModel.subtotalAmount = [dict objectForKey:@"subtotal_amount"];
                                       rModel.uploadedDate = [dict objectForKey:@"uploaded_date"];
                                       
                                       [appDel.userModel.recieptArray addObject:rModel];
                                       [self.navigationController popViewControllerAnimated:false];
                                       
                                   }
                                   else {
                                       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"We are Busy!!" message:@"Please try again" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                                       [alert show];
                                   }
                                   
                               }
                               else{
                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Something went wrong with internet connection!!!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                                   [alert show];
                               }
                               
                           }];
    
}

- (void) submitTravelReciept {
    
    selectedStoreModel = [self returnStore:_airlineTxt.text];
    
    [self.view endEditing:true];
    
    [CustomLoading showAlertMessage];
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    //create request
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    
    //Set Params
    [urlRequest setHTTPShouldHandleCookies:NO];
    [urlRequest setTimeoutInterval:60];
    [urlRequest setHTTPMethod:@"POST"];
    
    //Create boundary, it can be anything
    NSString *boundary = @"---011000010111000001101001";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [urlRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    //Populate a dictionary with all the regular values you would like to send.
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:recieptType forKey:@"receipt_type"];
    
    [parameters setValue:selectedStoreModel.storeId forKey:@"store_id"];
    
    NSString *str = [_subTotalTxt.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    [parameters setValue:str forKey:@"subtotal_amount"];
    
    [parameters setValue:_checkInTxt.text forKey:@"purchased_date"];
    
    [parameters setValue:strUserId forKey:@"customer_id"];
    
    [parameters setValue:_thirdPartyTxt.text forKey:@"third_party"];
    
    // add params (all params are strings)
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSString *FileParamConstant = @"receipt_image";
    
    NSData *imageData = UIImageJPEGRepresentation(_uploadViewPicture.image, 1);
    
    //Assuming data is not nil we add this to the multipart form
    if (imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [urlRequest setHTTPBody:body];
    
    NSString *urlString = [NSString stringWithFormat:@"http://www.saivian-squad.com/receipts/upload"];
    NSString *escapedPath = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // set URL
    [urlRequest setURL:[NSURL URLWithString:escapedPath]];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                               
                               [CustomLoading DismissAlertMessage];
                               NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                    options:kNilOptions error:&error];
                               
                               if ([httpResponse statusCode] == 200) {
                                   
                                   int status  =  [[json objectForKey:@"status"] intValue];
                                   
                                   if(status == 1) {
                                       
                                       NSArray * array = [json objectForKey:@"receipt"];
                                       NSDictionary *dict = [array objectAtIndex:0];
                                       
                                       RecieptModel *rModel = [[RecieptModel alloc] init];
                                       rModel.recieptId = [dict objectForKey:@"receipt_id"];
                                       rModel.recieptId = [dict objectForKey:@"receipt_id"];
                                       rModel.purchasedDate = [dict objectForKey:@"purchased_date"];
                                       rModel.recieptType = [dict objectForKey:@"receipt_type"];
                                       rModel.storeName = [dict objectForKey:@"store_name"];
                                       rModel.subtotalAmount = [dict objectForKey:@"subtotal_amount"];
                                       rModel.uploadedDate = [dict objectForKey:@"uploaded_date"];
                                       
                                       [appDel.userModel.recieptArray addObject:rModel];
                                       [self.navigationController popViewControllerAnimated:false];
                                       
                                   }
                                   else {
                                       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"We are Busy!!" message:@"Please try again" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                                       [alert show];
                                   }
                                   
                               }
                               else{
                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Something went wrong with internet connection!!!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                                   [alert show];
                               }
                               
                           }];
    
}

- (IBAction)airlinePressed:(id)sender {
    NSArray *arr = [[NSArray alloc] init];
    
    NSMutableArray *storeNames = [[NSMutableArray alloc] init];
    
    for(int i=0; i< appDel.userModel.allStores.count; i++) {
        StoreModel*sModel = [appDel.userModel.allStores objectAtIndex:i];
        [storeNames addObject:sModel.storeName];
    }
    
    arr = storeNames;
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 400;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)checkInPressed:(id)sender {
    _utDobPickerView.hidden = false;
    _airlineTxt.hidden = true;
    [self.view endEditing:YES];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [self.view addGestureRecognizer:tapRecognizer];
    self.view.tag=2;
}

- (IBAction)tuCancelPressed:(id)sender {
    _uploadTravelView.hidden = true;
}

- (IBAction)tuSubmitPressed:(id)sender {
    if(_airlineTxt.text.length > 0 && _checkInTxt.text.length > 0 && _subTotalTxt.text.length > 0 && _thirdPartyTxt.text.length > 0 && [self isStoreValid:_airlineTxt.text]){
        [self submitTravelReciept];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Please fill all fields with valid values" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender
{
    if(sender.view.tag==2) {
        [self.view endEditing:YES];
        
    }
    _storeName.hidden = false;
    _airlineTxt.hidden = false;
    _datePickerView.hidden = true;
    _utDobPickerView.hidden = true;
    [self.view removeGestureRecognizer:sender];
}

#pragma mark - MLPAutoCompleteTextField Delegate


- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
          shouldConfigureCell:(UITableViewCell *)cell
       withAutoCompleteString:(NSString *)autocompleteString
         withAttributedString:(NSAttributedString *)boldedString
        forAutoCompleteObject:(id<MLPAutoCompletionObject>)autocompleteObject
            forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //This is your chance to customize an autocomplete tableview cell before it appears in the autocomplete tableview
    NSString *filename = [autocompleteString stringByAppendingString:@".png"];
    filename = [filename stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    filename = [filename stringByReplacingOccurrencesOfString:@"&" withString:@"and"];
    [cell.imageView setImage:[UIImage imageNamed:filename]];
    
    return YES;
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(selectedObject){
        NSLog(@"selected object from autocomplete menu %@ with string %@", selectedObject, [selectedObject autocompleteString]);
    } else {
        NSLog(@"selected string '%@' from autocomplete menu", selectedString);
    }
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField willShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view will be added to the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didHideAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view ws removed from the view hierarchy");
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didShowAutoCompleteTableView:(UITableView *)autoCompleteTableView {
    NSLog(@"Autocomplete table view was added to the view hierarchy");
}

#pragma mark - helper methods 
- (NSArray *)allStores
{
    NSMutableArray *storeNamesArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<appDel.userModel.allStores.count; i++) {
        StoreModel *sModel = (StoreModel*)[appDel.userModel.allStores objectAtIndex:i];
        
        [storeNamesArray addObject:sModel.storeName];
    }
    NSArray *stores = storeNamesArray;
    
    return stores;
}

- (StoreModel*) returnStore : (NSString *) storeName {
    
    
    for(int i=0; i<appDel.userModel.allStores.count; i++) {
        StoreModel *sModel = (StoreModel*)[appDel.userModel.allStores objectAtIndex:i];
        
        if([[storeName uppercaseString] isEqualToString:[sModel.storeName uppercaseString]]) {
            return  sModel;
        }
    }
    
    return nil;
}

- (BOOL) isStoreValid : (NSString *) storeName {
    
    
    for(int i=0; i<appDel.userModel.allStores.count; i++) {
        StoreModel *sModel = (StoreModel*)[appDel.userModel.allStores objectAtIndex:i];
        
        if([[storeName uppercaseString] isEqualToString:[sModel.storeName uppercaseString]]) {
            return true;
        }
    }
    
    return false;
}

@end
