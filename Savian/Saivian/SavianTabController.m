//
//  SavianTabController.m
//  Saivian
//
//  Created by Ahmed Sadiq on 09/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "SavianTabController.h"
#import "Constants.h"
@implementation SavianTabController

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



- (id) showTabController : (UIView *)parentV andFrame: (CGRect) frame; {
    
    self.centerImg = (UIImageView *)[super init];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        self.frame = frame;
        self.layer.shadowOffset = CGSizeMake(-5, -5);
        
        self.centerImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height+8)];
        self.centerImg.image = [UIImage imageNamed:@"lower_bar.png"];
        
        
        self.centerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.centerBtn addTarget:self
                   action:@selector(delegateMethod:)
         forControlEvents:UIControlEventTouchUpInside];
        [self.centerBtn setTitle:@"" forState:UIControlStateNormal];
        
        
        self.recieptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.recieptBtn addTarget:self
                            action:@selector(delegateRecieptMethod:)
                  forControlEvents:UIControlEventTouchUpInside];
        [self.recieptBtn setTitle:@"" forState:UIControlStateNormal];
        
        self.storeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.storeBtn addTarget:self
                          action:@selector(delegateStoreMethod:)
                forControlEvents:UIControlEventTouchUpInside];
        [self.storeBtn setTitle:@"" forState:UIControlStateNormal];
        
        if(IS_IPAD) {
            [self.centerBtn setBackgroundImage:[UIImage imageNamed:@"camera_icon.png"] forState:UIControlStateNormal];
            [self.centerBtn setBackgroundImage:[UIImage imageNamed:@"camera_icon_glow.png"] forState:UIControlStateHighlighted];
            self.centerBtn.frame = CGRectMake((self.frame.size.width  / 2)-60, 20, 120, 148);
            
            [self.recieptBtn setBackgroundImage:[UIImage imageNamed:@"receipts_icon.png"] forState:UIControlStateNormal];
            
            self.recieptBtn.frame = CGRectMake(26, 93, 66, 70);
            
            [self.storeBtn setBackgroundImage:[UIImage imageNamed:@"store_icon.png"] forState:UIControlStateNormal];
            
            self.storeBtn.frame = CGRectMake((self.frame.size.width-105), 93, 66, 70);
        }
        else {
            [self.centerBtn setImage:[UIImage imageNamed:@"camera_icon.png"] forState:UIControlStateNormal];
            [self.centerBtn setImage:[UIImage imageNamed:@"camera_icon_glow.png"] forState:UIControlStateHighlighted];
            self.centerBtn.frame = CGRectMake((self.frame.size.width  / 2)-43, 8, 86, 90);
            
            
            [self.recieptBtn setImage:[UIImage imageNamed:@"receipts_icon.png"] forState:UIControlStateNormal];
            
            self.recieptBtn.frame = CGRectMake(05, 25, 100, 90);
            
            [self.storeBtn setImage:[UIImage imageNamed:@"store_icon.png"] forState:UIControlStateNormal];
            
            self.storeBtn.frame = CGRectMake((self.frame.size.width-105), 25, 100, 90);
        }
        
        
        
        [parentV.superview addSubview:self];
        
        [self addSubview:self.centerImg];
        [self addSubview:self.centerBtn];
        [self addSubview:self.recieptBtn];
        [self addSubview:self.storeBtn];
        
    }
    
    return self;
}

- (void) delegateMethod : (id) sender {
    [self.delegate SavianTabControllerPressed:self];
}

- (void) delegateRecieptMethod : (id) sender {
    [self.delegate SavianTabControllerRecieptPressed:self];
}
- (void) delegateStoreMethod : (id) sender {
    [self.delegate SavianTabControllerStorePressed:self];
}

-(void)dealloc {
    
}

@end
