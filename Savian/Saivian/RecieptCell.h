//
//  RecieptCell.h
//  Saivian
//
//  Created by Ahmed Sadiq on 23/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecieptCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;

@end
