//
//  DealCellHOOExpanded.h
//  Saivian
//
//  Created by Ahmed Sadiq on 19/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DealCellHOOExpanded : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *webBtn;

@property (weak, nonatomic) IBOutlet UIButton *locationBtn;
@property (weak, nonatomic) IBOutlet UIButton *hoursBtn;
@property (strong, nonatomic) IBOutlet UILabel *sundayTime;
@property (strong, nonatomic) IBOutlet UILabel *tuesdayTime;
@property (strong, nonatomic) IBOutlet UILabel *mondayTime;
@property (strong, nonatomic) IBOutlet UILabel *wednesdayTime;
@property (strong, nonatomic) IBOutlet UILabel *thursdayTime;
@property (strong, nonatomic) IBOutlet UILabel *firdayTime;
@property (strong, nonatomic) IBOutlet UILabel *saturdayTime;


@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) IBOutlet UILabel *addressLbl;
@property (strong, nonatomic) IBOutlet UILabel *addressLbl2;
@property (strong, nonatomic) IBOutlet UILabel *phoneLbl;
@property (strong, nonatomic) IBOutlet UILabel *stateLbl;

@property (strong, nonatomic) IBOutlet UIButton *heartBtn;

@property (strong, nonatomic) IBOutlet UIButton *callBtn;




@end
