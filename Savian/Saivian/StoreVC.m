//
//  StoreVC.m
//  Saivian
//
//  Created by Ahmed Sadiq on 23/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "StoreVC.h"
#import "StoreVCCell.h"
#import "AddStoreCell.h"
#import "DrawerVC.h"
#import "CustomLoading.h"
#import "StoreModel.h"
#import "StoreVCExpandedCell.h"
#import "PaymentMethod.h"
#import "Constants.h"
#import "NavigationHandler.h"
#import "HTAutocompleteManager.h"
@interface StoreVC ()

@end

@implementation StoreVC

- (void)viewDidLayoutSubviews{
    if(_tabControl == nil) {
        _tabControl = [[SavianTabController alloc] showTabController:self.lowerBar andFrame:_lowerBar.frame];
        _tabControl.direction = @"down";
        _tabControl.delegate = self;
    }
    
    [_top10TblView setContentSize:CGSizeMake(_top10TblView.contentSize.width, _top10TblView.contentSize.height+120)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    _mainScroller.contentSize = CGSizeMake(screenWidth*1, 500);
    appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
}

- (void) fetchStoresList {
    [CustomLoading showAlertMessage];
    
    
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    NSString *urlString = [NSString stringWithFormat:@"http://www.saivian-squad.com/stores/storesList?customer_id=%@",strUserId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    request = [ASIFormDataRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
    //[req setPostValue:strUserId forKey:@"customer_id"];
    
    [request setRequestMethod:@"GET"];
    
    request.tag = 1;
    
    [request setTimeOutSeconds:300];
    [request setDelegate:self];
    [request startAsynchronous];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return appDel.userModel.topTenStores.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == (appDel.userModel.topTenStores.count)) {
        static NSString *simpleTableIdentifier = @"AddStoreCell";
        
        AddStoreCell *cell = (AddStoreCell *)[tableView dequeueReusableCellWithIdentifier:nil];
        if (cell == nil)
        {
            NSArray *nib;
            if(IS_IPAD) {
                nib = [[NSBundle mainBundle] loadNibNamed:@"AddStoreCell_iPad" owner:self options:nil];
            }
            else {
                nib = [[NSBundle mainBundle] loadNibNamed:@"AddStoreCell" owner:self options:nil];
            }
            
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.zipBtn addTarget:self
                        action:@selector(zipBtnPressed:)
              forControlEvents:UIControlEventTouchUpInside];
        
        [HTAutocompleteTextField setDefaultAutocompleteDataSource:[HTAutocompleteManager sharedManager]];
        
        cell.stName.autocompleteType = HTAutocompleteTypeColor;
        
        [cell.submitBtn addTarget:self
                           action:@selector(submitStoreBtnPressed:)
                 forControlEvents:UIControlEventTouchUpInside];
        [cell.cancelBtn addTarget:self
                           action:@selector(cancelStoreBtnPressed:)
                 forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else {
        StoreModel *sModel = [appDel.userModel.topTenStores objectAtIndex:indexPath.row];
        if(sModel.isSelected) {
            
            StoreVCExpandedCell *cell = (StoreVCExpandedCell *)[tableView dequeueReusableCellWithIdentifier:nil];
            if (cell == nil)
            {
                NSArray *nib;
                if(IS_IPAD) {
                    nib = [[NSBundle mainBundle] loadNibNamed:@"StoreVCExpandedCell_iPad" owner:self options:nil];
                }
                else {
                    nib = [[NSBundle mainBundle] loadNibNamed:@"StoreVCExpandedCell" owner:self options:nil];
                }
                
                cell = [nib objectAtIndex:0];
                
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.featureImg.imageURL = [NSURL URLWithString:sModel.profileImage];
            NSURL *url = [NSURL URLWithString:sModel.profileImage];
            [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
            
            cell.titleLbl.text = sModel.storeName;
            cell.addressLbl.text = sModel.address;
            cell.phoneLbl.text = sModel.phoneNo;
            cell.webLbl.text = sModel.webUrl;
            
            cell.locationBtn.tag = indexPath.row;
            cell.likeBtn.tag = indexPath.row;
            cell.callBtn.tag = indexPath.row;
            cell.hoursBtn.tag = indexPath.row;
            cell.normalStateBtn.tag = indexPath.row;
            
            CLLocationCoordinate2D yourPos = CLLocationCoordinate2DMake([sModel.latitude doubleValue], [sModel.longitude doubleValue]);
            
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(yourPos, 800, 800);
            [cell.mainMap setRegion:[cell.mainMap regionThatFits:region] animated:YES];
            
            if(sModel.isFavourite) {
                [cell.likeBtn setImage:[UIImage imageNamed:@"fave_icon_active.png"] forState:UIControlStateNormal];
                
            }
            
            [cell.locationBtn addTarget:self
                                 action:@selector(locationBtnPressed:)
                       forControlEvents:UIControlEventTouchUpInside];
            [cell.likeBtn addTarget:self
                             action:@selector(likeBtnPressed:)
                   forControlEvents:UIControlEventTouchUpInside];
            [cell.callBtn addTarget:self
                             action:@selector(callBtnPressed:)
                   forControlEvents:UIControlEventTouchUpInside];
            [cell.hoursBtn addTarget:self
                              action:@selector(hoursBtnPressed:)
                    forControlEvents:UIControlEventTouchUpInside];
            [cell.normalStateBtn addTarget:self
                                    action:@selector(normalStateBtnPressed:)
                          forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        else {
            StoreVCCell *cell = (StoreVCCell *)[tableView dequeueReusableCellWithIdentifier:nil];
            if (cell == nil)
            {
                NSArray *nib;
                if(IS_IPAD) {
                    nib = [[NSBundle mainBundle] loadNibNamed:@"StoreVCCell_iPad" owner:self options:nil];
                }
                else {
                    nib = [[NSBundle mainBundle] loadNibNamed:@"StoreVCCell" owner:self options:nil];
                }
                
                cell = [nib objectAtIndex:0];
                
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.featureImg.imageURL = [NSURL URLWithString:sModel.profileImage];
            NSURL *url = [NSURL URLWithString:sModel.profileImage];
            [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
            
            cell.storeNameLbl.text = sModel.storeName;
            return cell;
        }
        
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row == (appDel.userModel.topTenStores.count)) {
        if(IS_IPAD) {
            return 600;
        }
        return 350;
    }
    else {
        StoreModel *sModel = [appDel.userModel.topTenStores objectAtIndex:indexPath.row];
        if(sModel.isSelected) {
            if(IS_IPAD) {
                return 670;
            }
            return 450;
        }
        else {
            if(IS_IPAD) {
                return 80;
            }
            else {
                return 44;
            }
        }
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row < appDel.userModel.topTenStores.count) {
        NSArray* rowsToReload = [NSArray arrayWithObjects:indexPath, nil];
        StoreModel *sModel = [appDel.userModel.topTenStores objectAtIndex:indexPath.row];
        if(sModel.isSelected) {
            sModel.isSelected = false;
            [_top10TblView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
        }
        else {
            
            selectedIndexPath = indexPath;
            StoreVCCell *cell = (StoreVCCell*)[tableView cellForRowAtIndexPath:indexPath];
            
            [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:(void (^)(void)) ^{
                CGRect screenRect = [[UIScreen mainScreen] bounds];
                CGFloat screenWidth = screenRect.size.width;
                
                cell.featureImg.frame = CGRectMake(0, 0, screenWidth, cell.featureImg.frame.size.height);
            }
             
                             completion:^(BOOL finished){
                                 sModel.isSelected = true;
                                 [_top10TblView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
                             }];
        }
    }
}

#pragma mark Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    isEditing = true;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [self.view addGestureRecognizer:tapRecognizer];
    self.view.tag=2;
    
    [self animateTextField: textField up: YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    // add your method here
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    isEditing = false;
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    
    if(!up && self.view.frame.origin.y == 0) {
        movement= 0;
    }
    
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark Market Research Methods

- (IBAction)genericPRessed:(id)sender {
    lastAnswer = @"Generic/Brand Store";
    [_genericBtn setImage:[UIImage imageNamed:@"radio_button_active.png"] forState:UIControlStateNormal];
    [_dontCareBtn setImage:[UIImage imageNamed:@"radio_button.png"] forState:UIControlStateNormal];
    [_nameBrandBtn setImage:[UIImage imageNamed:@"radio_button.png"] forState:UIControlStateNormal];
}

- (IBAction)dontCarePressed:(id)sender {
    lastAnswer = @"Don't Care";
    [_genericBtn setImage:[UIImage imageNamed:@"radio_button.png"] forState:UIControlStateNormal];
    [_dontCareBtn setImage:[UIImage imageNamed:@"radio_button_active.png"] forState:UIControlStateNormal];
    [_nameBrandBtn setImage:[UIImage imageNamed:@"radio_button.png"] forState:UIControlStateNormal];
}
- (IBAction)nameBrandPressed:(id)sender {
    lastAnswer = @"Name Brand";
    [_genericBtn setImage:[UIImage imageNamed:@"radio_button.png"] forState:UIControlStateNormal];
    [_dontCareBtn setImage:[UIImage imageNamed:@"radio_button.png"] forState:UIControlStateNormal];
    [_nameBrandBtn setImage:[UIImage imageNamed:@"radio_button_active.png"] forState:UIControlStateNormal];
}

- (IBAction)houseHoldCountPressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7", nil];
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 100;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)incomePressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"$1000",@"$2000",@"$3000",@"$4000",@"$5000",@"$6000",@"$7000", nil];
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 100;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}



- (IBAction)top10Pressed:(id)sender {
    [_top10Btn setBackgroundImage:[UIImage imageNamed:@"menu_bar_tab_active.png"] forState:UIControlStateNormal];
    [_methodPayment setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [_marketResearchBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    
    _mainScroller.contentOffset = CGPointMake(0, 0);
    
}

- (IBAction)methodPaymnetPressed:(id)sender {
    [_top10Btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [_methodPayment setBackgroundImage:[UIImage imageNamed:@"menu_bar_tab_active.png"] forState:UIControlStateNormal];
    [_marketResearchBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    _mainScroller.contentOffset = CGPointMake(screenWidth*1, 0);
    
    if(!appDel.userModel.paymentInfoModel) {
        [self fetchPaymentMethods];
    }
    else {
        _cardTypeLbl.text = appDel.userModel.paymentInfoModel.cardType;
        _cardNumberLbl.text = appDel.userModel.paymentInfoModel.cardNumber;
        _cardHolderNameLbl.text = appDel.userModel.paymentInfoModel.cardHolderName;
        _cvcLbl.text = appDel.userModel.paymentInfoModel.cvc;
        _cardExpiryLbl.text = [NSString stringWithFormat:@"%@/%@",appDel.userModel.paymentInfoModel.expiryMonth,appDel.userModel.paymentInfoModel.expiryYear];
        
        _cardNumberTxt.text = appDel.userModel.paymentInfoModel.cardNumber;
        _cardHolderNameTxt.text = appDel.userModel.paymentInfoModel.cardHolderName;
        
        [_cardTypeBtn setTitle:appDel.userModel.paymentInfoModel.cardType forState:UIControlStateNormal];
        _cvcTxt.text = appDel.userModel.paymentInfoModel.cvc;
        _cardExpiryTxt.text = [NSString stringWithFormat:@"%@/%@",appDel.userModel.paymentInfoModel.expiryMonth,appDel.userModel.paymentInfoModel.expiryYear];
        
    }
}

- (IBAction)marketResearchPressed:(id)sender {
    [_top10Btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [_methodPayment setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [_marketResearchBtn setBackgroundImage:[UIImage imageNamed:@"menu_bar_tab_active.png"] forState:UIControlStateNormal];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    _mainScroller.contentOffset = CGPointMake(screenWidth*2, 0);
}

- (IBAction)addStorePressed:(id)sender {
    
    [self.top10TblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:appDel.userModel.topTenStores.count inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
}

- (IBAction)cardTypePressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Master Card",@"Visa Card",@"Debit Card", nil];
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 100;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)sliderPressed:(id)sender {
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (IBAction)submitOnlineSignature:(id)sender {
    
    [self.view endEditing:true];
    
    if(_homeZipTxt.text.length > 0 && _houseHoldBtn.titleLabel.text.length > 0 && _annualIncomeBtn.titleLabel.text.length > 0 && lastAnswer.length > 0) {
        
        [CustomLoading showAlertMessage];
        
        NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
        
        
        NSURL *url = [NSURL URLWithString:@"http://saivian-squad.com/customer/submitMarketResearch"];
        request = [ASIFormDataRequest requestWithURL:url];
        [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
        [request setPostValue:strUserId forKey:@"customer_id"];
        [request setPostValue:_homeZipTxt.text forKey:@"answer1"];
        [request setPostValue:_houseHoldBtn.titleLabel.text forKey:@"answer2"];
        [request setPostValue:_annualIncomeBtn.titleLabel.text forKey:@"answer3"];
        [request setPostValue:lastAnswer forKey:@"answer4"];
        request.tag = 11;
        
        [request setRequestMethod:@"POST"];
        [request setTimeOutSeconds:300];
        [request setDelegate:self];
        [request startAsynchronous];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Form needs to be filled completely" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)zipBtnPressed:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    
    NSMutableArray *storesNameArray = [[NSMutableArray alloc] init];
    for(int i=0; i<appDel.userModel.allStores.count; i++) {
        StoreModel *sModel = [appDel.userModel.allStores objectAtIndex:i];
        [storesNameArray addObject:sModel.storeName];
    }
    
    arr = storesNameArray;
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 320;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.tag = 32;
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)normalStateBtnPressed:(id)sender {
    
    UIButton *senderBtn = (UIButton*)sender;
    
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:senderBtn.tag inSection:0];
    StoreVCExpandedCell *cell = (StoreVCExpandedCell*)[_top10TblView cellForRowAtIndexPath:myIP];
    
    cell.hooView.hidden = true;
    cell.mainMap.hidden = true;
    cell.featureImg.hidden = false;
    cell.normalStateBtn.enabled = false;
    
    [UIView transitionWithView:self.view
                      duration:1.0
                       options:(true ? UIViewAnimationOptionTransitionFlipFromRight :
                                UIViewAnimationOptionTransitionFlipFromLeft)
                    animations: ^{
                        
                    }
     
                    completion:^(BOOL finished) {
                    }];
}

- (IBAction)likeBtnPressed:(id)sender {
    
    UIButton *senderBtn = (UIButton*)sender;
    
    currentStoreIndex = senderBtn.tag;
    
    StoreModel *sModel = [appDel.userModel.topTenStores objectAtIndex:senderBtn.tag];
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:senderBtn.tag inSection:0] ;
    StoreVCExpandedCell *cell = (StoreVCExpandedCell*)[_top10TblView cellForRowAtIndexPath:myIP];
    
    if(!sModel.isFavourite) {
        
        [cell.likeBtn setImage:[UIImage imageNamed:@"fave_icon_active.png"] forState:UIControlStateNormal];
    }
    else{
        [cell.likeBtn setImage:[UIImage imageNamed:@"fave_icon.png"] forState:UIControlStateNormal];
    }
    [self sendFaouriteCall:sModel.storeId];
    
    
    
}
- (IBAction)locationBtnPressed:(id)sender {
    
    UIButton *senderBtn = (UIButton*)sender;
    
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:senderBtn.tag inSection:0] ;
    StoreVCExpandedCell *cell = (StoreVCExpandedCell*)[_top10TblView cellForRowAtIndexPath:myIP];
    
    cell.mainMap.hidden = false;
    cell.normalStateBtn.enabled = true;
    cell.hooView.hidden = true;
    
    [UIView transitionWithView:self.view
                      duration:1.0
                       options:(true ? UIViewAnimationOptionTransitionFlipFromRight :
                                UIViewAnimationOptionTransitionFlipFromLeft)
                    animations: ^{
                        
                    }
     
                    completion:^(BOOL finished) {
                    }];
    
}
- (IBAction)callBtnPressed:(id)sender {
    
    UIButton *senderBtn = (UIButton*)sender;
    StoreModel *sModel = [appDel.userModel.topTenStores objectAtIndex:senderBtn.tag];
    
    NSString *phoneNumber = [@"tel://" stringByAppendingString:sModel.phoneNo];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    
    
}

- (IBAction)hoursBtnPressed:(id)sender {
    
    UIButton *senderBtn = (UIButton*)sender;
    
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:senderBtn.tag inSection:0] ;
    StoreVCExpandedCell *cell = (StoreVCExpandedCell*)[_top10TblView cellForRowAtIndexPath:myIP];
    
    cell.featureImg.hidden = true;
    cell.mainMap.hidden = true;
    cell.hooView.hidden = false;
    cell.normalStateBtn.enabled = true;
    
    [UIView transitionWithView:self.view
                      duration:1.0
                       options:(true ? UIViewAnimationOptionTransitionFlipFromRight :
                                UIViewAnimationOptionTransitionFlipFromLeft)
                    animations: ^{
                        
                    }
     
                    completion:^(BOOL finished) {
                    }];
}

#pragma mark - Mark Favourite Server Methods

- (void) sendFaouriteCall : (NSString *) storeID {
    
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    
    NSURL *url = [NSURL URLWithString:@"http://www.saivian-squad.com/stores/addFavouriteStore"];
    request = [ASIFormDataRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
    [request setPostValue:strUserId forKey:@"customer_id"];
    [request setPostValue:storeID forKey:@"store_id"];
    request.tag = 10;
    
    [request setRequestMethod:@"POST"];
    [request setTimeOutSeconds:300];
    [request setDelegate:self];
    [request startAsynchronous];
    
    
}



- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    if(sender.tag == 32) {
        selectedStoreIndex = sender.selectedIndex;
        
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:appDel.userModel.topTenStores.count inSection:0] ;
        
        AddStoreCell *cell = (AddStoreCell*)[_top10TblView cellForRowAtIndexPath:myIP];
        
        StoreModel *sModel = (StoreModel*)[appDel.userModel.allStores objectAtIndex:selectedStoreIndex];
        
        cell.storeAddress.text = sModel.address;
        cell.storeName.text = sModel.zip;
        cell.phoneNum.text = sModel.phoneNo;
        cell.stateTxt.text = sModel.state;
        
    }
    
    [self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

#pragma mark - ScrollView Delegate Methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    float fractionalPage = scrollView.contentOffset.x / screenWidth;
    NSInteger page1 = lround(fractionalPage);
    
    [self.view endEditing:true];
    
    if(page1 == 0) {
        [_top10Btn setBackgroundImage:[UIImage imageNamed:@"menu_bar_tab_active.png"] forState:UIControlStateNormal];
        [_methodPayment setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_marketResearchBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    else if (page1 == 1) {
        [_top10Btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_methodPayment setBackgroundImage:[UIImage imageNamed:@"menu_bar_tab_active.png"] forState:UIControlStateNormal];
        [_marketResearchBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        
        if(!appDel.userModel.paymentInfoModel) {
            [self fetchPaymentMethods];
        }
        else {
            _cardTypeLbl.text = appDel.userModel.paymentInfoModel.cardType;
            _cardNumberLbl.text = appDel.userModel.paymentInfoModel.cardNumber;
            _cardHolderNameLbl.text = appDel.userModel.paymentInfoModel.cardHolderName;
            _cvcLbl.text = appDel.userModel.paymentInfoModel.cvc;
            _cardExpiryLbl.text = [NSString stringWithFormat:@"%@/%@",appDel.userModel.paymentInfoModel.expiryMonth,appDel.userModel.paymentInfoModel.expiryYear];
            
            _cardNumberTxt.text = appDel.userModel.paymentInfoModel.cardNumber;
            _cardHolderNameTxt.text = appDel.userModel.paymentInfoModel.cardHolderName;
            
            [_cardTypeBtn setTitle:appDel.userModel.paymentInfoModel.cardType forState:UIControlStateNormal];
            _cvcTxt.text = appDel.userModel.paymentInfoModel.cvc;
            _cardExpiryTxt.text = [NSString stringWithFormat:@"%@/%@",appDel.userModel.paymentInfoModel.expiryMonth,appDel.userModel.paymentInfoModel.expiryYear];
        }
        
    }
    else {
        [_top10Btn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_methodPayment setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_marketResearchBtn setBackgroundImage:[UIImage imageNamed:@"menu_bar_tab_active.png"] forState:UIControlStateNormal];
    }
}

#pragma mark Server Request Delegate Methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    if(request.tag == 1) {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
        int success = [[result objectForKey:@"status"] intValue];
        
        if(success == 1) {
            NSArray *storeArray = [result objectForKey:@"stores_list"];
            appDel.userModel.topTenStores = [[NSMutableArray alloc] init];
            for(int i=0; i<storeArray.count; i++) {
                NSDictionary *storeDict = (NSDictionary*)[storeArray objectAtIndex:i];
                StoreModel *sModel = [[StoreModel alloc] init];
                
                sModel.storeId = [storeDict objectForKey:@"store_id"];
                sModel.storeName = [storeDict objectForKey:@"store_name"];
                sModel.webUrl = [storeDict objectForKey:@"web_url"];
                sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
                sModel.profileImage = [storeDict objectForKey:@"profile_image"];
                sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
                sModel.address = [storeDict objectForKey:@"address"];
                sModel.city = [storeDict objectForKey:@"city"];
                sModel.state = [storeDict objectForKey:@"state"];
                sModel.zip = [storeDict objectForKey:@"zip"];
                sModel.latitude = [storeDict objectForKey:@"latitude"];
                sModel.longitude = [storeDict objectForKey:@"longitude"];
                
                [appDel.userModel.topTenStores addObject:sModel];
            }
            
            
            [self fetchAllStores];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"We are not available at the moment" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if(request.tag == 2) {
        [CustomLoading DismissAlertMessage];
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
        int success = [[result objectForKey:@"status"] intValue];
        
        if(success == 1) {
            NSArray *storeArray = [result objectForKey:@"stores_list"];
            appDel.userModel.allStores = [[NSMutableArray alloc] init];
            for(int i=0; i<storeArray.count; i++) {
                NSDictionary *storeDict = (NSDictionary*)[storeArray objectAtIndex:i];
                StoreModel *sModel = [[StoreModel alloc] init];
                
                sModel.storeId = [storeDict objectForKey:@"store_id"];
                sModel.storeName = [storeDict objectForKey:@"store_name"];
                sModel.webUrl = [storeDict objectForKey:@"web_url"];
                sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
                sModel.profileImage = [storeDict objectForKey:@"profile_image"];
                sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
                sModel.address = [storeDict objectForKey:@"address"];
                sModel.city = [storeDict objectForKey:@"city"];
                sModel.state = [storeDict objectForKey:@"state"];
                sModel.zip = [storeDict objectForKey:@"zip"];
                sModel.latitude = [storeDict objectForKey:@"latitude"];
                sModel.longitude = [storeDict objectForKey:@"longitude"];
                
                [appDel.userModel.allStores addObject:sModel];
            }
            
            [_top10TblView reloadData];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"We are not available at the moment" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if(request.tag == 3) {
        [CustomLoading DismissAlertMessage];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
        int success = [[result objectForKey:@"status"] intValue];
        
        if(success == 1) {
            NSDictionary *storeDict = (NSDictionary*)[result objectForKey:@"store_data"];
            StoreModel *sModel = [[StoreModel alloc] init];
            
            sModel.storeId = [storeDict objectForKey:@"store_id"];
            sModel.storeName = [storeDict objectForKey:@"store_name"];
            sModel.webUrl = [storeDict objectForKey:@"web_url"];
            sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
            sModel.profileImage = [storeDict objectForKey:@"profile_image"];
            sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
            sModel.address = [storeDict objectForKey:@"address"];
            sModel.city = [storeDict objectForKey:@"city"];
            sModel.state = [storeDict objectForKey:@"state"];
            sModel.zip = [storeDict objectForKey:@"zip"];
            sModel.latitude = [storeDict objectForKey:@"latitude"];
            sModel.longitude = [storeDict objectForKey:@"longitude"];
            
            [appDel.userModel.topTenStores addObject:sModel];
            [_top10TblView reloadData];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Authentication Error" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if(request.tag == 4) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
        int success = [[result objectForKey:@"status"] intValue];
        
        if(success == 1) {
            NSDictionary *storeDict = (NSDictionary*)[result objectForKey:@"payment_info"];
            PaymentMethod *pModel = [[PaymentMethod alloc] init];
            
            pModel.cardHolderName = [storeDict objectForKey:@"card_holder_name"];
            pModel.cardType = [storeDict objectForKey:@"card_type"];
            pModel.cardNumber = [storeDict objectForKey:@"card_number"];
            pModel.cvc = [storeDict objectForKey:@"cvc"];
            pModel.expiryMonth = [storeDict objectForKey:@"expiry_month"];
            pModel.expiryYear = [storeDict objectForKey:@"expiry_year"];
            
            appDel.userModel.paymentInfoModel = pModel;
            _cardTypeLbl.text = pModel.cardType;
            _cardNumberLbl.text = pModel.cardNumber;
            _cardHolderNameLbl.text = pModel.cardHolderName;
            _cvcLbl.text = pModel.cvc;
            _cardExpiryLbl.text = [NSString stringWithFormat:@"%@/%@",pModel.expiryMonth,pModel.expiryYear];
            
            _cardNumberTxt.text = pModel.cardNumber;
            _cardHolderNameTxt.text = pModel.cardHolderName;
            _cvcTxt.text = pModel.cvc;
            _cardExpiryTxt.text = [NSString stringWithFormat:@"%@/%@",pModel.expiryMonth,pModel.expiryYear];
            
            [_cardTypeBtn setTitle:appDel.userModel.paymentInfoModel.cardType forState:UIControlStateNormal];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Authentication Error" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if(request.tag == 5) {
        [CustomLoading DismissAlertMessage];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
        int success = [[result objectForKey:@"status"] intValue];
        
        if(success == 1 ) {
            
            if(request.tag == 5) {
                NSDictionary *storeDict = (NSDictionary*)[result objectForKey:@"payment_info"];
                PaymentMethod *pModel = [[PaymentMethod alloc] init];
                
                pModel.cardHolderName = [storeDict objectForKey:@"card_holder_name"];
                pModel.cardType = [storeDict objectForKey:@"card_type"];
                pModel.cardNumber = [storeDict objectForKey:@"card_number"];
                pModel.cvc = [storeDict objectForKey:@"cvc"];
                pModel.expiryMonth = [storeDict objectForKey:@"expiry_month"];
                pModel.expiryYear = [storeDict objectForKey:@"expiry_year"];
                
                appDel.userModel.paymentInfoModel = pModel;
                
                _cardTypeLbl.text = pModel.cardType;
                _cardNumberLbl.text = pModel.cardNumber;
                _cardHolderNameLbl.text = pModel.cardHolderName;
                _cvcLbl.text = pModel.cvc;
                _cardExpiryLbl.text = [NSString stringWithFormat:@"%@/%@",pModel.expiryMonth,pModel.expiryYear];
                
                _cardNumberTxt.text = pModel.cardNumber;
                _cardHolderNameTxt.text = pModel.cardHolderName;
                _cvcTxt.text = pModel.cvc;
                _cardExpiryTxt.text = [NSString stringWithFormat:@"%@/%@",pModel.expiryMonth,pModel.expiryYear];
                
                [_cardTypeBtn setTitle:appDel.userModel.paymentInfoModel.cardType forState:UIControlStateNormal];
            }
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Authentication Error" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
    else if (request.tag == 10){
        [CustomLoading DismissAlertMessage];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
        int success = [[result objectForKey:@"status"] intValue];
        
        if(success == 1) {
            
            NSString *message = [result objectForKey:@"message"];
            StoreModel *sModel = [appDel.userModel.topTenStores objectAtIndex:currentStoreIndex];
            
            if([message isEqualToString:@"Store added successfully."]){
                [appDel.userModel.favouriteStores addObject:sModel];
            }
            else{
                for(int i=0; i<appDel.userModel.favouriteStores.count; i++){
                    StoreModel *tempModel = [appDel.userModel.favouriteStores objectAtIndex:i];
                    
                    if ([tempModel.storeId isEqualToString:sModel.storeId]) {
                        [appDel.userModel.favouriteStores removeObject:tempModel];
                        break;
                    }
                }
            }        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"We are not available at the moment" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else if (request.tag == 11) {
        [CustomLoading DismissAlertMessage];
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
        int success = [[result objectForKey:@"status"] intValue];
        
        if(success == 1) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Submitted successfully" message:@"Thank you for filling this form. We value your feedback." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            alert.tag = 11;
            alert.delegate = self;
            [alert show];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Authentication Error" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }
}

- (void)requestFailed:(ASIHTTPRequest *)theRequest {
    [CustomLoading DismissAlertMessage];
    NSString *response = [[NSString alloc] initWithData:[theRequest responseData] encoding:NSUTF8StringEncoding];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Please try later!!!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
    
}

#pragma mark - AlertView delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(alertView.tag == 11) {
        _homeZipTxt.text = @"";
        [_houseHoldBtn setTitle:@"" forState:UIControlStateNormal];
        [_annualIncomeBtn setTitle:@"" forState:UIControlStateNormal];
        
        [_genericBtn setImage:[UIImage imageNamed:@"radio_button.png"] forState:UIControlStateNormal];
        [_dontCareBtn setImage:[UIImage imageNamed:@"radio_button.png"] forState:UIControlStateNormal];
        [_nameBrandBtn setImage:[UIImage imageNamed:@"radio_button.png"] forState:UIControlStateNormal];
    }
}



#pragma mark - Server Helper methods

- (void) fetchAllStores {
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    NSString *urlString = [NSString stringWithFormat:@"http://www.saivian-squad.com/stores/fetchAll"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    request = [ASIFormDataRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
    //[req setPostValue:strUserId forKey:@"customer_id"];
    
    [request setRequestMethod:@"GET"];
    
    request.tag = 2;
    
    [request setTimeOutSeconds:300];
    [request setDelegate:self];
    [request startAsynchronous];
}

#pragma mark Add Store Methods


- (IBAction)cancelStoreBtnPressed:(id)sender {
    
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:appDel.userModel.topTenStores.count inSection:0] ;
    
    AddStoreCell *cell = (AddStoreCell*)[_top10TblView cellForRowAtIndexPath:myIP];
    [cell.zipBtn setTitle:@"" forState:UIControlStateNormal];
    cell.storeAddress.text = @"";
    cell.storeName.text = @"";
    cell.phoneNum.text = @"";
    cell.stateTxt.text = @"";
    cell.cityTxt.text = @"";
    cell.stName.text = @"";
    
}

- (IBAction)submitStoreBtnPressed:(id)sender {
    
    [self.view endEditing:true];
    
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:appDel.userModel.topTenStores.count inSection:0] ;
    AddStoreCell *cell = (AddStoreCell*)[_top10TblView cellForRowAtIndexPath:myIP];
        
    if(cell.stName.text.length > 0 && cell.storeAddress.text.length > 0 && cell.storeName.text.length>0 && cell.phoneNum.text.length > 0 && cell.stateTxt.text.length > 0 && cell.cityTxt.text.length > 0)  {
        
        [CustomLoading showAlertMessage];
        
        NSURL *url = [NSURL URLWithString:@"http://www.saivian-squad.com/stores/add"];
        request = [ASIFormDataRequest requestWithURL:url];
        [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
        request.tag = 3;
        [request setPostValue:strUserId forKey:@"customer_id"];
        [request setPostValue:cell.stName.text forKey:@"store_name"];
        [request setPostValue:cell.storeAddress.text forKey:@"store_address"];
        [request setPostValue:cell.cityTxt.text forKey:@"city"];
        [request setPostValue:cell.stateTxt.text forKey:@"state"];
        [request setPostValue:cell.storeName.text forKey:@"zip"];
        [request setPostValue:cell.phoneNum.text forKey:@"phone_no"];
        
        [request setRequestMethod:@"POST"];
        [request setTimeOutSeconds:300];
        [request setDelegate:self];
        [request startAsynchronous];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something Missing" message:@"All fields regarding new store needs to be filled" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark Payment Methods

-(void) fetchPaymentMethods {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    NSString *urlString = [NSString stringWithFormat:@"http://www.saivian-squad.com/customer/fetchPaymentInfo?customer_id=%@",strUserId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    request = [ASIFormDataRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
    request.tag = 4;
    
    [request setRequestMethod:@"GET"];
    [request setTimeOutSeconds:300];
    [request setDelegate:self];
    [request startAsynchronous];
}

- (IBAction)cancelEditPaymentMethodsPressed:(id)sender {
    
    //_cardNumberTxt.text.length > 3 && _cardHolderNameTxt.text.length > 0 && _cardTypeBtn.titleLabel.text.length>0 && _cvcTxt.text.length > 0 && _cardExpiryTxt.text.length > 0
    _cardNumberTxt.text = @"";
    _cardHolderNameTxt.text = @"";
    [_cardTypeBtn setTitle:@"" forState:UIControlStateNormal];
    _cvcTxt.text = @"";
    _cardExpiryTxt.text = @"";
    
}

- (IBAction)submitCardInfoPressed:(id)sender {
    
    [self.view endEditing:true];
    
    if(_cardNumberTxt.text.length > 1 && _cardHolderNameTxt.text.length > 0 && _cardTypeBtn.titleLabel.text.length>0 && _cvcTxt.text.length > 0 && _cardExpiryTxt.text.length > 0) {
        
        NSString *str=_cardExpiryTxt.text;
        
        NSArray *items = [str componentsSeparatedByString:@"/"];
        
        NSString *str1=[items objectAtIndex:0];   //shows Description
        NSString *str2=[items objectAtIndex:1];
        
        [CustomLoading showAlertMessage];
        NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
        
        NSURL *url = [NSURL URLWithString:@"http://www.saivian-squad.com/customer/addPaymentInfo"];
        request = [ASIFormDataRequest requestWithURL:url];
        [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
        request.tag = 5;
        [request setPostValue:strUserId forKey:@"customer_id"];
        [request setPostValue:_cardHolderNameTxt.text forKey:@"card_holder_name"];
        [request setPostValue:_cardTypeBtn.titleLabel.text forKey:@"card_type"];
        [request setPostValue:_cardNumberTxt.text forKey:@"card_number"];
        [request setPostValue:_cvcTxt.text forKey:@"cvc"];
        [request setPostValue:str1 forKey:@"expiry_month"];
        [request setPostValue:str2 forKey:@"expiry_year"];
        [request setRequestMethod:@"POST"];
        [request setTimeOutSeconds:300];
        [request setDelegate:self];
        [request startAsynchronous];
        
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something Missing" message:@"All fields are needed to be filled" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (IBAction)expiryDatePressed:(id)sender {
    _expiryPickerView.hidden = false;
    _expiryDatePciker.minimumDate = [NSDate date];
    [_expiryDatePciker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.view endEditing:true];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [self.view addGestureRecognizer:tapRecognizer];
    self.view.tag=2;
}

- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"MM/yyyy"];
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:datePicker.date]];
    //assign text to label
    
    _cardExpiryTxt.text = str;
    
    
}

-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender
{
    if(sender.view.tag==2) {
        if(isEditing) {
            [self.view endEditing:YES];
            isEditing = false;
        }
    }
    [self.view removeGestureRecognizer:sender];
    
    _expiryPickerView.hidden = true;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void) SavianTabControllerPressed: (SavianTabController *) sender {
    if([_tabControl.direction isEqualToString:@"down"]) {
        
        _tabControl.direction = @"up";
        
        CGRect lowerBarFrame = _tabControl.frame;
        
        
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            if(IS_IPAD) {
                self.tabControl.frame  = CGRectMake(0, lowerBarFrame.origin.y-100, lowerBarFrame.size.width,lowerBarFrame.size.height);
            }
            else {
                self.tabControl.frame  = CGRectMake(0, lowerBarFrame.origin.y-60, lowerBarFrame.size.width,lowerBarFrame.size.height);
            }
            
            
        } completion:^(BOOL finished) {
            
        }];
    }
    else {
        [[NavigationHandler getInstance] MoveToCameraController];
    }
}

- (void) SavianTabControllerRecieptPressed: (SavianTabController *) sender {
    [[NavigationHandler getInstance] NavigateToRecieptScreen];
}
- (void) SavianTabControllerStorePressed:(SavianTabController *)sender {
    [[NavigationHandler getInstance] NavigateToStoreScreen];
}
@end
