//
//  CashBackLocalModel.h
//  Saivian
//
//  Created by Ahmed Sadiq on 30/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CashBackLocalModel : NSObject
@property (nonatomic, retain) NSString *cashBackId;
@property (nonatomic, retain) NSString *categoryName;
@property (nonatomic, retain) NSString *cashBackDescription;
@property (nonatomic, retain) NSString *image;
@property (strong, retain) NSMutableArray *storesList;
@property BOOL isActive;



@end
