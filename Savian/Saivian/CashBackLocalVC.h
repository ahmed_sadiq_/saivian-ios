//
//  CashBackLocalVC.h
//  Saivian
//
//  Created by Ahmed Sadiq on 11/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "StoreModel.h"
#import "ASIFormDataRequest.h"
#import "SavianTabController.h"
#import <CoreLocation/CoreLocation.h>
@interface CashBackLocalVC : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,ASIHTTPRequestDelegate,SavianTabControllerDelegate, CLLocationManagerDelegate> {
    int tableViewX;
    int tableViewY;
    int tableViewWidth;
    int tableViewHeight;
    
    int populator;
    AppDelegate *appDel;
    ASIFormDataRequest *request;
    StoreModel *sModelSelected;
    IBOutlet UIImageView *cbImg;
    IBOutlet UIImageView *isImg;
    CLLocation *currentLocation;
}
@property (nonatomic , strong) CLLocationManager *locationManager;
@property BOOL isInstantSaving;
@property (weak, nonatomic) IBOutlet UIView *lowerBar;
@property (strong, nonatomic) SavianTabController *tabControl;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) NSMutableDictionary *dataDictionary;
@property (strong, nonatomic) NSMutableArray *buttonsList;
@property (weak, nonatomic) IBOutlet UIButton *cashbackBtn;
@property (weak, nonatomic) IBOutlet UIButton *instantBtn;

@property (weak, nonatomic) IBOutlet UIScrollView *upperScroller;
@property (weak, nonatomic) IBOutlet UIScrollView *lowerScroller;
- (IBAction)sliderPressed:(id)sender;

- (IBAction)tabBarPressed:(id)sender;

- (IBAction)cbPressed:(id)sender;
- (IBAction)isPressed:(id)sender;
@end
