//
//  UserModel.h
//  Saivian
//
//  Created by Ahmed Sadiq on 25/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentMethod.h"


@interface UserModel : NSObject
@property (nonatomic, retain) NSString *userID;
@property (nonatomic, retain) NSString *referral_id;
@property (nonatomic, retain) NSString *saivian_id;
@property (nonatomic, retain) NSString *first_name;
@property (nonatomic, retain) NSString *last_name;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *password;
@property (nonatomic, retain) NSString *profile_image;
@property (nonatomic, retain) NSString *gender;
@property (nonatomic, retain) NSString *dob;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *state;
@property (nonatomic, retain) NSString *zip;
@property (nonatomic, retain) NSString *country;
@property (nonatomic, retain) NSString *phone_no;
@property (nonatomic, retain) NSString *is_verified;
@property (nonatomic, retain) NSString *is_active;
@property (nonatomic, retain) NSString *created_date;
@property (strong, retain) PaymentMethod *paymentInfoModel;
@property (strong, retain) NSMutableArray *cashBackLocalArray;
@property (strong, retain) NSMutableArray *topTenStores;
@property (strong, retain) NSMutableArray *favouriteStores;
@property (strong, retain) NSMutableArray *allStores;
@property (strong, retain) NSMutableArray *recieptArray;

@end
