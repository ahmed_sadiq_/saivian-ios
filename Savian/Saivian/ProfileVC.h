//
//  ProfileVC.h
//  Saivian
//
//  Created by Apple on 28/04/2016.
//  Copyright © 2016 Osama. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "AsyncImageView.h"

@interface ProfileVC : ViewController<UITextFieldDelegate,UIImagePickerControllerDelegate> {
    AppDelegate *appDel;
}
@property (strong, nonatomic) IBOutlet UIButton *editBtn;
@property (strong, nonatomic) IBOutlet AsyncImageView *profilePic;

@property (weak, nonatomic) IBOutlet UITextField *fNmeTxt;
@property (weak, nonatomic) IBOutlet UIImageView *fNmeImg;

@property (weak, nonatomic) IBOutlet UITextField *lNmeTxt;
@property (weak, nonatomic) IBOutlet UIImageView *lNmeImg;

@property (weak, nonatomic) IBOutlet UITextField *sIdTxt;
@property (weak, nonatomic) IBOutlet UIImageView *sIdImg;

@property (weak, nonatomic) IBOutlet UITextField *psdTxt;
@property (weak, nonatomic) IBOutlet UIImageView *pswdImg;

@property (weak, nonatomic) IBOutlet UITextField *emilTxt;
@property (weak, nonatomic) IBOutlet UIImageView *emailImg;

@property (weak, nonatomic) IBOutlet UITextField *phoneTxt;
@property (weak, nonatomic) IBOutlet UIImageView *phoneImg;


@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIView *whtsThisView;
@property (weak, nonatomic) IBOutlet UIView *urlView;
@property (weak, nonatomic) IBOutlet UITextField *urlTxt;


- (IBAction)editPressed:(id)sender;

- (IBAction)whatsThisPressed:(id)sender;
- (IBAction)shareBtnPressed:(id)sender;
- (IBAction)profilePicPressed:(id)sender;



@end
