//
//  SplashAnimationVC.m
//  Saivian
//
//  Created by Ahmed Sadiq on 24/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "SplashAnimationVC.h"
#import "NavigationHandler.h"
@interface SplashAnimationVC ()

@end

@implementation SplashAnimationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         self.shopImg.frame = self.shopView.frame;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.4
                                               delay:0.0
                                             options: UIViewAnimationCurveEaseIn
                                          animations:^{
                                              self.saveImg.frame = self.saveView.frame;
                                          }
                                          completion:^(BOOL finished){
                                              [UIView animateWithDuration:0.4
                                                                    delay:0.0
                                                                  options: UIViewAnimationCurveEaseIn
                                                               animations:^{
                                                                   self.shareImg.frame = self.shareView.frame;
                                                               }
                                                               completion:^(BOOL finished){
                                                                   [self showLabel];
                                                               }];
                                          }];
                     }];
}

- (void) showLabel {
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         _shopLbl.hidden = false;
                         
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.3
                                               delay:0.1
                                             options: UIViewAnimationCurveEaseIn
                                          animations:^{
                                              _saveLbl.hidden = false;
                                          }
                                          completion:^(BOOL finished){
                                              [UIView animateWithDuration:0.3
                                                                    delay:0.2
                                                                  options: UIViewAnimationCurveEaseIn
                                                               animations:^{
                                                                   _shareLbl.hidden = false;
                                                               }
                                                               completion:^(BOOL finished){
                                                                   [self hideImages];
                                                               }];
                                          }];
                     }];
}

- (void) hideImages {
    [UIView animateWithDuration:0.3
                          delay:0.4
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         self.shopImg.frame = CGRectMake(-85, _shopImg.frame.origin.y, _shareImg.frame.size.width, _shareImg.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.3
                                               delay:0.0
                                             options: UIViewAnimationCurveEaseIn
                                          animations:^{
                                              self.saveImg.frame = CGRectMake(-85, _saveImg.frame.origin.y, _shareImg.frame.size.width, _shareImg.frame.size.height);
                                          }
                                          completion:^(BOOL finished){
                                              [UIView animateWithDuration:0.3
                                                                    delay:0.0
                                                                  options: UIViewAnimationCurveEaseIn
                                                               animations:^{
                                                                   self.shareImg.frame = CGRectMake(-85, _shareImg.frame.origin.y, _shareImg.frame.size.width, _shareImg.frame.size.height);
                                                               }
                                                               completion:^(BOOL finished){
                                                                   _shopLbl.hidden = true;
                                                                   _shareLbl.hidden = true;
                                                                   _saveLbl.hidden = true;
                                                                   
                                                                   [self showLogo];
                                                               }];
                                          }];
                     }];
}

- (void) showLogo {
    _logo.hidden = false;
    _logo.transform=CGAffineTransformMakeScale(0.5, 0.5);
    [UIView animateWithDuration:0.4
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:(void (^)(void)) ^{
                         _logo.transform=CGAffineTransformMakeScale(1.2, 1.2);
                     }
                     completion:^(BOOL finished){
                         _logo.transform=CGAffineTransformIdentity;
                         [UIView animateWithDuration:0.4
                                               delay:0
                                             options:UIViewAnimationOptionBeginFromCurrentState
                                          animations:(void (^)(void)) ^{
                                              _logo.transform=CGAffineTransformMakeScale(1.0, 1.0);
                                          }
                                          completion:^(BOOL finished){
                                              _logo.transform=CGAffineTransformIdentity;
                                              
                                              BOOL isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLoggedIn"];
                                              if(isLoggedIn) {
                                                  [[NavigationHandler getInstance] NavigateToHomeScreen];
                                              }
                                              else {
                                                  [[NavigationHandler getInstance] loadFirstVC];
                                              }
                                              
                                          }];
                     }];
}
//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
