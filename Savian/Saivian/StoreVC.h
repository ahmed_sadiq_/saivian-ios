//
//  StoreVC.h
//  Saivian
//
//  Created by Ahmed Sadiq on 23/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "AppDelegate.h"
#import "ASIFormDataRequest.h"
#import "SavianTabController.h"

@interface StoreVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,NIDropDownDelegate,UIScrollViewDelegate,ASIHTTPRequestDelegate,SavianTabControllerDelegate,UIAlertViewDelegate> {
    ASIFormDataRequest *request;
    NIDropDown *dropDown;
    
    NSIndexPath *selectedIndexPath;
    int selectedStoreIndex;
    int currentStoreIndex;
    AppDelegate *appDel;
    NSString *lastAnswer;
    
    BOOL isEditing;
}
@property (weak, nonatomic) IBOutlet UIView *lowerBar;

@property (strong, nonatomic) SavianTabController *tabControl;

@property (strong, nonatomic) NSMutableArray *storeArray;
@property (strong, nonatomic) NSMutableArray *allStoresArray;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroller;

@property (weak, nonatomic) IBOutlet UIButton *top10Btn;

@property (weak, nonatomic) IBOutlet UIButton *methodPayment;
@property (weak, nonatomic) IBOutlet UIButton *marketResearchBtn;

@property (weak, nonatomic) IBOutlet UIView *top10StoresView;
@property (weak, nonatomic) IBOutlet UIView *paymentMethodView;
@property (weak, nonatomic) IBOutlet UITableView *top10TblView;

@property (weak, nonatomic) IBOutlet UITextField *homeZipTxt;

@property (weak, nonatomic) IBOutlet UIButton *nameBrandBtn;
@property (weak, nonatomic) IBOutlet UIButton *genericBtn;
@property (weak, nonatomic) IBOutlet UIButton *dontCareBtn;


//Method of Payment

@property (weak, nonatomic) IBOutlet UILabel *cardHolderNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *cardTypeLbl;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *cvcLbl;
@property (strong, nonatomic) IBOutlet UILabel *cardExpiryLbl;
@property (weak, nonatomic) IBOutlet UITextField *cardHolderNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTxt;
@property (strong, nonatomic) IBOutlet UITextField *cvcTxt;
@property (strong, nonatomic) IBOutlet UITextField *cardExpiryTxt;
@property (weak, nonatomic) IBOutlet UIButton *cardTypeBtn;
@property (strong, nonatomic) IBOutlet UIView *expiryPickerView;
@property (strong, nonatomic) IBOutlet UIDatePicker *expiryDatePciker;
@property (strong, nonatomic) IBOutlet UIButton *cardExpiryBtn;

- (IBAction)submitCardInfoPressed:(id)sender;
- (IBAction)expiryDatePressed:(id)sender;


//end of Payment Method properties
@property (strong, nonatomic) IBOutlet UIButton *houseHoldBtn;
@property (strong, nonatomic) IBOutlet UIButton *annualIncomeBtn;

- (IBAction)genericPRessed:(id)sender;
- (IBAction)dontCarePressed:(id)sender;

- (IBAction)houseHoldCountPressed:(id)sender;
- (IBAction)incomePressed:(id)sender;
- (IBAction)nameBrandPressed:(id)sender;


- (IBAction)top10Pressed:(id)sender;
- (IBAction)methodPaymnetPressed:(id)sender;
- (IBAction)marketResearchPressed:(id)sender;
- (IBAction)addStorePressed:(id)sender;
- (IBAction)cardTypePressed:(id)sender;

- (IBAction)cancelEditPaymentMethodsPressed:(id)sender;

- (IBAction)sliderPressed:(id)sender;

- (IBAction)submitOnlineSignature:(id)sender;




@end
