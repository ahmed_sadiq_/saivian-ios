//
//  RecieptVC.h
//  Saivian
//
//  Created by Ahmed Sadiq on 23/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "AppDelegate.h"

@interface RecieptVC : UIViewController <UITableViewDelegate,UITableViewDataSource,NIDropDownDelegate> {
    NIDropDown *dropDown;
    AppDelegate *appDel;
    
    BOOL isSearch;
    BOOL isMonthSelected;
    IBOutlet UIButton *monthBtn;
    int selectedIndex;
    NSArray * arr;
    IBOutlet UILabel *monthsPan;
    
}
@property (strong, nonatomic) NSMutableArray *searchArray;
@property (weak, nonatomic) IBOutlet UITextField *searchTxt;
@property (weak, nonatomic) IBOutlet UITableView *mainTblView;
- (IBAction)monthPressed:(id)sender;
- (IBAction)sliderPressed:(id)sender;

@end
