//
//  HomeVC.m
//  Saivian
//
//  Created by Apple on 28/04/2016.
//  Copyright © 2016 Osama. All rights reserved.
//

#import "HomeVC.h"
#import "Constants.h"
#import "DrawerVC.h"
#import "NavigationHandler.h"
#import "CustomLoading.h"
#import "StoreModel.h"
#import "CashBackLocalModel.h"
#import "OpeningHours.h"
#import "PaymentMethod.h"
#import "RecieptModel.h"
#import "UIImageView+RoundImage.h"
#import <QuartzCore/QuartzCore.h>

@interface HomeVC ()

@end

@implementation HomeVC

- (void)viewDidLayoutSubviews{
    if(_tabControl == nil) {
        _tabControl = [[SavianTabController alloc] showTabController:self.lowerBar andFrame:_lowerBar.frame];
        _tabControl.direction = @"down";
        _tabControl.delegate = self;
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    if(appDel.reloadImage) {
        
        self.profilePic.imageURL = [NSURL URLWithString:appDel.userModel.profile_image];
        NSURL *url = [NSURL URLWithString:appDel.userModel.profile_image];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        
        [self.profilePic.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        [self.profilePic.layer setBorderWidth: 2.0];
        
        [self.profilePic roundImageCorner];
        
        appDel.reloadImage = false;
    }
    [self populateData];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    // Do any additional setup after loading the view from its nib.
    UISwipeGestureRecognizer* sgr = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipe:)];
    [sgr setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:sgr];
    
    if(!appDel.isLoading) {
        [self populateData];
    }
    else{
        [CustomLoading showAlertMessage];
        [NSTimer scheduledTimerWithTimeInterval:0.3
                                         target:self
                                       selector:@selector(tick:)
                                       userInfo:nil
                                        repeats:YES];
    }
}

- (void) populateData {
    if(appDel.userModel) {
        self.profilePic.imageURL = [NSURL URLWithString:appDel.userModel.profile_image];
        NSURL *url = [NSURL URLWithString:appDel.userModel.profile_image];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        [self.profilePic.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        [self.profilePic.layer setBorderWidth: 2.0];
        
        [self.profilePic roundImageCorner];
        
        if(appDel.userModel.first_name && appDel.userModel.last_name) {
            _userName.text = [NSString stringWithFormat:@"%@ %@",appDel.userModel.first_name, appDel.userModel.last_name];
        }
        
        
        earnedAmount = 0.0;
        boundToCheck = [[[NSUserDefaults standardUserDefaults] objectForKey:@"monthCashBack"] intValue];
        
        if(earnedAmount < boundToCheck) {
            [NSTimer scheduledTimerWithTimeInterval:0.02
                                             target:self
                                           selector:@selector(updateAmountTimer:)
                                           userInfo:nil
                                            repeats:YES];
        }
        else {
            
            NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
            [dateformate setDateFormat:@"MMM, dd yyyy"]; // Date formater
            NSString *date = [dateformate stringFromDate:[NSDate date]];
            
            _amountLbl.alpha = 1.0f;
            _shareBtn.hidden = NO;
            _monthCashBackLbl.hidden = false;
            NSDateFormatter *weekday = [[NSDateFormatter alloc] init];
            [weekday setDateFormat: @"EEE"];
            
            _monthCashBackLbl.text = [NSString stringWithFormat:@"Month to Date Cash Back %@ %@",[weekday stringFromDate:[NSDate date]], date];
            [self buttonEffect];
        }
    }
    
}

- (void) tick:(NSTimer *) timer {
    if(!appDel.isLoading) {
        [timer invalidate];
        timer = nil;
        
        [self populateData];
    }
}

- (void) loadUserData {
    [CustomLoading showAlertMessage];
    
    
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    NSString *urlString = [NSString stringWithFormat:@"http://www.saivian-squad.com/merchant/fetchCashBackLocal?customer_id=%@",strUserId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    request = [ASIFormDataRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
    //[req setPostValue:strUserId forKey:@"customer_id"];
    
    [request setRequestMethod:@"GET"];
    [request setTimeOutSeconds:300];
    [request setDelegate:self];
    [request startAsynchronous];
}

- (void) updateAmountTimer : (NSTimer*) timer {
    earnedAmount = earnedAmount+8.50;
    
    if(earnedAmount >= boundToCheck) {
        
        [timer invalidate];
        timer = nil;
        [self startBlinkingLabel];
    }
    if(earnedAmount > boundToCheck) {
        earnedAmount = boundToCheck;
    }
    NSString *amountStr = [NSString stringWithFormat:@"$ %.2f",earnedAmount];
    _amountLbl.text = amountStr;
    
}
-(void) startBlinkingLabel
{
    _amountLbl.alpha = 0;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setFromValue:[NSNumber numberWithFloat:0.0]];
    [animation setToValue:[NSNumber numberWithFloat:1.0]];
    [animation setDuration:0.3f];
    [animation setRepeatCount:3];
    [animation setAutoreverses:YES];
    [animation setRemovedOnCompletion:NO];
    [animation setDelegate:self];
    [_amountLbl.layer addAnimation:animation forKey:@"animation"];
}
- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag{
    if(flag){
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"MMM, dd yyyy"]; // Date formater
        NSString *date = [dateformate stringFromDate:[NSDate date]];
        
        _amountLbl.alpha = 1.0f;
        _shareBtn.hidden = NO;
        _monthCashBackLbl.hidden = false;
        NSDateFormatter *weekday = [[NSDateFormatter alloc] init];
        [weekday setDateFormat: @"EEE"];
        
        _monthCashBackLbl.text = [NSString stringWithFormat:@"Month to Date Cash Back %@ %@",[weekday stringFromDate:[NSDate date]], date];
        [self buttonEffect];
    }
}

-(void)buttonEffect{
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform"];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.duration = 0.125;
    anim.repeatCount = 1;
    anim.autoreverses = YES;
    anim.removedOnCompletion = YES;
    anim.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)];
    [_shareBtn.layer addAnimation:anim forKey:nil];
}

-(void) stopBlinkingLabel
{
    // REMOVE ANIMATION
    [_amountLbl.layer removeAllAnimations];
    _amountLbl.alpha = 1.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ShowDrawer:(id)sender {
    
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
    
}
- (void)rightSwipe:(UISwipeGestureRecognizer *)gesture
{
    [self ShowDrawer:self];
}

- (void) SavianTabControllerPressed: (SavianTabController *) sender {
    if([_tabControl.direction isEqualToString:@"down"]) {
        
        _tabControl.direction = @"up";
        
        CGRect lowerBarFrame = _tabControl.frame;
        
        
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            if(IS_IPAD) {
                self.tabControl.frame  = CGRectMake(0, lowerBarFrame.origin.y-100, lowerBarFrame.size.width,lowerBarFrame.size.height);
            }
            else {
                self.tabControl.frame  = CGRectMake(0, lowerBarFrame.origin.y-60, lowerBarFrame.size.width,lowerBarFrame.size.height);
            }
            
            
        } completion:^(BOOL finished) {
            
        }];
    }
    else {
        [[NavigationHandler getInstance] MoveToCameraController];
    }
}

- (void) SavianTabControllerRecieptPressed: (SavianTabController *) sender {
    [[NavigationHandler getInstance] NavigateToRecieptScreen];
}
- (void) SavianTabControllerStorePressed:(SavianTabController *)sender {
    [[NavigationHandler getInstance] NavigateToStoreScreen];
}

- (IBAction)cashBackLocalPressed:(id)sender {
    [[NavigationHandler getInstance] MoveToCashBackLocal];
}



- (IBAction)monthToDatePressed:(id)sender {
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"MMM, dd yyyy"]; // Date formater
    NSString *date = [dateformate stringFromDate:[NSDate date]];
    
    NSDateFormatter *weekday = [[NSDateFormatter alloc] init];
    [weekday setDateFormat: @"EEE"];
    
    _monthCashBackLbl.hidden = false;
    _monthCashBackLbl.text = [NSString stringWithFormat:@"Month to Date Cash Back %@ %@",[weekday stringFromDate:[NSDate date]], date];
    _amountLbl.hidden = false;
    earnedAmount = 0.0;
    boundToCheck = [[[NSUserDefaults standardUserDefaults] objectForKey:@"monthCashBack"] intValue];
    
    if(earnedAmount < boundToCheck) {
        [NSTimer scheduledTimerWithTimeInterval:0.02
                                         target:self
                                       selector:@selector(updateAmountTimer:)
                                       userInfo:nil
                                        repeats:YES];
    }
    
    [monthBtn setImage:[UIImage imageNamed:@"month_to_date_btn_active.png"] forState:UIControlStateNormal];
    [yearBtn setImage:[UIImage imageNamed:@"year_to_date_btn.png"] forState:UIControlStateNormal];
    
}

- (IBAction)yearToDatePressed:(id)sender {
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"MMMM, dd yyyy"]; // Date formater
    NSString *date = [dateformate stringFromDate:[NSDate date]];
    
    NSDateFormatter *weekday = [[NSDateFormatter alloc] init];
    [weekday setDateFormat: @"EEE"];
    
    _monthCashBackLbl.hidden = false;
    _monthCashBackLbl.text = [NSString stringWithFormat:@"Year to Date Cash Back %@ %@",[weekday stringFromDate:[NSDate date]], date];
    _amountLbl.hidden = false;
    earnedAmount = 0.0;
    boundToCheck = [[[NSUserDefaults standardUserDefaults] objectForKey:@"yearCashBack"] intValue];
    
    if(earnedAmount < boundToCheck) {
        
        [NSTimer scheduledTimerWithTimeInterval:0.02
                                         target:self
                                       selector:@selector(updateAmountTimer:)
                                       userInfo:nil
                                        repeats:YES];
    }
    [monthBtn setImage:[UIImage imageNamed:@"month_to_date_btn.png"] forState:UIControlStateNormal];
    [yearBtn setImage:[UIImage imageNamed:@"year_to_date_btn_active.png"] forState:UIControlStateNormal];
}

#pragma mark - Sharing Buttons

- (IBAction)twitterBtnPressed:(id)sender {
//    NSURL *instagramURL = [NSURL URLWithString:@"twitter://"];
//    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
//        [[UIApplication sharedApplication] openURL:instagramURL];
//    }
//    else {
//        //Does not have instagram app
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.twitter.com/"]];
//    }
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        NSString *urlStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"urlShared"];
        
        if(urlStr) {
            NSString *finalShareableStr = [NSString stringWithFormat:@"Shop. Save. Share. @ Saivian: %@",urlStr];
            [mySLComposerSheet setInitialText:finalShareableStr];
            [mySLComposerSheet addURL:[NSURL URLWithString:urlStr]];
        }
        else {
            [mySLComposerSheet setInitialText:@"Shop. Save. Share. @ Saivian: https://www.saivian.net"];
            [mySLComposerSheet addURL:[NSURL URLWithString:@"https://www.saivian.net"]];
        }
        
        
        
        [mySLComposerSheet addImage:[UIImage imageNamed:@"icon_40@2x.png"]];
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else {
        
        NSString *message;
        NSString *title;
        message = @"Go to Device Settings and set up Twitter Account";
        title   = @"Account Configuration Error";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)fbBtnPressed:(id)sender {
//    NSURL *instagramURL = [NSURL URLWithString:@"fb://"];
//    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
//        [[UIApplication sharedApplication] openURL:instagramURL];
//    }
//    else {
//        //Does not have instagram app
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/"]];
//    }
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        NSString *urlStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"urlShared"];
        
        if(urlStr) {
            NSString *finalShareableStr = [NSString stringWithFormat:@"Shop. Save. Share. @ Saivian: %@",urlStr];
            [mySLComposerSheet setInitialText:finalShareableStr];
            [mySLComposerSheet addURL:[NSURL URLWithString:urlStr]];
        }
        else {
            [mySLComposerSheet setInitialText:@"Shop. Save. Share. @ Saivian: https://www.saivian.net"];
            [mySLComposerSheet addURL:[NSURL URLWithString:@"https://www.saivian.net"]];
        }
        
        [mySLComposerSheet addImage:[UIImage imageNamed:@"icon_40@2x.png"]];
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else {
        
        NSString *message;
        NSString *title;
        message = @"Go to Device Settings and set up Facebook Account";
        title = @"Account Configuration Error";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)instaBtnPRessed:(id)sender {
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        [[UIApplication sharedApplication] openURL:instagramURL];
    }
    else {
        //Does not have instagram app
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.instagram.com/"]];
    }
}

- (IBAction)mailBtnPRessed:(id)sender {
    NSString *emailTitle = @"Saivian";
    
    NSString *urlStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"urlShared"];
    
    NSString *messageBody = @"Shop. Save. Share. @ Saivian: https://www.saivian.net";
    
    if(urlStr) {
        messageBody = [NSString stringWithFormat:@"Shop. Save. Share. @ Saivian: %@",urlStr];
        
    }
    else {
        messageBody = @"Shop. Save. Share. @ Saivian: https://www.saivian.net";
    }
    
    // Email Content
    
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@""];
    
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    

    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    if(mc == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Configuration Error" message:@"Please configure atleast one email address in device settings" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }else {
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
    
}

- (IBAction)textingBtnPressed:(id)sender {
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        NSString *urlStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"urlShared"];
        
        if(urlStr) {
            controller.body = [NSString stringWithFormat:@"Shop. Save. Share. @ Saivian: %@",urlStr];
        }
        else {
            controller.body = @"Shop. Save. Share. @ Saivian: https://www.saivian.net";
        }
        
        controller.recipients = nil;
        controller.messageComposeDelegate = self;
        // Present mail view controller on screen
        [self presentViewController:controller animated:YES completion:NULL];
    }
}

- (IBAction)sharingBtnPressed:(id)sender {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [_overlayView addGestureRecognizer:tapRecognizer];
    _overlayView.tag=2;
    _overlayView.alpha = 1.0;
    
    _sharingView.transform=CGAffineTransformMakeScale(2.5, 2.5);
    
    _overlayView.hidden = false;
    
    [UIView animateWithDuration:0.4
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:(void (^)(void)) ^{
                         _sharingView.transform=CGAffineTransformMakeScale(1.0, 1.0);
                     }
                     completion:^(BOOL finished){
                         _sharingView.transform=CGAffineTransformIdentity;
                         
                     }];
    
}
-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender
{
    if(sender.view.tag==2) {
        //do something here
        _overlayView.alpha = 0.75;
        [UIView animateWithDuration:0.4
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:(void (^)(void)) ^{
                             _sharingView.transform=CGAffineTransformMakeScale(2.5, 2.5);
                             
                         }
                         completion:^(BOOL finished){
                             _sharingView.transform=CGAffineTransformIdentity;
                             
                             _overlayView.hidden = true;
                         }];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [CustomLoading DismissAlertMessage];
    // NSString *response = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
    int success = [[result objectForKey:@"status"] intValue];
    
    if(success == 1) {
        
        int monthCashBack = [[result objectForKey:@"month_cashback"] intValue];
        int yearCashBack = [[result objectForKey:@"year_cashback"] intValue];
        
        NSString *strMonthCashBack = [NSString stringWithFormat:@"%d",monthCashBack];
        NSString *strYearCashBack = [NSString stringWithFormat:@"%d",yearCashBack];
        
        [[NSUserDefaults standardUserDefaults] setObject:strMonthCashBack forKey:@"monthCashBack"];
        [[NSUserDefaults standardUserDefaults] setObject:strYearCashBack forKey:@"yearCashBack"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSDictionary *user = [result objectForKey:@"customer_data"];
        
        appDel.userModel = [[UserModel alloc] init];
        
        appDel.userModel.userID = [user objectForKey:@"id"];
        appDel.userModel.referral_id = [user objectForKey:@"referral_id"];
        appDel.userModel.first_name = [user objectForKey:@"first_name"];
        appDel.userModel.last_name = [user objectForKey:@"last_name"];
        appDel.userModel.email = [user objectForKey:@"email"];
        appDel.userModel.password = [user objectForKey:@"password"];
        appDel.userModel.profile_image = [user objectForKey:@"profile_image"];
        appDel.userModel.gender = [user objectForKey:@"gender"];
        appDel.userModel.dob = [user objectForKey:@"dob"];
        appDel.userModel.city = [user objectForKey:@"city"];
        appDel.userModel.state = [user objectForKey:@"state"];
        appDel.userModel.zip = [user objectForKey:@"zip"];
        appDel.userModel.country = [user objectForKey:@"country"];
        appDel.userModel.phone_no = [user objectForKey:@"phone_no"];
        appDel.userModel.is_verified = [user objectForKey:@"is_verified"];
        appDel.userModel.is_active = [user objectForKey:@"is_active"];
        appDel.userModel.created_date = [user objectForKey:@"created_date"];
        
        self.profilePic.imageURL = [NSURL URLWithString:appDel.userModel.profile_image];
        NSURL *url = [NSURL URLWithString:appDel.userModel.profile_image];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        
        [self.profilePic.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        [self.profilePic.layer setBorderWidth: 2.0];
        
        [self.profilePic roundImageCorner];
        
        _userName.text = [NSString stringWithFormat:@"%@ %@",appDel.userModel.first_name, appDel.userModel.last_name];
        
        earnedAmount = 0.0;
        boundToCheck = [[[NSUserDefaults standardUserDefaults] objectForKey:@"monthCashBack"] intValue];
        
        appDel.userModel.cashBackLocalArray = [[NSMutableArray alloc] init];
        
        NSArray *cashBackData = [result objectForKey:@"cash_back_data"];
        
        for(int i=0; i<cashBackData.count; i++) {
            CashBackLocalModel *cblModel = [[CashBackLocalModel alloc] init];
            NSDictionary *tempDict = [cashBackData objectAtIndex:i];
            
            cblModel.cashBackId = [tempDict objectForKey:@"id"];
            cblModel.categoryName = [tempDict objectForKey:@"category_name"];
            cblModel.cashBackDescription = [tempDict objectForKey:@"description"];
            cblModel.image = [tempDict objectForKey:@"image"];
            cblModel.isActive = [[tempDict objectForKey:@"is_active"] boolValue];
            cblModel.storesList = [[NSMutableArray alloc] init];
            
            NSArray *storesData = [tempDict objectForKey:@"stores_list"];
            
            for(int j=0; j<storesData.count; j++) {
                NSDictionary *storeDict = [storesData objectAtIndex:j];
                
                StoreModel *sModel = [[StoreModel alloc] init];
                
                sModel.storeId = [storeDict objectForKey:@"store_id"];
                sModel.storeName = [storeDict objectForKey:@"store_name"];
                sModel.firstName = [storeDict objectForKey:@"first_name"];
                sModel.lastName = [storeDict objectForKey:@"last_name"];
                sModel.storeDescription = [storeDict objectForKey:@"description"];
                sModel.email = [storeDict objectForKey:@"email"];
                sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
                sModel.webUrl = [storeDict objectForKey:@"web_url"];
                sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
                sModel.profileImage = [storeDict objectForKey:@"profile_image"];
                sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
                
                NSDictionary *locationDict = [storeDict objectForKey:@"location"];
                
                sModel.address = [locationDict objectForKey:@"address"];
                sModel.city = [locationDict objectForKey:@"city"];
                sModel.state = [locationDict objectForKey:@"state"];
                sModel.zip = [locationDict objectForKey:@"zip"];
                sModel.country = [locationDict objectForKey:@"country"];
                sModel.latitude = [locationDict objectForKey:@"latitude"];
                sModel.longitude = [locationDict objectForKey:@"longitude"];
                
                
                sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
                
                
                [cblModel.storesList addObject:sModel];
            }
            [appDel.userModel.cashBackLocalArray addObject:cblModel];
        }
        
        
        appDel.userModel.topTenStores = [[NSMutableArray alloc] init];
        
        NSArray *topTenStoresData = [result objectForKey:@"top_ten_stores"];
        
        for(int i=0; i<topTenStoresData.count; i++) {
            NSDictionary *storeDict = [topTenStoresData objectAtIndex:i];
            
            StoreModel *sModel = [[StoreModel alloc] init];
            
            sModel.storeId = [storeDict objectForKey:@"store_id"];
            sModel.storeName = [storeDict objectForKey:@"store_name"];
            sModel.firstName = [storeDict objectForKey:@"first_name"];
            sModel.lastName = [storeDict objectForKey:@"last_name"];
            sModel.storeDescription = [storeDict objectForKey:@"description"];
            sModel.email = [storeDict objectForKey:@"email"];
            sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
            sModel.webUrl = [storeDict objectForKey:@"web_url"];
            sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
            sModel.profileImage = [storeDict objectForKey:@"profile_image"];
            sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
            
            NSDictionary *locationDict = [storeDict objectForKey:@"location"];
            
            sModel.address = [locationDict objectForKey:@"address"];
            sModel.city = [locationDict objectForKey:@"city"];
            sModel.state = [locationDict objectForKey:@"state"];
            sModel.zip = [locationDict objectForKey:@"zip"];
            sModel.country = [locationDict objectForKey:@"country"];
            sModel.latitude = [locationDict objectForKey:@"latitude"];
            sModel.longitude = [locationDict objectForKey:@"longitude"];
            
            
            sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
            
            [appDel.userModel.topTenStores addObject:sModel];
        }
        
        appDel.userModel.favouriteStores = [[NSMutableArray alloc] init];
        
        NSArray *favouriteStoresData = [result objectForKey:@"favourite_stores"];
        
        for(int i=0; i<favouriteStoresData.count; i++) {
            NSDictionary *storeDict = [favouriteStoresData objectAtIndex:i];
            
            StoreModel *sModel = [[StoreModel alloc] init];
            
            sModel.storeId = [storeDict objectForKey:@"store_id"];
            sModel.storeName = [storeDict objectForKey:@"store_name"];
            sModel.firstName = [storeDict objectForKey:@"first_name"];
            sModel.lastName = [storeDict objectForKey:@"last_name"];
            sModel.storeDescription = [storeDict objectForKey:@"description"];
            sModel.email = [storeDict objectForKey:@"email"];
            sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
            sModel.webUrl = [storeDict objectForKey:@"web_url"];
            sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
            sModel.profileImage = [storeDict objectForKey:@"profile_image"];
            sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
            
            NSDictionary *locationDict = [storeDict objectForKey:@"location"];
            
            sModel.address = [locationDict objectForKey:@"address"];
            sModel.city = [locationDict objectForKey:@"city"];
            sModel.state = [locationDict objectForKey:@"state"];
            sModel.zip = [locationDict objectForKey:@"zip"];
            sModel.country = [locationDict objectForKey:@"country"];
            sModel.latitude = [locationDict objectForKey:@"latitude"];
            sModel.longitude = [locationDict objectForKey:@"longitude"];
            
            
            sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
            
            [appDel.userModel.favouriteStores addObject:sModel];
        }
        
        appDel.userModel.allStores = [[NSMutableArray alloc] init];
        
        NSArray *allStoresData = [result objectForKey:@"all_stores_list"];
        
        for(int i=0; i<allStoresData.count; i++) {
            NSDictionary *storeDict = [allStoresData objectAtIndex:i];
            
            StoreModel *sModel = [[StoreModel alloc] init];
            
            sModel.storeId = [storeDict objectForKey:@"store_id"];
            sModel.storeName = [storeDict objectForKey:@"store_name"];
            sModel.firstName = [storeDict objectForKey:@"first_name"];
            sModel.lastName = [storeDict objectForKey:@"last_name"];
            sModel.storeDescription = [storeDict objectForKey:@"description"];
            sModel.email = [storeDict objectForKey:@"email"];
            sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
            sModel.webUrl = [storeDict objectForKey:@"web_url"];
            sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
            sModel.profileImage = [storeDict objectForKey:@"profile_image"];
            sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
            
            NSDictionary *locationDict = [storeDict objectForKey:@"location"];
            
            sModel.address = [locationDict objectForKey:@"address"];
            sModel.city = [locationDict objectForKey:@"city"];
            sModel.state = [locationDict objectForKey:@"state"];
            sModel.zip = [locationDict objectForKey:@"zip"];
            sModel.country = [locationDict objectForKey:@"country"];
            sModel.latitude = [locationDict objectForKey:@"latitude"];
            sModel.longitude = [locationDict objectForKey:@"longitude"];
            
            
            sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
            
            [appDel.userModel.allStores addObject:sModel];
        }
        
        appDel.userModel.recieptArray = [[NSMutableArray alloc] init];
        
        NSArray *recieptsData = [result objectForKey:@"receipts_list"];
        
        for(int i=0; i<recieptsData.count; i++) {
            NSDictionary *recieptDict = [recieptsData objectAtIndex:i];
            
            RecieptModel *rModel = [[RecieptModel alloc] init];
            
            rModel.recieptId = [recieptDict objectForKey:@"receipt_id"];
            rModel.storeName = [recieptDict objectForKey:@"store_name"];
            rModel.recieptType = [recieptDict objectForKey:@"receipt_type"];
            rModel.purchasedDate = [recieptDict objectForKey:@"purchased_date"];
            rModel.subtotalAmount = [recieptDict objectForKey:@"subtotal_amount"];
            rModel.uploadedDate = [recieptDict objectForKey:@"uploaded_date"];
            
            [appDel.userModel.recieptArray addObject:rModel];
        }
        
        NSDictionary *storeDict = (NSDictionary*)[result objectForKey:@"payment_info"];
        appDel.userModel.paymentInfoModel = [[PaymentMethod alloc] init];
        
        appDel.userModel.paymentInfoModel.cardHolderName = [storeDict objectForKey:@"card_holder_name"];
        appDel.userModel.paymentInfoModel.cardType = [storeDict objectForKey:@"card_type"];
        appDel.userModel.paymentInfoModel.cardNumber = [storeDict objectForKey:@"card_number"];
        appDel.userModel.paymentInfoModel.cvc = [storeDict objectForKey:@"cvc"];
        appDel.userModel.paymentInfoModel.expiryMonth = [storeDict objectForKey:@"expiry_month"];
        appDel.userModel.paymentInfoModel.expiryYear = [storeDict objectForKey:@"expiry_year"];
        
        
        
        [[NSUserDefaults standardUserDefaults] setObject:appDel.userModel.userID forKey:@"userID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        if(earnedAmount < boundToCheck) {
            [NSTimer scheduledTimerWithTimeInterval:0.02
                                             target:self
                                           selector:@selector(updateAmountTimer:)
                                           userInfo:nil
                                            repeats:YES];
            
            
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Verification Error" message:@"We are not available at the moment" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

- (void)requestFailed:(ASIHTTPRequest *)theRequest {
    [CustomLoading DismissAlertMessage];
    NSString *response = [[NSString alloc] initWithData:[theRequest responseData] encoding:NSUTF8StringEncoding];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Something went wrong!!!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
    
}


- (IBAction)instantSavingPressed:(id)sender {
    [[NavigationHandler getInstance] MoveToInstantSaving];
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
