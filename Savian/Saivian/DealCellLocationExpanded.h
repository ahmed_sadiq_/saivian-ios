//
//  DealCellLocationExpanded.h
//  Saivian
//
//  Created by Ahmed Sadiq on 18/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface DealCellLocationExpanded : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *locationBtn;
@property (weak, nonatomic) IBOutlet UIButton *hoursBtn;
@property (strong, nonatomic) IBOutlet MKMapView *map;
@property (strong, nonatomic) IBOutlet UIButton *webBtn;

@property (strong, nonatomic) IBOutlet UIButton *heartBtn;

@property (strong, nonatomic) IBOutlet UIButton *callBtn;

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) IBOutlet UILabel *addressLbl;
@property (strong, nonatomic) IBOutlet UILabel *addressLbl2;
@property (strong, nonatomic) IBOutlet UILabel *phoneLbl;
@property (strong, nonatomic) IBOutlet UILabel *stateLbl;
@end
