//
//  InstantSavingModel.h
//  Saivian
//
//  Created by Ahmed Sadiq on 19/06/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstantSavingModel : NSObject
@property (strong, retain) NSMutableArray *categoriesList;
@end
