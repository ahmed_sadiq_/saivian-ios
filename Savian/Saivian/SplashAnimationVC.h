//
//  SplashAnimationVC.h
//  Saivian
//
//  Created by Ahmed Sadiq on 24/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"

@interface SplashAnimationVC : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *shopImg;
@property (weak, nonatomic) IBOutlet UIImageView *saveImg;
@property (weak, nonatomic) IBOutlet UIImageView *shareImg;
@property (weak, nonatomic) IBOutlet UIView *shopView;
@property (weak, nonatomic) IBOutlet UIView *saveView;
@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet UILabel *shopLbl;
@property (weak, nonatomic) IBOutlet UILabel *saveLbl;
@property (weak, nonatomic) IBOutlet UILabel *shareLbl;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@end
