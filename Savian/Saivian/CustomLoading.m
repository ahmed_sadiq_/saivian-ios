//
//  CustomLoading.m
//  Wits
//
//  Created by Obaid ur Rehman on 01/09/2015.
//  Copyright (c) 2015 Xint Solutions. All rights reserved.
//

#import "CustomLoading.h"
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@implementation CustomLoading
static BOOL showing;


CustomLoading *loaderObj;
@synthesize coinImage = _coinImage;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */


+ (void)showAlertMessage
{
    
    if(showing)
    {
        return;
    }
    showing = true;
    loaderObj = [[[NSBundle mainBundle] loadNibNamed:@"CustomLoading" owner:nil options:nil] objectAtIndex:0];
    [loaderObj showPrivate];
    
}

-(void)showPrivate
{
    
    UIWindow *window =  [[UIApplication sharedApplication].delegate window];
    self.frame = window.frame;
    [window addSubview:self];
    self.center = window.center;
    
    [self frameAnimations];
    
    // [self startTimer];
    
    //     CATransition* transition = [CATransition animation];
    //     transition.startProgress = 0;
    //     transition.endProgress = 1.0;
    //     transition.type = @"flip";
    //     transition.subtype = @"fromRight";
    //     transition.duration = 2;
    //     transition.repeatCount = 100000;
    //     [_coinImage.layer addAnimation:transition forKey:@"transition"];
    //
    
    //     CABasicAnimation* rotationAnimation;
    //     rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    //     rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * 3*4 ];
    //     rotationAnimation.duration = 2;
    //     rotationAnimation.cumulative = YES;
    //     rotationAnimation.repeatCount = 10000;
    //
    //     [_coinImage.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    //     CABasicAnimation* rotationAnimation;
    //     rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    //     rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * 2 * 0.8 ];
    //     rotationAnimation.duration = 0.8;
    //     rotationAnimation.cumulative = YES;
    //     rotationAnimation.repeatCount = 5;
    //
    //     [_coinImage.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    // UIView *myView = cell.containerView;
    //     [self performAnimation];
    
    
    
    
}

-(void) performAnimation
{
    CALayer *layer = _coinImage.layer;
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform.m34 = 1.0 / -1000;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, M_PI / 0.2, 0.0f, 1.0f, 0.0f);
    layer.transform = rotationAndPerspectiveTransform;
    [UIView animateWithDuration:0.1 animations:^{
        layer.transform = CATransform3DIdentity;
        
    }];
    
    // [self performAnimation];
}
- (void) startTimer {
    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(tick:)
                                   userInfo:nil
                                    repeats:YES];
}

- (void) tick:(NSTimer *) timer {
    [self performAnimation];
    
}
+(void) DismissAlertMessage
{
    
    CustomLoading *cusLoading = [[CustomLoading alloc]init];
    if(showing)
    {
        
        [loaderObj dissmiss ];
    }
    
    
    //     [CustomLoading ];
    
    
}
- (void)dissmiss
{
    showing = false;
    _coinImage = nil;
    
    [self removeFromSuperview];
    
}

-(void)frames
{
    
    //     CGRect imageFrame = CGRectMake(
    //
    //                                    CGRectGetMidX(self.view.frame) - CGRectGetMidX(_coinImage.frame),
    //                                    CGRectGetMidY(self.view.frame) - CGRectGetMidY(_coinImage.frame),
    //                                    CGRectGetWidth(_coinImage.frame),
    //                                    CGRectGetHeight(_coinImage.frame));
}



-(void)frameAnimations
{
    _coinImage.animationImages = @[
                                   [UIImage imageNamed:@"shorter1.png"],
                                   [UIImage imageNamed:@"shorter2.png"],
                                   [UIImage imageNamed:@"shorter3.png"],
                                   [UIImage imageNamed:@"shorter4.png"],
                                   [UIImage imageNamed:@"shorter5.png"],
                                   [UIImage imageNamed:@"shorter6.png"],
                                   [UIImage imageNamed:@"shorter7.png"],
                                   [UIImage imageNamed:@"shorter8.png"],
                                   [UIImage imageNamed:@"shorter9.png"],
                                   [UIImage imageNamed:@"shorter10.png"],
                                   [UIImage imageNamed:@"shorter11.png"],
                                   [UIImage imageNamed:@"shorter12.png"],
                                   [UIImage imageNamed:@"shorter13.png"],
                                   [UIImage imageNamed:@"shorter14.png"],
                                   [UIImage imageNamed:@"shorter15.png"],
                                   [UIImage imageNamed:@"shorter16.png"],
                                   [UIImage imageNamed:@"shorter17.png"],
                                   [UIImage imageNamed:@"shorter18.png"],
                                   [UIImage imageNamed:@"shorter19.png"],
                                   [UIImage imageNamed:@"shorter20.png"],
                                   [UIImage imageNamed:@"shorter21.png"],
                                   [UIImage imageNamed:@"shorter22.png"],
                                   [UIImage imageNamed:@"shorter23.png"],
                                   [UIImage imageNamed:@"shorter24.png"],
                                   [UIImage imageNamed:@"shorter25.png"],
                                   [UIImage imageNamed:@"shorter26.png"],
                                   [UIImage imageNamed:@"shorter27.png"],
                                   [UIImage imageNamed:@"shorter28.png"],
                                   [UIImage imageNamed:@"shorter29.png"],
                                   [UIImage imageNamed:@"shorter30.png"],
                                   [UIImage imageNamed:@"shorter31.png"],
                                   [UIImage imageNamed:@"shorter32.png"],
                                   [UIImage imageNamed:@"shorter33.png"],
                                   [UIImage imageNamed:@"shorter34.png"],
                                   [UIImage imageNamed:@"shorter35.png"],
                                   [UIImage imageNamed:@"shorter36.png"],
                                   [UIImage imageNamed:@"shorter37.png"],
                                   [UIImage imageNamed:@"shorter38.png"],
                                   [UIImage imageNamed:@"shorter39.png"],
                                   [UIImage imageNamed:@"shorter40.png"],
                                   [UIImage imageNamed:@"shorter41.png"],
                                   [UIImage imageNamed:@"shorter42.png"],
                                   [UIImage imageNamed:@"shorter43.png"],];
    _coinImage.animationDuration = 1.50;
    [_coinImage startAnimating];
}

@end
