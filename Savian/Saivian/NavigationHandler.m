//
//  NavigationHandler.m
//  Saivian
//
//  Created by Apple on 27/04/2016.
//  Copyright © 2016 Osama. All rights reserved.
//

#import "NavigationHandler.h"
#import "ViewController.h"
#import "CameraControllerVC.h"
#import "HomeVC.h"
#import "ProfileVC.h"
#import "RecieptVC.h"
#import "StoreVC.h"
#import "CashBackLocalVC.h"
#import "SplashAnimationVC.h"
#import "ForgotPasswordVC.h"
#import "Constants.h"
@implementation NavigationHandler

- (id)initWithMainWindow:(UIWindow *)_tempWindow{
    
    if(self = [super init])
    {
        _window = _tempWindow;
    }
    instance = self;
    return self;
}

static NavigationHandler *instance= NULL;

+(NavigationHandler *)getInstance
{
    if (instance == nil) {
        instance = [[super alloc] init];
    }
    
    return instance;
}

-(void) loadSplashAnimaion {
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    SplashAnimationVC *_mainVC;
    if(IS_IPAD) {
        _mainVC = [[SplashAnimationVC alloc] initWithNibName:@"SplashAnimation_iPad" bundle:nil];
    }
    else {
        _mainVC = [[SplashAnimationVC alloc] initWithNibName:@"SplashAnimationVC" bundle:nil];
    }
    
    navController = [[UINavigationController alloc] initWithRootViewController:_mainVC];
    _window.rootViewController = navController;
    [navController setNavigationBarHidden:YES];
}

-(void)loadFirstVC{
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication]delegate];
    ViewController *_mainVC;
    if(IS_IPAD) {
        _mainVC = [[ViewController alloc] initWithNibName:@"ViewController_iPad" bundle:nil];
    }
    else {
        _mainVC = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    }
    
    navController = [[UINavigationController alloc] initWithRootViewController:_mainVC];
    _window.rootViewController = navController;
    [navController setNavigationBarHidden:YES];
}
-(void)NavigateToHomeScreen{
    
    HomeVC *homeVC;
    if(IS_IPAD){
        homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC_iPad" bundle:nil];
    }
    else{
        homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    }
    
    navController = [[UINavigationController alloc] initWithRootViewController:homeVC];
    
    _window.rootViewController = navController;
    [navController setNavigationBarHidden:YES];
}
-(void)MoveToProfile{
    
    [navController popToRootViewControllerAnimated:NO];
    ProfileVC *myBeam = [[ProfileVC alloc] initWithNibName:@"ProfileVC" bundle:nil];
    [navController pushViewController:myBeam animated:YES];
    
}

-(void)MoveToCashBackLocal{
    
    [navController popToRootViewControllerAnimated:NO];
    CashBackLocalVC *myBeam;
    if(IS_IPAD) {
        myBeam = [[CashBackLocalVC alloc] initWithNibName:@"CashBackLocalVC_iPad" bundle:nil];
    }
    else {
        myBeam = [[CashBackLocalVC alloc] initWithNibName:@"CashBackLocalVC" bundle:nil];
    }
    
    [navController pushViewController:myBeam animated:YES];
    
}

-(void)MoveToInstantSaving{
    
    [navController popToRootViewControllerAnimated:NO];
    CashBackLocalVC *myBeam = [[CashBackLocalVC alloc] initWithNibName:@"CashBackLocalVC" bundle:nil];
    myBeam.isInstantSaving = true;
    [navController pushViewController:myBeam animated:YES];
    
}

-(void)NavigateToRecieptScreen {
    RecieptVC *myBeam;
    if(IS_IPAD) {
        myBeam = [[RecieptVC alloc] initWithNibName:@"RecieptVC_iPad" bundle:nil];
    }
    else {
        myBeam = [[RecieptVC alloc] initWithNibName:@"RecieptVC" bundle:nil];
    }
    
    [navController pushViewController:myBeam animated:NO];
}

-(void)NavigateToForgotPassword {
    ForgotPasswordVC *myBeam;
    if(IS_IPAD) {
        myBeam = [[ForgotPasswordVC alloc] initWithNibName:@"ForgotPassword_iPad" bundle:nil];
    }
    else {
        myBeam = [[ForgotPasswordVC alloc] initWithNibName:@"ForgotPasswordVC" bundle:nil];
    }
    
    [UIView transitionWithView:navController.view
                      duration:0.75
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        [navController pushViewController:myBeam animated:NO];
                    }
                    completion:nil];
    
    
}

-(void)NavigateToStoreScreen {
    StoreVC *myBeam;
    if(IS_IPAD) {
        myBeam = [[StoreVC alloc] initWithNibName:@"StoreVC_iPad" bundle:nil];
    }
    else {
        myBeam = [[StoreVC alloc] initWithNibName:@"StoreVC" bundle:nil];
    }
    
    [navController pushViewController:myBeam animated:NO];
}

-(void)MoveToCameraController {
    CameraControllerVC *myBeam = [[CameraControllerVC alloc] initWithNibName:@"CameraControllerVC" bundle:nil];
    [navController pushViewController:myBeam animated:NO];
}
@end
