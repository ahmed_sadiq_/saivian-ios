//
//  DealsModel.h
//  Saivian
//
//  Created by Ahmed Sadiq on 16/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealsModel : NSObject

@property (nonatomic, retain) NSString *urlPath;
@property int state;
@property int belongTo;

@end
