//
//  DealCellExpanded.h
//  Saivian
//
//  Created by Ahmed Sadiq on 16/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface DealCellExpanded : UITableViewCell
@property (weak, nonatomic) IBOutlet AsyncImageView *imgView;

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) IBOutlet UILabel *addressLbl;
@property (strong, nonatomic) IBOutlet UILabel *addressLbl2;
@property (strong, nonatomic) IBOutlet UILabel *phoneLbl;
@property (strong, nonatomic) IBOutlet UILabel *stateLbl;
@property (strong, nonatomic) IBOutlet UIButton *webBtn;


@property (strong, nonatomic) IBOutlet UIButton *callBtn;

@property (weak, nonatomic) IBOutlet UIButton *locationBtn;
@property (weak, nonatomic) IBOutlet UIButton *hoursBtn;
@property (strong, nonatomic) IBOutlet UIButton *heartBtn;
@end
