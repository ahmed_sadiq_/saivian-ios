//
//  ProfileVC.m
//  Saivian
//
//  Created by Apple on 28/04/2016.
//  Copyright © 2016 Osama. All rights reserved.
//

#import "ProfileVC.h"
#import "DrawerVC.h"
#import "UIImageView+RoundImage.h"
#import "CustomLoading.h"
#import <QuartzCore/QuartzCore.h>
@interface ProfileVC ()

@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    _fNmeTxt.text = appDel.userModel.first_name;
    _lNmeTxt.text = appDel.userModel.last_name;
    _sIdTxt.text = appDel.userModel.saivian_id;
    _psdTxt.text = appDel.userModel.password;
    _emilTxt.text = appDel.userModel.email;
    _phoneTxt.text = appDel.userModel.phone_no;
    
    self.profilePic.imageURL = [NSURL URLWithString:appDel.userModel.profile_image];
    NSURL *url = [NSURL URLWithString:appDel.userModel.profile_image];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
    [self.profilePic.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [self.profilePic.layer setBorderWidth: 2.0];
    
    [self.profilePic roundImageCorner];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)openDrawer:(id)sender {
    
    [[DrawerVC getInstance] AddInView:self.view];
    [[DrawerVC getInstance] ShowInView];
}

- (IBAction)editPressed:(id)sender {
    
    
    UIButton *senderBtn = (UIButton*) sender;
    
    if(senderBtn.tag == 0) {
        _fNmeImg.hidden= false;
        _lNmeImg.hidden = false;
        //_sIdImg.hidden = false;
        //_pswdImg.hidden = false;
        //_emailImg.hidden = false;
        _phoneImg.hidden = false;
        
        _fNmeTxt.enabled = true;
        _lNmeTxt.enabled = true;
        //_sIdTxt.enabled = true;
        //_psdTxt.enabled = true;
        //_emilTxt.enabled = true;
        _phoneTxt.enabled = true;
        
        senderBtn.tag = 1;
        
        [_editBtn setBackgroundImage:[UIImage imageNamed:@"save_btn.png"] forState:UIControlStateNormal];
    }
    else {
        
        [self editProfileImage];
    }
    
    
}

- (IBAction)whatsThisPressed:(id)sender {
    
    _whtsThisView.layer.cornerRadius = 5;
    _whtsThisView.layer.masksToBounds = YES;
    _whtsThisView.layer.borderWidth = 1;
    _whtsThisView.layer.borderColor = [UIColor grayColor].CGColor;
    _overlayView.hidden = false;
    _whtsThisView.hidden = false;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [_overlayView addGestureRecognizer:tapRecognizer];
    _overlayView.tag=2;
}

-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender
{
    if(sender.view.tag==2) {
        //do something here
        _overlayView.hidden = true;
        _whtsThisView.hidden = true;
        _urlView.hidden = true;
        
        [_urlTxt resignFirstResponder];
    }
}


- (IBAction)shareBtnPressed:(id)sender {
    
    _urlView.layer.cornerRadius = 2;
    _urlView.layer.masksToBounds = YES;
    _urlView.layer.borderWidth = 1;
    _urlView.layer.borderColor = [UIColor grayColor].CGColor;
    _overlayView.hidden = false;
    _urlView.hidden = false;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [_overlayView addGestureRecognizer:tapRecognizer];
    _overlayView.tag=2;
    
}

- (IBAction)profilePicPressed:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.profilePic.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    [self.profilePic.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [self.profilePic.layer setBorderWidth: 2.0];
    
    [self.profilePic roundImageCorner];
}


#pragma mark Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
    
    
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag == 6) {
        [self animateTextField: textField up: YES];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    // add your method here
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField.tag == 6) {
        
        
        [self animateTextField: textField up: NO];
    }
    else if(textField.tag == 11) {
        if(textField.text.length > 0) {
            
            NSString *urlStr = textField.text;
            
            if ([urlStr hasSuffix:@".com"] || [urlStr hasSuffix:@".net"]) {
                
                [[NSUserDefaults standardUserDefaults] setObject:urlStr forKey:@"urlShared"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else
            {
                NSString *message = @"Invalid Url, try again!";
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                               message:message
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                int duration = 1; // duration in seconds
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert dismissViewControllerAnimated:YES completion:nil];
                });
                
            }
        }
    }
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
#pragma mark - Sign Up Server Methods

- (void) editProfileImage {
    [CustomLoading showAlertMessage];
    NSString *strUserId = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    //create request
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    
    //Set Params
    [urlRequest setHTTPShouldHandleCookies:NO];
    [urlRequest setTimeoutInterval:60];
    [urlRequest setHTTPMethod:@"POST"];
    
    //Create boundary, it can be anything
    NSString *boundary = @"---011000010111000001101001";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [urlRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    //Populate a dictionary with all the regular values you would like to send.
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:_fNmeTxt.text forKey:@"first_name"];
    
    [parameters setValue:_lNmeTxt.text forKey:@"last_name"];
    
    [parameters setValue:_phoneTxt.text forKey:@"phone_no"];
    
    [parameters setValue:strUserId forKey:@"customer_id"];
    
    
    // add params (all params are strings)
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSString *FileParamConstant = @"profile_image";
    
    NSData *imageData = UIImageJPEGRepresentation(_profilePic.image, 1);
    
    //Assuming data is not nil we add this to the multipart form
    if (imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [urlRequest setHTTPBody:body];
    
    NSString *urlString = [NSString stringWithFormat:@"http://www.saivian-squad.com/customer/editProfile"];
    NSString *escapedPath = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // set URL
    [urlRequest setURL:[NSURL URLWithString:escapedPath]];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                               
                               [CustomLoading DismissAlertMessage];
                               
                               
                               
                               if ([httpResponse statusCode] == 200) {
                                   
                                   NSLog(@"success");
                                   
                                   NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                        options:kNilOptions error:&error];
                                   
                                   int status  =  [[json objectForKey:@"status"] intValue];
                                   
                                   if(status == 1) {
                                       NSDictionary *customerData = [json objectForKey:@"customer_data"];
                                       
                                       appDel.userModel.first_name = _fNmeTxt.text;
                                       appDel.userModel.last_name = _lNmeTxt.text;
                                       appDel.userModel.phone_no = _phoneTxt.text;
                                       appDel.userModel.profile_image = [customerData objectForKey:@"profile_image"];
                                       appDel.reloadImage = true;
                                       
                                   }
                                   else {
                                       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"We are Busy!!" message:@"Please try again" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                                       [alert show];
                                   }
                                   
                               }
                               else{
                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Something went wrong with internet connection!!!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                                   [alert show];
                               }
                               
                           }];
    
}

@end
