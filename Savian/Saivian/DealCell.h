//
//  DealCell.h
//  Saivian
//
//  Created by Ahmed Sadiq on 16/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface DealCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsyncImageView *imgView;

@end
