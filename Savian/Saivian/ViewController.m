//
//  ViewController.m
//  Saivian
//
//  Created by Apple on 27/04/2016.
//  Copyright © 2016 Osama. All rights reserved.
//

#import "ViewController.h"
#import "NavigationHandler.h"
#import "Constants.h"
#import "CustomLoading.h"
#import "UserModel.h"
#import "AppDelegate.h"

#import "StoreModel.h"
#import "RecieptModel.h"
#import "CashBackLocalModel.h"

@interface ViewController ()

@end

@implementation ViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [self.view addGestureRecognizer:tapRecognizer];
    self.view.tag=2;
    
    _emailTxt.autocapitalizationType = UITextAutocapitalizationTypeNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Sign In View

-(IBAction)signInPressed:(id)sender
{
    [self sendSignInCalls];
}

- (IBAction)signUpFromSignInPressed:(id)sender {
    
    _signInView.hidden = true;
    
    [UIView beginAnimations:@"LeftFlip" context:nil];
    [UIView setAnimationDuration:0.5];
    _signUpView.hidden=NO;
    [UIView setAnimationCurve:UIViewAnimationOptionTransitionCrossDissolve];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight    forView:_signUpView cache:YES];
    [UIView commitAnimations];
    
    
    
}
-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender
{
    if(sender.view.tag==2) {
        [self.view endEditing:YES];
    }
}
- (IBAction)forgotBtnPressed:(id)sender {
    [[NavigationHandler getInstance] NavigateToForgotPassword];
}

#pragma mark - Sign Up View

- (IBAction)signUpPressed:(id)sender {
    [self sendSignUpCalls];
}

- (IBAction)signUpSkipPressed:(id)sender {
    [[NavigationHandler getInstance] NavigateToHomeScreen];
}

- (IBAction)SignInFromSignUpPressed:(id)sender {
    
    _signUpView.hidden = true;
    
    
    [UIView beginAnimations:@"LeftFlip" context:nil];
    [UIView setAnimationDuration:0.5];
    _signInView.hidden=NO;
    [UIView setAnimationCurve:UIViewAnimationTransitionFlipFromLeft ];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft    forView:_signInView cache:YES];
    [UIView commitAnimations];
}

#pragma mark - Sign In Server Methods

- (void) sendSignInCalls {
    [self.view endEditing:YES];
    
    if([self verifyServerSignInCall]) {
        [CustomLoading showAlertMessage];
        
        
        NSURL *url = [NSURL URLWithString:@"http://www.saivian-squad.com/customer/login"];
        request = [ASIFormDataRequest requestWithURL:url];
        [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
        [request setPostValue:[_emailTxt.text lowercaseString] forKey:@"email"];
        [request setPostValue:_pswdTxt.text forKey:@"password"];
        
        [request setRequestMethod:@"POST"];
        [request setTimeOutSeconds:300];
        [request setDelegate:self];
        [request startAsynchronous];
    }
}

- (BOOL) verifyServerSignInCall {
    
    if (_pswdTxt.text.length < 1 || _emailTxt.text.length <1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Username or password cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    return true;
}

#pragma mark - Sign Up Server Methods

- (void) sendSignUpCalls {
    [self.view endEditing:YES];
    if([self verifyServerCall]) {
//        [CustomLoading showAlertMessage];
//        
//        
//        NSURL *url = [NSURL URLWithString:@"http://www.saivian-squad.com/customer/signup"];
//        request = [ASIFormDataRequest requestWithURL:url];
//        [request addRequestHeader:@"Content-Type" value:@"multipart/form-data; boundary=---011000010111000001101001"];
//        [request setPostValue:_signUpFNameTxt.text forKey:@"first_name"];
//        [request setPostValue:_signUpLNameTxt.text forKey:@"last_name"];
//        [request setPostValue:_signUpEmailTxt.text forKey:@"email"];
//        [request setPostValue:_signUpPswdTxt.text forKey:@"password"];
//        [request setPostValue:_signUpPNumberTxt.text forKey:@"referral_id"];
//        [request setPostValue:_signUpUNameTxt.text forKey:@"dob"];
//        
//        [request setRequestMethod:@"POST"];
//        [request setTimeOutSeconds:300];
//        [request setDelegate:self];
//        [request startAsynchronous];
        
        /*Thank you for signing up. You need to visit http://www.saivian.net/ to complete your sign up process and start using the app. In case of any questions, please contact Saivian support team.*/
        
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:nil
                                                          message:@"Thank you for signing up. You need to visit http://www.saivian.net/ to complete your sign up process and start using the app. In case of any questions, please contact Saivian support team."
                                                         delegate:self
                                                cancelButtonTitle:nil
                                                otherButtonTitles:@"Got it", @"Visit", nil];
        [myAlert show];
    }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        
        _signUpView.hidden = true;
        
        
        [UIView beginAnimations:@"LeftFlip" context:nil];
        [UIView setAnimationDuration:0.5];
        _signInView.hidden=NO;
        [UIView setAnimationCurve:UIViewAnimationTransitionFlipFromLeft ];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft    forView:_signInView cache:YES];
        [UIView commitAnimations];
        
    }else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.saivian.net/"]];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [CustomLoading DismissAlertMessage];
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
    int success = [[result objectForKey:@"status"] intValue];
    
    if(success == 1) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"https://www.saivian.net" forKey:@"urlShared"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        int monthCashBack = [[result objectForKey:@"month_cashback"] intValue];
        int yearCashBack = [[result objectForKey:@"year_cashback"] intValue];
        
        NSString *strMonthCashBack = [NSString stringWithFormat:@"%d",monthCashBack];
        NSString *strYearCashBack = [NSString stringWithFormat:@"%d",yearCashBack];
        
        [[NSUserDefaults standardUserDefaults] setObject:strMonthCashBack forKey:@"monthCashBack"];
        [[NSUserDefaults standardUserDefaults] setObject:strYearCashBack forKey:@"yearCashBack"];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isLoggedIn"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSDictionary *user = [result objectForKey:@"customer_data"];
        
        AppDelegate *appDel = (AppDelegate*) [UIApplication sharedApplication].delegate;
        
        appDel.userModel = [[UserModel alloc] init];
        
        appDel.userModel.userID = [user objectForKey:@"id"];
        appDel.userModel.referral_id = [user objectForKey:@"referral_id"];
        appDel.userModel.first_name = [user objectForKey:@"first_name"];
        appDel.userModel.last_name = [user objectForKey:@"last_name"];
        appDel.userModel.email = [user objectForKey:@"email"];
        appDel.userModel.password = [user objectForKey:@"password"];
        appDel.userModel.profile_image = [user objectForKey:@"profile_image"];
        appDel.userModel.gender = [user objectForKey:@"gender"];
        appDel.userModel.dob = [user objectForKey:@"dob"];
        appDel.userModel.city = [user objectForKey:@"city"];
        appDel.userModel.state = [user objectForKey:@"state"];
        appDel.userModel.zip = [user objectForKey:@"zip"];
        appDel.userModel.country = [user objectForKey:@"country"];
        appDel.userModel.phone_no = [user objectForKey:@"phone_no"];
        appDel.userModel.is_verified = [user objectForKey:@"is_verified"];
        appDel.userModel.is_active = [user objectForKey:@"is_active"];
        appDel.userModel.created_date = [user objectForKey:@"created_date"];
        appDel.userModel.saivian_id = [user objectForKey:@"saivian_id"];
        
        appDel.userModel.cashBackLocalArray = [[NSMutableArray alloc] init];
        
        NSArray *cashBackData = [result objectForKey:@"cash_back_data"];
        
        for(int i=0; i<cashBackData.count; i++) {
            CashBackLocalModel *cblModel = [[CashBackLocalModel alloc] init];
            NSDictionary *tempDict = [cashBackData objectAtIndex:i];
            
            cblModel.cashBackId = [tempDict objectForKey:@"id"];
            cblModel.categoryName = [tempDict objectForKey:@"category_name"];
            cblModel.cashBackDescription = [tempDict objectForKey:@"description"];
            cblModel.image = [tempDict objectForKey:@"image"];
            cblModel.isActive = [[tempDict objectForKey:@"is_active"] boolValue];
            cblModel.storesList = [[NSMutableArray alloc] init];
            
            NSArray *storesData = [tempDict objectForKey:@"stores_list"];
            
            for(int j=0; j<storesData.count; j++) {
                NSDictionary *storeDict = [storesData objectAtIndex:j];
                
                StoreModel *sModel = [[StoreModel alloc] init];
                
                sModel.storeId = [storeDict objectForKey:@"store_id"];
                sModel.storeName = [storeDict objectForKey:@"store_name"];
                sModel.firstName = [storeDict objectForKey:@"first_name"];
                sModel.lastName = [storeDict objectForKey:@"last_name"];
                sModel.storeDescription = [storeDict objectForKey:@"description"];
                sModel.email = [storeDict objectForKey:@"email"];
                sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
                sModel.webUrl = [storeDict objectForKey:@"web_url"];
                sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
                sModel.profileImage = [storeDict objectForKey:@"profile_image"];
                sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
                
                NSDictionary *locationDict = [storeDict objectForKey:@"location"];
                
                sModel.address = [locationDict objectForKey:@"address"];
                sModel.city = [locationDict objectForKey:@"city"];
                sModel.state = [locationDict objectForKey:@"state"];
                sModel.zip = [locationDict objectForKey:@"zip"];
                sModel.country = [locationDict objectForKey:@"country"];
                sModel.latitude = [locationDict objectForKey:@"latitude"];
                sModel.longitude = [locationDict objectForKey:@"longitude"];
                sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
                
                
                [cblModel.storesList addObject:sModel];
            }
            [appDel.userModel.cashBackLocalArray addObject:cblModel];
        }
        
        
        appDel.userModel.topTenStores = [[NSMutableArray alloc] init];
        
        NSArray *topTenStoresData = [result objectForKey:@"top_ten_stores"];
        
        for(int i=0; i<topTenStoresData.count; i++) {
            NSDictionary *storeDict = [topTenStoresData objectAtIndex:i];
            
            StoreModel *sModel = [[StoreModel alloc] init];
            
            sModel.storeId = [storeDict objectForKey:@"store_id"];
            sModel.storeName = [storeDict objectForKey:@"store_name"];
            sModel.firstName = [storeDict objectForKey:@"first_name"];
            sModel.lastName = [storeDict objectForKey:@"last_name"];
            sModel.storeDescription = [storeDict objectForKey:@"description"];
            sModel.email = [storeDict objectForKey:@"email"];
            sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
            sModel.webUrl = [storeDict objectForKey:@"web_url"];
            sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
            sModel.profileImage = [storeDict objectForKey:@"profile_image"];
            sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
            
            NSDictionary *locationDict = [storeDict objectForKey:@"location"];
            
            sModel.address = [locationDict objectForKey:@"address"];
            sModel.city = [locationDict objectForKey:@"city"];
            sModel.state = [locationDict objectForKey:@"state"];
            sModel.zip = [locationDict objectForKey:@"zip"];
            sModel.country = [locationDict objectForKey:@"country"];
            sModel.latitude = [locationDict objectForKey:@"latitude"];
            sModel.longitude = [locationDict objectForKey:@"longitude"];
            
            
            sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
            
            [appDel.userModel.topTenStores addObject:sModel];
        }
        
        appDel.userModel.favouriteStores = [[NSMutableArray alloc] init];
        
        NSArray *favouriteStoresData = [result objectForKey:@"favourite_stores"];
        
        for(int i=0; i<favouriteStoresData.count; i++) {
            NSDictionary *storeDict = [favouriteStoresData objectAtIndex:i];
            
            StoreModel *sModel = [[StoreModel alloc] init];
            
            sModel.storeId = [storeDict objectForKey:@"store_id"];
            sModel.storeName = [storeDict objectForKey:@"store_name"];
            sModel.firstName = [storeDict objectForKey:@"first_name"];
            sModel.lastName = [storeDict objectForKey:@"last_name"];
            sModel.storeDescription = [storeDict objectForKey:@"description"];
            sModel.email = [storeDict objectForKey:@"email"];
            sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
            sModel.webUrl = [storeDict objectForKey:@"web_url"];
            sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
            sModel.profileImage = [storeDict objectForKey:@"profile_image"];
            sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
            
            NSDictionary *locationDict = [storeDict objectForKey:@"location"];
            
            sModel.address = [locationDict objectForKey:@"address"];
            sModel.city = [locationDict objectForKey:@"city"];
            sModel.state = [locationDict objectForKey:@"state"];
            sModel.zip = [locationDict objectForKey:@"zip"];
            sModel.country = [locationDict objectForKey:@"country"];
            sModel.latitude = [locationDict objectForKey:@"latitude"];
            sModel.longitude = [locationDict objectForKey:@"longitude"];
            
            
            sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
            
            [appDel.userModel.favouriteStores addObject:sModel];
        }
        
        appDel.userModel.allStores = [[NSMutableArray alloc] init];
        
        NSArray *allStoresData = [result objectForKey:@"all_stores_list"];
        
        for(int i=0; i<allStoresData.count; i++) {
            NSDictionary *storeDict = [allStoresData objectAtIndex:i];
            
            StoreModel *sModel = [[StoreModel alloc] init];
            
            sModel.storeId = [storeDict objectForKey:@"store_id"];
            sModel.storeName = [storeDict objectForKey:@"store_name"];
            sModel.firstName = [storeDict objectForKey:@"first_name"];
            sModel.lastName = [storeDict objectForKey:@"last_name"];
            sModel.storeDescription = [storeDict objectForKey:@"description"];
            sModel.email = [storeDict objectForKey:@"email"];
            sModel.phoneNo = [storeDict objectForKey:@"phone_no"];
            sModel.webUrl = [storeDict objectForKey:@"web_url"];
            sModel.isFavourite = [[storeDict objectForKey:@"is_favourite"] boolValue];
            sModel.profileImage = [storeDict objectForKey:@"profile_image"];
            sModel.featuredImage = [storeDict objectForKey:@"featured_image"];
            
            NSDictionary *locationDict = [storeDict objectForKey:@"location"];
            
            sModel.address = [locationDict objectForKey:@"address"];
            sModel.city = [locationDict objectForKey:@"city"];
            sModel.state = [locationDict objectForKey:@"state"];
            sModel.zip = [locationDict objectForKey:@"zip"];
            sModel.country = [locationDict objectForKey:@"country"];
            sModel.latitude = [locationDict objectForKey:@"latitude"];
            sModel.longitude = [locationDict objectForKey:@"longitude"];
            
            
            sModel.openingHoursDict = [storeDict objectForKey:@"opening_hours"];
            
            [appDel.userModel.allStores addObject:sModel];
        }
        
        appDel.userModel.recieptArray = [[NSMutableArray alloc] init];
        
        NSArray *recieptsData = [result objectForKey:@"receipts_list"];
        
        for(int i=0; i<recieptsData.count; i++) {
            NSDictionary *recieptDict = [recieptsData objectAtIndex:i];
            
            RecieptModel *rModel = [[RecieptModel alloc] init];
            
            rModel.recieptId = [recieptDict objectForKey:@"receipt_id"];
            rModel.storeName = [recieptDict objectForKey:@"store_name"];
            rModel.recieptType = [recieptDict objectForKey:@"receipt_type"];
            rModel.purchasedDate = [recieptDict objectForKey:@"purchased_date"];
            rModel.subtotalAmount = [recieptDict objectForKey:@"subtotal_amount"];
            rModel.uploadedDate = [recieptDict objectForKey:@"uploaded_date"];
            
            [appDel.userModel.recieptArray addObject:rModel];
        }
        
        NSDictionary *storeDict = (NSDictionary*)[result objectForKey:@"payment_info"];
        appDel.userModel.paymentInfoModel = [[PaymentMethod alloc] init];
        
        appDel.userModel.paymentInfoModel.cardHolderName = [storeDict objectForKey:@"card_holder_name"];
        appDel.userModel.paymentInfoModel.cardType = [storeDict objectForKey:@"card_type"];
        appDel.userModel.paymentInfoModel.cardNumber = [storeDict objectForKey:@"card_number"];
        appDel.userModel.paymentInfoModel.cvc = [storeDict objectForKey:@"cvc"];
        appDel.userModel.paymentInfoModel.expiryMonth = [storeDict objectForKey:@"expiry_month"];
        appDel.userModel.paymentInfoModel.expiryYear = [storeDict objectForKey:@"expiry_year"];
        
        
        
        [[NSUserDefaults standardUserDefaults] setObject:appDel.userModel.userID forKey:@"userID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NavigationHandler getInstance] NavigateToHomeScreen];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Authentication Error" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)theRequest {
    [CustomLoading DismissAlertMessage];
    NSString *response = [[NSString alloc] initWithData:[theRequest responseData] encoding:NSUTF8StringEncoding];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Something went wrong!!!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
}

- (BOOL) verifyServerCall {
    
    if(![self validateEmail:_signUpEmailTxt.text]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Email is not in valid format" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if (_signUpFNameTxt.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"First Name cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if(_signUpLNameTxt.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Last Name cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if(_signUpUNameTxt.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Username cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if(_signUpPswdTxt.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Password cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if(_signUpPNumberTxt.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Referral Code cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else if(_signUpPNumberTxt.text.length < 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Validation Error" message:@"Phone number cannot be empty" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    
    return true;
}

-(BOOL)validateEmail:(NSString *)candidate {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

#pragma mark Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag == 23 || textField.tag == 22) {
        [self animateTextField: textField up: YES];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    // add your method here
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField.tag == 23 || textField.tag == 22) {
        [self animateTextField: textField up: NO];
    }
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}



- (IBAction)signUpDobPressed:(id)sender {
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.delegate = self;
    hsdpvc.date = [NSDate date];
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date {
    NSLog(@"Date picked %@", date);
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    dateFormater.dateFormat = @"yyyy-MM-dd";
    self.signUpUNameTxt.text = [dateFormater stringFromDate:date];
    
}

//optional
- (void)hsDatePickerDidDismissWithQuitMethod:(HSDatePickerQuitMethod)method {
    NSLog(@"Picker did dismiss with %lu", method);
}

//optional
- (void)hsDatePickerWillDismissWithQuitMethod:(HSDatePickerQuitMethod)method {
    NSLog(@"Picker will dismiss with %lu", method);
}
@end
