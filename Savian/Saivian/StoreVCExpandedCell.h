//
//  StoreVCExpandedCell.h
//  Saivian
//
//  Created by Ahmed Sadiq on 26/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import <MapKit/MapKit.h>

@interface StoreVCExpandedCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (strong, nonatomic) IBOutlet MKMapView *mainMap;
@property (weak, nonatomic) IBOutlet AsyncImageView *featureImg;

@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *phoneLbl;
@property (weak, nonatomic) IBOutlet UILabel *webLbl;

@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UIButton *locationBtn;
@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UIButton *hoursBtn;
@property (strong, nonatomic) IBOutlet UIButton *normalStateBtn;

@property (strong, nonatomic) IBOutlet UIView *hooView;
@property (strong, nonatomic) IBOutlet UILabel *sundayTime;
@property (strong, nonatomic) IBOutlet UILabel *tuesdayTime;
@property (strong, nonatomic) IBOutlet UILabel *mondayTime;
@property (strong, nonatomic) IBOutlet UILabel *wednesdayTime;
@property (strong, nonatomic) IBOutlet UILabel *thursdayTime;
@property (strong, nonatomic) IBOutlet UILabel *firdayTime;
@property (strong, nonatomic) IBOutlet UILabel *saturdayTime;






@end
