//
//  HomeVC.h
//  Saivian
//
//  Created by Apple on 28/04/2016.
//  Copyright © 2016 Osama. All rights reserved.
//

#import "ViewController.h"
#import "SavianTabController.h"
#import "AsyncImageView.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>

@interface HomeVC : ViewController <SavianTabControllerDelegate,ASIHTTPRequestDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate> {
    float earnedAmount;
    BOOL blinkStatus;
    
    int boundToCheck;
    AppDelegate *appDel;
    ASIFormDataRequest *req;

    IBOutlet UIButton *monthBtn;
    IBOutlet UIButton *yearBtn;
    
}
- (IBAction)instantSavingPressed:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *monthCashBackLbl;


@property (weak, nonatomic) IBOutlet UILabel *userName;


@property (weak, nonatomic) IBOutlet AsyncImageView *profilePic;


@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet UIView *lowerBar;
@property (strong, nonatomic) SavianTabController *tabControl;
@property (weak, nonatomic) IBOutlet UILabel  *amountLbl;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;

@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIView *sharingView;

- (IBAction)twitterBtnPressed:(id)sender;
- (IBAction)fbBtnPressed:(id)sender;
- (IBAction)instaBtnPRessed:(id)sender;
- (IBAction)mailBtnPRessed:(id)sender;
- (IBAction)textingBtnPressed:(id)sender;

- (IBAction)sharingBtnPressed:(id)sender;


- (IBAction)cashBackLocalPressed:(id)sender;

- (IBAction)monthToDatePressed:(id)sender;
- (IBAction)yearToDatePressed:(id)sender;
@end
