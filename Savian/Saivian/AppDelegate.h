//
//  AppDelegate.h
//  Saivian
//
//  Created by Apple on 27/04/2016.
//  Copyright © 2016 Osama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"
#import "PaymentMethod.h"
#import "ASIFormDataRequest.h"
#import "InstantSavingModel.h"

@class ViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate,ASIHTTPRequestDelegate> {
    ASIFormDataRequest *request;
}

@property BOOL isLoading;
@property BOOL reloadImage;
@property (strong, nonatomic) UIWindow *window;
@property ( strong , nonatomic ) UINavigationController *navigationController;
@property ( strong , nonatomic ) ViewController *viewController;
@property ( strong , nonatomic ) UserModel *userModel;
@property ( strong , nonatomic ) PaymentMethod *paymentMethod;
@property ( strong , nonatomic ) InstantSavingModel *instantSavingModel;
@end

