//
//  CameraControllerVC.h
//  Saivian
//
//  Created by Ahmed Sadiq on 10/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import "NIDropDown.h"
#import "StoreModel.h"
#import "HTAutocompleteTextField.h"
#import "MLPAutoCompleteTextField.h"

@class DEMODataSource;
@class MLPAutoCompleteTextField;
@interface CameraControllerVC : UIViewController<NIDropDownDelegate,MLPAutoCompleteTextFieldDelegate> {
    BOOL isTypeSelected;
    NIDropDown *dropDown;
    AppDelegate *appDel;
    int selectedIndex;
    StoreModel *selectedStoreModel;
    NSString *recieptType;
    BOOL isvalidStore;
}

@property (strong, nonatomic) IBOutlet UIView *uploadTravelView;
@property (strong, nonatomic) IBOutlet UIView *utDobPickerView;
@property (strong, nonatomic) IBOutlet UIImageView *utCardUploadImg;
@property (strong, nonatomic) IBOutlet UIDatePicker *utDobPicker;
@property (strong, nonatomic) IBOutlet MLPAutoCompleteTextField *airlineTxt;
@property (strong, nonatomic) IBOutlet UITextField *thirdPartyTxt;
@property (strong, nonatomic) IBOutlet UITextField *checkInTxt;
@property (strong, nonatomic) IBOutlet UITextField *subTotalTxt;

- (IBAction)airlinePressed:(id)sender;
- (IBAction)checkInPressed:(id)sender;

- (IBAction)tuCancelPressed:(id)sender;
- (IBAction)tuSubmitPressed:(id)sender;



@property (strong, nonatomic) IBOutlet UIButton *travelBtn;
@property (strong, nonatomic) IBOutlet UIButton *shoppingBtn;
@property (weak, nonatomic) IBOutlet UIButton *cameraBtn;
@property (weak, nonatomic) IBOutlet UIImageView *selectTypeImg;
@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet UIImageView *capturedImg;
@property (strong, nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property (weak, nonatomic) IBOutlet UIView *confirmBar;
@property (strong, nonatomic) IBOutlet UIView *datePickerView;
@property (strong, nonatomic) IBOutlet UIDatePicker *dbPicker;

@property (weak, nonatomic) IBOutlet UIView *uploadView;
@property (weak, nonatomic) IBOutlet UIImageView *uploadViewPicture;

@property (strong, nonatomic) IBOutlet MLPAutoCompleteTextField *storeName;
@property (strong, nonatomic) IBOutlet UIImageView *storeTxtImg;

@property (strong, nonatomic) IBOutlet DEMODataSource *autocompleteDataSource;

@property (weak, nonatomic) IBOutlet UITextField *purchaseDate;
@property (weak, nonatomic) IBOutlet UITextField *subtotalAmount;
- (IBAction)datePressed:(id)sender;
- (IBAction)storePressed:(id)sender;


- (IBAction)sliderBtnPressed:(id)sender;
- (IBAction)cameraPressed:(id)sender;
- (IBAction)shoppingPressed:(id)sender;
- (IBAction)travelPressed:(id)sender;

- (IBAction)retakeImgPressed:(id)sender;
- (IBAction)uploadPressed:(id)sender;


- (IBAction)cancelUploadPressed:(id)sender;
- (IBAction)uploadNowPressed:(id)sender;
@end
