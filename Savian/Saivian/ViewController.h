//
//  ViewController.h
//  Saivian
//
//  Created by Apple on 27/04/2016.
//  Copyright © 2016 Osama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"
#import "HSDatePickerViewController.h"
@interface ViewController : UIViewController <ASIHTTPRequestDelegate,HSDatePickerViewControllerDelegate,UIAlertViewDelegate>
{
    ASIFormDataRequest *request;
}

#pragma mark - Sign In View


@property (weak, nonatomic) IBOutlet UIView *signInView;
@property (weak, nonatomic) IBOutlet UITextField *emailTxt;
@property (weak, nonatomic) IBOutlet UITextField *pswdTxt;

- (IBAction)forgotBtnPressed:(id)sender;
- (IBAction)signInPressed:(id)sender;
- (IBAction)signUpFromSignInPressed:(id)sender;

#pragma mark - Sign Up View
@property (weak, nonatomic) IBOutlet UIView *signUpView;
@property (weak, nonatomic) IBOutlet UITextField *signUpEmailTxt;
@property (weak, nonatomic) IBOutlet UITextField *signUpFNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *signUpLNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *signUpUNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *signUpPswdTxt;
@property (weak, nonatomic) IBOutlet UITextField *signUpPNumberTxt;
@property (weak, nonatomic) IBOutlet UITextField *signUpStateTxt;
- (IBAction)signUpPressed:(id)sender;
- (IBAction)signUpSkipPressed:(id)sender;
- (IBAction)SignInFromSignUpPressed:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *signinBtn;
- (IBAction)signUpDobPressed:(id)sender;
@end

