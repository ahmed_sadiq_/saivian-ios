//
//  OpeningHours.h
//  Saivian
//
//  Created by Ahmed Sadiq on 30/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpeningHours : NSObject
@property (strong, retain) NSMutableArray *dayName;
@property (strong, retain) NSMutableArray *dayTiming;
@end
