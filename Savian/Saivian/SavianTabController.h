//
//  SavianTabController.h
//  Saivian
//
//  Created by Ahmed Sadiq on 09/05/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SavianTabController;
@protocol SavianTabControllerDelegate
- (void) SavianTabControllerPressed: (SavianTabController *) sender;
- (void) SavianTabControllerRecieptPressed: (SavianTabController *) sender;
- (void) SavianTabControllerStorePressed: (SavianTabController *) sender;
@end

@interface SavianTabController : UIView {
    
}

@property (nonatomic, retain) NSString *direction;
@property(nonatomic, strong) UIImageView *centerImg;
@property(nonatomic, strong) UIButton *centerBtn;
@property(nonatomic, strong) UIButton *recieptBtn;
@property(nonatomic, strong) UIButton *storeBtn;
@property (nonatomic, retain) id <SavianTabControllerDelegate> delegate;

- (id) showTabController : (UIView *)parentV andFrame: (CGRect) frame;

@end
