//
//  Constants.h
//  HydePark
//
//  Created by Ahmed Sadiq on 14/05/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#ifndef ServiceDesk_Constants_h
#define ServiceDesk_Constants_h

#define SERVER_URL @"http://www.saivian-squad.com/"
#define SERVER_SIGNUP @"userSignup"


#pragma mark-
#pragma mark Screen Sizes
#define IS_IPHONE_6_PLUS ([[UIScreen mainScreen] bounds].size.height == 736)
#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667)
#define IS_IPHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568)
#define IS_IPHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480)
#define IS_IPAD ([[UIScreen mainScreen] bounds].size.height == 1024)

#endif
